<?php
/**
 * SupplierApiTest
 *
 * PHP version 5
 *
 * @category Class
 * @package  Trollweb\EaccountingApi
 * @author   http://github.com/swagger-api/swagger-codegen
 * @license  http://www.apache.org/licenses/LICENSE-2.0 Apache Licene v2
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Visma eAccounting API V2
 *
 * No descripton provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v2
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the model.
 */

namespace Trollweb\EaccountingApi;

/**
 * SupplierApiTest Class Doc Comment
 *
 * @category    Class */
// * @description SupplierApi
/**
 * @package     Trollweb\EaccountingApi
 * @author      http://github.com/swagger-api/swagger-codegen
 * @license     http://www.apache.org/licenses/LICENSE-2.0 Apache Licene v2
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class SupplierApiTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass()
    {

    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {

    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {

    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {

    }

    /**
     * Test "SupplierApi"
     */
    public function testSupplierApi()
    {

    }

    /**
     * Test attribute "id"
     */
    public function testPropertyId()
    {

    }

    /**
     * Test attribute "supplier_number"
     */
    public function testPropertySupplierNumber()
    {

    }

    /**
     * Test attribute "address1"
     */
    public function testPropertyAddress1()
    {

    }

    /**
     * Test attribute "address2"
     */
    public function testPropertyAddress2()
    {

    }

    /**
     * Test attribute "automatic_payment_service"
     */
    public function testPropertyAutomaticPaymentService()
    {

    }

    /**
     * Test attribute "bank_account_number"
     */
    public function testPropertyBankAccountNumber()
    {

    }

    /**
     * Test attribute "bank_bban"
     */
    public function testPropertyBankBban()
    {

    }

    /**
     * Test attribute "bank_bic"
     */
    public function testPropertyBankBic()
    {

    }

    /**
     * Test attribute "bank_code"
     */
    public function testPropertyBankCode()
    {

    }

    /**
     * Test attribute "bank_country_code"
     */
    public function testPropertyBankCountryCode()
    {

    }

    /**
     * Test attribute "bankgiro_number"
     */
    public function testPropertyBankgiroNumber()
    {

    }

    /**
     * Test attribute "bank_iban"
     */
    public function testPropertyBankIban()
    {

    }

    /**
     * Test attribute "bank_name"
     */
    public function testPropertyBankName()
    {

    }

    /**
     * Test attribute "city"
     */
    public function testPropertyCity()
    {

    }

    /**
     * Test attribute "contact_person_email"
     */
    public function testPropertyContactPersonEmail()
    {

    }

    /**
     * Test attribute "contact_person_mobile"
     */
    public function testPropertyContactPersonMobile()
    {

    }

    /**
     * Test attribute "contact_person_name"
     */
    public function testPropertyContactPersonName()
    {

    }

    /**
     * Test attribute "contact_person_phone"
     */
    public function testPropertyContactPersonPhone()
    {

    }

    /**
     * Test attribute "corporate_identity_number"
     */
    public function testPropertyCorporateIdentityNumber()
    {

    }

    /**
     * Test attribute "country_code"
     */
    public function testPropertyCountryCode()
    {

    }

    /**
     * Test attribute "created_utc"
     */
    public function testPropertyCreatedUtc()
    {

    }

    /**
     * Test attribute "currency_code"
     */
    public function testPropertyCurrencyCode()
    {

    }

    /**
     * Test attribute "email_address"
     */
    public function testPropertyEmailAddress()
    {

    }

    /**
     * Test attribute "mobile_phone"
     */
    public function testPropertyMobilePhone()
    {

    }

    /**
     * Test attribute "modified_utc"
     */
    public function testPropertyModifiedUtc()
    {

    }

    /**
     * Test attribute "name"
     */
    public function testPropertyName()
    {

    }

    /**
     * Test attribute "note"
     */
    public function testPropertyNote()
    {

    }

    /**
     * Test attribute "plusgiro_number"
     */
    public function testPropertyPlusgiroNumber()
    {

    }

    /**
     * Test attribute "postal_code"
     */
    public function testPropertyPostalCode()
    {

    }

    /**
     * Test attribute "telephone"
     */
    public function testPropertyTelephone()
    {

    }

    /**
     * Test attribute "terms_of_payment_id"
     */
    public function testPropertyTermsOfPaymentId()
    {

    }

    /**
     * Test attribute "www_address"
     */
    public function testPropertyWwwAddress()
    {

    }

    /**
     * Test attribute "bank_fee_code"
     */
    public function testPropertyBankFeeCode()
    {

    }

    /**
     * Test attribute "pay_from_bank_account_id"
     */
    public function testPropertyPayFromBankAccountId()
    {

    }

    /**
     * Test attribute "foreign_payment_code_id"
     */
    public function testPropertyForeignPaymentCodeId()
    {

    }

    /**
     * Test attribute "uses_payment_reference_numbers"
     */
    public function testPropertyUsesPaymentReferenceNumbers()
    {

    }

    /**
     * Test attribute "is_active"
     */
    public function testPropertyIsActive()
    {

    }

    /**
     * Test attribute "self_employed_without_fixed_address"
     */
    public function testPropertySelfEmployedWithoutFixedAddress()
    {

    }

}
