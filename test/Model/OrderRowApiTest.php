<?php
/**
 * OrderRowApiTest
 *
 * PHP version 5
 *
 * @category Class
 * @package  Trollweb\EaccountingApi
 * @author   http://github.com/swagger-api/swagger-codegen
 * @license  http://www.apache.org/licenses/LICENSE-2.0 Apache Licene v2
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Visma eAccounting API V2
 *
 * No descripton provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v2
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the model.
 */

namespace Trollweb\EaccountingApi;

/**
 * OrderRowApiTest Class Doc Comment
 *
 * @category    Class */
// * @description OrderRowApi
/**
 * @package     Trollweb\EaccountingApi
 * @author      http://github.com/swagger-api/swagger-codegen
 * @license     http://www.apache.org/licenses/LICENSE-2.0 Apache Licene v2
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class OrderRowApiTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass()
    {

    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {

    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {

    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {

    }

    /**
     * Test "OrderRowApi"
     */
    public function testOrderRowApi()
    {

    }

    /**
     * Test attribute "line_number"
     */
    public function testPropertyLineNumber()
    {

    }

    /**
     * Test attribute "delivered_quantity"
     */
    public function testPropertyDeliveredQuantity()
    {

    }

    /**
     * Test attribute "article_id"
     */
    public function testPropertyArticleId()
    {

    }

    /**
     * Test attribute "article_number"
     */
    public function testPropertyArticleNumber()
    {

    }

    /**
     * Test attribute "is_text_row"
     */
    public function testPropertyIsTextRow()
    {

    }

    /**
     * Test attribute "text"
     */
    public function testPropertyText()
    {

    }

    /**
     * Test attribute "unit_price"
     */
    public function testPropertyUnitPrice()
    {

    }

    /**
     * Test attribute "discount_percentage"
     */
    public function testPropertyDiscountPercentage()
    {

    }

    /**
     * Test attribute "quantity"
     */
    public function testPropertyQuantity()
    {

    }

    /**
     * Test attribute "work_cost_type"
     */
    public function testPropertyWorkCostType()
    {

    }

    /**
     * Test attribute "is_work_cost"
     */
    public function testPropertyIsWorkCost()
    {

    }

    /**
     * Test attribute "eligible_for_reverse_charge_on_vat"
     */
    public function testPropertyEligibleForReverseChargeOnVat()
    {

    }

    /**
     * Test attribute "cost_center_item_id1"
     */
    public function testPropertyCostCenterItemId1()
    {

    }

    /**
     * Test attribute "cost_center_item_id2"
     */
    public function testPropertyCostCenterItemId2()
    {

    }

    /**
     * Test attribute "cost_center_item_id3"
     */
    public function testPropertyCostCenterItemId3()
    {

    }

    /**
     * Test attribute "id"
     */
    public function testPropertyId()
    {

    }

    /**
     * Test attribute "project_id"
     */
    public function testPropertyProjectId()
    {

    }

}
