<?php
/**
 * OrderApiTest
 *
 * PHP version 5
 *
 * @category Class
 * @package  Trollweb\EaccountingApi
 * @author   http://github.com/swagger-api/swagger-codegen
 * @license  http://www.apache.org/licenses/LICENSE-2.0 Apache Licene v2
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Visma eAccounting API V2
 *
 * No descripton provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v2
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the model.
 */

namespace Trollweb\EaccountingApi;

/**
 * OrderApiTest Class Doc Comment
 *
 * @category    Class */
// * @description OrderApi
/**
 * @package     Trollweb\EaccountingApi
 * @author      http://github.com/swagger-api/swagger-codegen
 * @license     http://www.apache.org/licenses/LICENSE-2.0 Apache Licene v2
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class OrderApiTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass()
    {

    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {

    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {

    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {

    }

    /**
     * Test "OrderApi"
     */
    public function testOrderApi()
    {

    }

    /**
     * Test attribute "id"
     */
    public function testPropertyId()
    {

    }

    /**
     * Test attribute "amount"
     */
    public function testPropertyAmount()
    {

    }

    /**
     * Test attribute "customer_id"
     */
    public function testPropertyCustomerId()
    {

    }

    /**
     * Test attribute "currency_code"
     */
    public function testPropertyCurrencyCode()
    {

    }

    /**
     * Test attribute "created_utc"
     */
    public function testPropertyCreatedUtc()
    {

    }

    /**
     * Test attribute "vat_amount"
     */
    public function testPropertyVatAmount()
    {

    }

    /**
     * Test attribute "roundings_amount"
     */
    public function testPropertyRoundingsAmount()
    {

    }

    /**
     * Test attribute "delivered_amount"
     */
    public function testPropertyDeliveredAmount()
    {

    }

    /**
     * Test attribute "delivered_vat_amount"
     */
    public function testPropertyDeliveredVatAmount()
    {

    }

    /**
     * Test attribute "delivered_roundings_amount"
     */
    public function testPropertyDeliveredRoundingsAmount()
    {

    }

    /**
     * Test attribute "delivery_customer_name"
     */
    public function testPropertyDeliveryCustomerName()
    {

    }

    /**
     * Test attribute "delivery_address1"
     */
    public function testPropertyDeliveryAddress1()
    {

    }

    /**
     * Test attribute "delivery_address2"
     */
    public function testPropertyDeliveryAddress2()
    {

    }

    /**
     * Test attribute "delivery_postal_code"
     */
    public function testPropertyDeliveryPostalCode()
    {

    }

    /**
     * Test attribute "delivery_city"
     */
    public function testPropertyDeliveryCity()
    {

    }

    /**
     * Test attribute "delivery_country_code"
     */
    public function testPropertyDeliveryCountryCode()
    {

    }

    /**
     * Test attribute "your_reference"
     */
    public function testPropertyYourReference()
    {

    }

    /**
     * Test attribute "our_reference"
     */
    public function testPropertyOurReference()
    {

    }

    /**
     * Test attribute "invoice_address1"
     */
    public function testPropertyInvoiceAddress1()
    {

    }

    /**
     * Test attribute "invoice_address2"
     */
    public function testPropertyInvoiceAddress2()
    {

    }

    /**
     * Test attribute "invoice_city"
     */
    public function testPropertyInvoiceCity()
    {

    }

    /**
     * Test attribute "invoice_country_code"
     */
    public function testPropertyInvoiceCountryCode()
    {

    }

    /**
     * Test attribute "invoice_customer_name"
     */
    public function testPropertyInvoiceCustomerName()
    {

    }

    /**
     * Test attribute "invoice_postal_code"
     */
    public function testPropertyInvoicePostalCode()
    {

    }

    /**
     * Test attribute "delivery_method_name"
     */
    public function testPropertyDeliveryMethodName()
    {

    }

    /**
     * Test attribute "delivery_method_code"
     */
    public function testPropertyDeliveryMethodCode()
    {

    }

    /**
     * Test attribute "delivery_term_name"
     */
    public function testPropertyDeliveryTermName()
    {

    }

    /**
     * Test attribute "delivery_term_code"
     */
    public function testPropertyDeliveryTermCode()
    {

    }

    /**
     * Test attribute "eu_third_party"
     */
    public function testPropertyEuThirdParty()
    {

    }

    /**
     * Test attribute "customer_is_private_person"
     */
    public function testPropertyCustomerIsPrivatePerson()
    {

    }

    /**
     * Test attribute "order_date"
     */
    public function testPropertyOrderDate()
    {

    }

    /**
     * Test attribute "status"
     */
    public function testPropertyStatus()
    {

    }

    /**
     * Test attribute "number"
     */
    public function testPropertyNumber()
    {

    }

    /**
     * Test attribute "modified_utc"
     */
    public function testPropertyModifiedUtc()
    {

    }

    /**
     * Test attribute "delivery_date"
     */
    public function testPropertyDeliveryDate()
    {

    }

    /**
     * Test attribute "house_work_amount"
     */
    public function testPropertyHouseWorkAmount()
    {

    }

    /**
     * Test attribute "house_work_automatic_distribution"
     */
    public function testPropertyHouseWorkAutomaticDistribution()
    {

    }

    /**
     * Test attribute "house_work_corporate_identity_number"
     */
    public function testPropertyHouseWorkCorporateIdentityNumber()
    {

    }

    /**
     * Test attribute "house_work_property_name"
     */
    public function testPropertyHouseWorkPropertyName()
    {

    }

    /**
     * Test attribute "rows"
     */
    public function testPropertyRows()
    {

    }

    /**
     * Test attribute "shipped_date_time"
     */
    public function testPropertyShippedDateTime()
    {

    }

    /**
     * Test attribute "rot_reduced_invoicing_type"
     */
    public function testPropertyRotReducedInvoicingType()
    {

    }

    /**
     * Test attribute "rot_property_type"
     */
    public function testPropertyRotPropertyType()
    {

    }

    /**
     * Test attribute "persons"
     */
    public function testPropertyPersons()
    {

    }

    /**
     * Test attribute "reverse_charge_on_construction_services"
     */
    public function testPropertyReverseChargeOnConstructionServices()
    {

    }

}
