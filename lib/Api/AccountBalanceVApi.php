<?php
/**
 * AccountBalanceVApi
 * PHP version 5
 *
 * @category Class
 * @package  Trollweb\EaccountingApi
 * @author   http://github.com/swagger-api/swagger-codegen
 * @license  http://www.apache.org/licenses/LICENSE-2.0 Apache Licene v2
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Visma eAccounting API V2
 *
 * No descripton provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v2
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace Trollweb\EaccountingApi\Api;

use \Trollweb\EaccountingApi\Configuration;
use \Trollweb\EaccountingApi\ApiClient;
use \Trollweb\EaccountingApi\ApiException;
use \Trollweb\EaccountingApi\ObjectSerializer;

/**
 * AccountBalanceVApi Class Doc Comment
 *
 * @category Class
 * @package  Trollweb\EaccountingApi
 * @author   http://github.com/swagger-api/swagger-codegen
 * @license  http://www.apache.org/licenses/LICENSE-2.0 Apache Licene v2
 * @link     https://github.com/swagger-api/swagger-codegen
 */
class AccountBalanceVApi
{

    /**
     * API Client
     *
     * @var \Trollweb\EaccountingApi\ApiClient instance of the ApiClient
     */
    protected $apiClient;

    /**
     * Constructor
     *
     * @param \Trollweb\EaccountingApi\ApiClient|null $apiClient The api client to use
     */
    public function __construct(\Trollweb\EaccountingApi\ApiClient $apiClient = null)
    {
        if ($apiClient == null) {
            $apiClient = new ApiClient();
            $apiClient->getConfig()->setHost('https://eaccountingapi-sandbox.test.vismaonline.com');
        }

        $this->apiClient = $apiClient;
    }

    /**
     * Get API client
     *
     * @return \Trollweb\EaccountingApi\ApiClient get the API client
     */
    public function getApiClient()
    {
        return $this->apiClient;
    }

    /**
     * Set the API client
     *
     * @param \Trollweb\EaccountingApi\ApiClient $apiClient set the API client
     *
     * @return AccountBalanceVApi
     */
    public function setApiClient(\Trollweb\EaccountingApi\ApiClient $apiClient)
    {
        $this->apiClient = $apiClient;
        return $this;
    }

    /**
     * Operation accountBalanceV2Get
     *
     * Get Account Balance on a specific account and date.
     *
     * @param int $account_number  (required)
     * @param \DateTime $date Format: yyyy-MM-dd (required)
     * @return \Trollweb\EaccountingApi\Model\AccountBalanceAPI
     * @throws \Trollweb\EaccountingApi\ApiException on non-2xx response
     */
    public function accountBalanceV2Get($account_number, $date)
    {
        list($response) = $this->accountBalanceV2GetWithHttpInfo($account_number, $date);
        return $response;
    }

    /**
     * Operation accountBalanceV2GetWithHttpInfo
     *
     * Get Account Balance on a specific account and date.
     *
     * @param int $account_number  (required)
     * @param \DateTime $date Format: yyyy-MM-dd (required)
     * @return Array of \Trollweb\EaccountingApi\Model\AccountBalanceAPI, HTTP status code, HTTP response headers (array of strings)
     * @throws \Trollweb\EaccountingApi\ApiException on non-2xx response
     */
    public function accountBalanceV2GetWithHttpInfo($account_number, $date)
    {
        // verify the required parameter 'account_number' is set
        if ($account_number === null) {
            throw new \InvalidArgumentException('Missing the required parameter $account_number when calling accountBalanceV2Get');
        }
        // verify the required parameter 'date' is set
        if ($date === null) {
            throw new \InvalidArgumentException('Missing the required parameter $date when calling accountBalanceV2Get');
        }
        // parse inputs
        $resourcePath = "/v2/accountbalances/{accountNumber}/{date}";
        $httpBody = '';
        $queryParams = array();
        $headerParams = array();
        $formParams = array();
        $_header_accept = $this->apiClient->selectHeaderAccept(array('application/json', 'text/json', 'application/xml', 'text/xml'));
        if (!is_null($_header_accept)) {
            $headerParams['Accept'] = $_header_accept;
        }
        $headerParams['Content-Type'] = $this->apiClient->selectHeaderContentType(array());

        // path params
        if ($account_number !== null) {
            $resourcePath = str_replace(
                "{" . "accountNumber" . "}",
                $this->apiClient->getSerializer()->toPathValue($account_number),
                $resourcePath
            );
        }
        // path params
        if ($date !== null) {
            $resourcePath = str_replace(
                "{" . "date" . "}",
                $this->apiClient->getSerializer()->toPathValue($date),
                $resourcePath
            );
        }
        // default format to json
        $resourcePath = str_replace("{format}", "json", $resourcePath);

        
        // for model (json/xml)
        if (isset($_tempBody)) {
            $httpBody = $_tempBody; // $_tempBody is the method argument, if present
        } elseif (count($formParams) > 0) {
            $httpBody = $formParams; // for HTTP post (form)
        }
        // make the API Call
        try {
            list($response, $statusCode, $httpHeader) = $this->apiClient->callApi(
                $resourcePath,
                'GET',
                $queryParams,
                $httpBody,
                $headerParams,
                '\Trollweb\EaccountingApi\Model\AccountBalanceAPI',
                '/v2/accountbalances/{accountNumber}/{date}'
            );

            return array($this->apiClient->getSerializer()->deserialize($response, '\Trollweb\EaccountingApi\Model\AccountBalanceAPI', $httpHeader), $statusCode, $httpHeader);
        } catch (ApiException $e) {
            switch ($e->getCode()) {
                case 200:
                    $data = $this->apiClient->getSerializer()->deserialize($e->getResponseBody(), '\Trollweb\EaccountingApi\Model\AccountBalanceAPI', $e->getResponseHeaders());
                    $e->setResponseObject($data);
                    break;
            }

            throw $e;
        }
    }

    /**
     * Operation accountBalanceV2Get_0
     *
     * Get Account balance on a specific date (yyyy-MM-dd). Filter to include accounts where balance is 0.
     *
     * @param \DateTime $date Format: yyyy-MM-dd (required)
     * @return \Trollweb\EaccountingApi\Model\PaginatedResponseAccountBalanceAPI
     * @throws \Trollweb\EaccountingApi\ApiException on non-2xx response
     */
    public function accountBalanceV2Get_0($date)
    {
        list($response) = $this->accountBalanceV2Get_0WithHttpInfo($date);
        return $response;
    }

    /**
     * Operation accountBalanceV2Get_0WithHttpInfo
     *
     * Get Account balance on a specific date (yyyy-MM-dd). Filter to include accounts where balance is 0.
     *
     * @param \DateTime $date Format: yyyy-MM-dd (required)
     * @return Array of \Trollweb\EaccountingApi\Model\PaginatedResponseAccountBalanceAPI, HTTP status code, HTTP response headers (array of strings)
     * @throws \Trollweb\EaccountingApi\ApiException on non-2xx response
     */
    public function accountBalanceV2Get_0WithHttpInfo($date)
    {
        // verify the required parameter 'date' is set
        if ($date === null) {
            throw new \InvalidArgumentException('Missing the required parameter $date when calling accountBalanceV2Get_0');
        }
        // parse inputs
        $resourcePath = "/v2/accountbalances/{date}";
        $httpBody = '';
        $queryParams = array();
        $headerParams = array();
        $formParams = array();
        $_header_accept = $this->apiClient->selectHeaderAccept(array('application/json', 'text/json', 'application/xml', 'text/xml'));
        if (!is_null($_header_accept)) {
            $headerParams['Accept'] = $_header_accept;
        }
        $headerParams['Content-Type'] = $this->apiClient->selectHeaderContentType(array());

        // path params
        if ($date !== null) {
            $resourcePath = str_replace(
                "{" . "date" . "}",
                $this->apiClient->getSerializer()->toPathValue($date),
                $resourcePath
            );
        }
        // default format to json
        $resourcePath = str_replace("{format}", "json", $resourcePath);

        
        // for model (json/xml)
        if (isset($_tempBody)) {
            $httpBody = $_tempBody; // $_tempBody is the method argument, if present
        } elseif (count($formParams) > 0) {
            $httpBody = $formParams; // for HTTP post (form)
        }
        // make the API Call
        try {
            list($response, $statusCode, $httpHeader) = $this->apiClient->callApi(
                $resourcePath,
                'GET',
                $queryParams,
                $httpBody,
                $headerParams,
                '\Trollweb\EaccountingApi\Model\PaginatedResponseAccountBalanceAPI',
                '/v2/accountbalances/{date}'
            );

            return array($this->apiClient->getSerializer()->deserialize($response, '\Trollweb\EaccountingApi\Model\PaginatedResponseAccountBalanceAPI', $httpHeader), $statusCode, $httpHeader);
        } catch (ApiException $e) {
            switch ($e->getCode()) {
                case 200:
                    $data = $this->apiClient->getSerializer()->deserialize($e->getResponseBody(), '\Trollweb\EaccountingApi\Model\PaginatedResponseAccountBalanceAPI', $e->getResponseHeaders());
                    $e->setResponseObject($data);
                    break;
            }

            throw $e;
        }
    }

}
