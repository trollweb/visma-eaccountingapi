<?php
/**
 * AllocationPeriodsVApi
 * PHP version 5
 *
 * @category Class
 * @package  Trollweb\EaccountingApi
 * @author   http://github.com/swagger-api/swagger-codegen
 * @license  http://www.apache.org/licenses/LICENSE-2.0 Apache Licene v2
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Visma eAccounting API V2
 *
 * No descripton provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v2
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace Trollweb\EaccountingApi\Api;

use \Trollweb\EaccountingApi\Configuration;
use \Trollweb\EaccountingApi\ApiClient;
use \Trollweb\EaccountingApi\ApiException;
use \Trollweb\EaccountingApi\ObjectSerializer;

/**
 * AllocationPeriodsVApi Class Doc Comment
 *
 * @category Class
 * @package  Trollweb\EaccountingApi
 * @author   http://github.com/swagger-api/swagger-codegen
 * @license  http://www.apache.org/licenses/LICENSE-2.0 Apache Licene v2
 * @link     https://github.com/swagger-api/swagger-codegen
 */
class AllocationPeriodsVApi
{

    /**
     * API Client
     *
     * @var \Trollweb\EaccountingApi\ApiClient instance of the ApiClient
     */
    protected $apiClient;

    /**
     * Constructor
     *
     * @param \Trollweb\EaccountingApi\ApiClient|null $apiClient The api client to use
     */
    public function __construct(\Trollweb\EaccountingApi\ApiClient $apiClient = null)
    {
        if ($apiClient == null) {
            $apiClient = new ApiClient();
            $apiClient->getConfig()->setHost('https://eaccountingapi-sandbox.test.vismaonline.com');
        }

        $this->apiClient = $apiClient;
    }

    /**
     * Get API client
     *
     * @return \Trollweb\EaccountingApi\ApiClient get the API client
     */
    public function getApiClient()
    {
        return $this->apiClient;
    }

    /**
     * Set the API client
     *
     * @param \Trollweb\EaccountingApi\ApiClient $apiClient set the API client
     *
     * @return AllocationPeriodsVApi
     */
    public function setApiClient(\Trollweb\EaccountingApi\ApiClient $apiClient)
    {
        $this->apiClient = $apiClient;
        return $this;
    }

    /**
     * Operation allocationPeriodsV2Get
     *
     * Get allocation periods.
     *
     * @return \Trollweb\EaccountingApi\Model\PaginatedResponseAllocationPeriodApi
     * @throws \Trollweb\EaccountingApi\ApiException on non-2xx response
     */
    public function allocationPeriodsV2Get()
    {
        list($response) = $this->allocationPeriodsV2GetWithHttpInfo();
        return $response;
    }

    /**
     * Operation allocationPeriodsV2GetWithHttpInfo
     *
     * Get allocation periods.
     *
     * @return Array of \Trollweb\EaccountingApi\Model\PaginatedResponseAllocationPeriodApi, HTTP status code, HTTP response headers (array of strings)
     * @throws \Trollweb\EaccountingApi\ApiException on non-2xx response
     */
    public function allocationPeriodsV2GetWithHttpInfo()
    {
        // parse inputs
        $resourcePath = "/v2/allocationperiods";
        $httpBody = '';
        $queryParams = array();
        $headerParams = array();
        $formParams = array();
        $_header_accept = $this->apiClient->selectHeaderAccept(array('application/json', 'text/json', 'application/xml', 'text/xml'));
        if (!is_null($_header_accept)) {
            $headerParams['Accept'] = $_header_accept;
        }
        $headerParams['Content-Type'] = $this->apiClient->selectHeaderContentType(array());

        // default format to json
        $resourcePath = str_replace("{format}", "json", $resourcePath);

        
        // for model (json/xml)
        if (isset($_tempBody)) {
            $httpBody = $_tempBody; // $_tempBody is the method argument, if present
        } elseif (count($formParams) > 0) {
            $httpBody = $formParams; // for HTTP post (form)
        }
        // make the API Call
        try {
            list($response, $statusCode, $httpHeader) = $this->apiClient->callApi(
                $resourcePath,
                'GET',
                $queryParams,
                $httpBody,
                $headerParams,
                '\Trollweb\EaccountingApi\Model\PaginatedResponseAllocationPeriodApi',
                '/v2/allocationperiods'
            );

            return array($this->apiClient->getSerializer()->deserialize($response, '\Trollweb\EaccountingApi\Model\PaginatedResponseAllocationPeriodApi', $httpHeader), $statusCode, $httpHeader);
        } catch (ApiException $e) {
            switch ($e->getCode()) {
                case 200:
                    $data = $this->apiClient->getSerializer()->deserialize($e->getResponseBody(), '\Trollweb\EaccountingApi\Model\PaginatedResponseAllocationPeriodApi', $e->getResponseHeaders());
                    $e->setResponseObject($data);
                    break;
            }

            throw $e;
        }
    }

    /**
     * Operation allocationPeriodsV2Get_0
     *
     * Get single allocation period.
     *
     * @param string $allocation_period_id Id of the requsted allocation period. (required)
     * @return \Trollweb\EaccountingApi\Model\AllocationPeriodApi
     * @throws \Trollweb\EaccountingApi\ApiException on non-2xx response
     */
    public function allocationPeriodsV2Get_0($allocation_period_id)
    {
        list($response) = $this->allocationPeriodsV2Get_0WithHttpInfo($allocation_period_id);
        return $response;
    }

    /**
     * Operation allocationPeriodsV2Get_0WithHttpInfo
     *
     * Get single allocation period.
     *
     * @param string $allocation_period_id Id of the requsted allocation period. (required)
     * @return Array of \Trollweb\EaccountingApi\Model\AllocationPeriodApi, HTTP status code, HTTP response headers (array of strings)
     * @throws \Trollweb\EaccountingApi\ApiException on non-2xx response
     */
    public function allocationPeriodsV2Get_0WithHttpInfo($allocation_period_id)
    {
        // verify the required parameter 'allocation_period_id' is set
        if ($allocation_period_id === null) {
            throw new \InvalidArgumentException('Missing the required parameter $allocation_period_id when calling allocationPeriodsV2Get_0');
        }
        // parse inputs
        $resourcePath = "/v2/allocationperiods/{allocationPeriodId}";
        $httpBody = '';
        $queryParams = array();
        $headerParams = array();
        $formParams = array();
        $_header_accept = $this->apiClient->selectHeaderAccept(array('application/json', 'text/json', 'application/xml', 'text/xml'));
        if (!is_null($_header_accept)) {
            $headerParams['Accept'] = $_header_accept;
        }
        $headerParams['Content-Type'] = $this->apiClient->selectHeaderContentType(array());

        // path params
        if ($allocation_period_id !== null) {
            $resourcePath = str_replace(
                "{" . "allocationPeriodId" . "}",
                $this->apiClient->getSerializer()->toPathValue($allocation_period_id),
                $resourcePath
            );
        }
        // default format to json
        $resourcePath = str_replace("{format}", "json", $resourcePath);

        
        // for model (json/xml)
        if (isset($_tempBody)) {
            $httpBody = $_tempBody; // $_tempBody is the method argument, if present
        } elseif (count($formParams) > 0) {
            $httpBody = $formParams; // for HTTP post (form)
        }
        // make the API Call
        try {
            list($response, $statusCode, $httpHeader) = $this->apiClient->callApi(
                $resourcePath,
                'GET',
                $queryParams,
                $httpBody,
                $headerParams,
                '\Trollweb\EaccountingApi\Model\AllocationPeriodApi',
                '/v2/allocationperiods/{allocationPeriodId}'
            );

            return array($this->apiClient->getSerializer()->deserialize($response, '\Trollweb\EaccountingApi\Model\AllocationPeriodApi', $httpHeader), $statusCode, $httpHeader);
        } catch (ApiException $e) {
            switch ($e->getCode()) {
                case 200:
                    $data = $this->apiClient->getSerializer()->deserialize($e->getResponseBody(), '\Trollweb\EaccountingApi\Model\AllocationPeriodApi', $e->getResponseHeaders());
                    $e->setResponseObject($data);
                    break;
            }

            throw $e;
        }
    }

    /**
     * Operation allocationPeriodsV2Post
     *
     * Add allocation periods for voucher or supplier invoice.
     *
     * @param \Trollweb\EaccountingApi\Model\AllocationPlan[] $allocation_plans  (required)
     * @return \Trollweb\EaccountingApi\Model\AllocationPeriodApi
     * @throws \Trollweb\EaccountingApi\ApiException on non-2xx response
     */
    public function allocationPeriodsV2Post($allocation_plans)
    {
        list($response) = $this->allocationPeriodsV2PostWithHttpInfo($allocation_plans);
        return $response;
    }

    /**
     * Operation allocationPeriodsV2PostWithHttpInfo
     *
     * Add allocation periods for voucher or supplier invoice.
     *
     * @param \Trollweb\EaccountingApi\Model\AllocationPlan[] $allocation_plans  (required)
     * @return Array of \Trollweb\EaccountingApi\Model\AllocationPeriodApi, HTTP status code, HTTP response headers (array of strings)
     * @throws \Trollweb\EaccountingApi\ApiException on non-2xx response
     */
    public function allocationPeriodsV2PostWithHttpInfo($allocation_plans)
    {
        // verify the required parameter 'allocation_plans' is set
        if ($allocation_plans === null) {
            throw new \InvalidArgumentException('Missing the required parameter $allocation_plans when calling allocationPeriodsV2Post');
        }
        // parse inputs
        $resourcePath = "/v2/allocationperiods";
        $httpBody = '';
        $queryParams = array();
        $headerParams = array();
        $formParams = array();
        $_header_accept = $this->apiClient->selectHeaderAccept(array('application/json', 'text/json', 'application/xml', 'text/xml'));
        if (!is_null($_header_accept)) {
            $headerParams['Accept'] = $_header_accept;
        }
        $headerParams['Content-Type'] = $this->apiClient->selectHeaderContentType(array('application/json','text/json','application/xml','text/xml','application/x-www-form-urlencoded'));

        // default format to json
        $resourcePath = str_replace("{format}", "json", $resourcePath);

        // body params
        $_tempBody = null;
        if (isset($allocation_plans)) {
            $_tempBody = $allocation_plans;
        }

        // for model (json/xml)
        if (isset($_tempBody)) {
            $httpBody = $_tempBody; // $_tempBody is the method argument, if present
        } elseif (count($formParams) > 0) {
            $httpBody = $formParams; // for HTTP post (form)
        }
        // make the API Call
        try {
            list($response, $statusCode, $httpHeader) = $this->apiClient->callApi(
                $resourcePath,
                'POST',
                $queryParams,
                $httpBody,
                $headerParams,
                '\Trollweb\EaccountingApi\Model\AllocationPeriodApi',
                '/v2/allocationperiods'
            );

            return array($this->apiClient->getSerializer()->deserialize($response, '\Trollweb\EaccountingApi\Model\AllocationPeriodApi', $httpHeader), $statusCode, $httpHeader);
        } catch (ApiException $e) {
            switch ($e->getCode()) {
                case 200:
                    $data = $this->apiClient->getSerializer()->deserialize($e->getResponseBody(), '\Trollweb\EaccountingApi\Model\AllocationPeriodApi', $e->getResponseHeaders());
                    $e->setResponseObject($data);
                    break;
            }

            throw $e;
        }
    }

}
