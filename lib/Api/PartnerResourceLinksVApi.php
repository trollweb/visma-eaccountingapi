<?php
/**
 * PartnerResourceLinksVApi
 * PHP version 5
 *
 * @category Class
 * @package  Trollweb\EaccountingApi
 * @author   http://github.com/swagger-api/swagger-codegen
 * @license  http://www.apache.org/licenses/LICENSE-2.0 Apache Licene v2
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Visma eAccounting API V2
 *
 * No descripton provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v2
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace Trollweb\EaccountingApi\Api;

use \Trollweb\EaccountingApi\Configuration;
use \Trollweb\EaccountingApi\ApiClient;
use \Trollweb\EaccountingApi\ApiException;
use \Trollweb\EaccountingApi\ObjectSerializer;

/**
 * PartnerResourceLinksVApi Class Doc Comment
 *
 * @category Class
 * @package  Trollweb\EaccountingApi
 * @author   http://github.com/swagger-api/swagger-codegen
 * @license  http://www.apache.org/licenses/LICENSE-2.0 Apache Licene v2
 * @link     https://github.com/swagger-api/swagger-codegen
 */
class PartnerResourceLinksVApi
{

    /**
     * API Client
     *
     * @var \Trollweb\EaccountingApi\ApiClient instance of the ApiClient
     */
    protected $apiClient;

    /**
     * Constructor
     *
     * @param \Trollweb\EaccountingApi\ApiClient|null $apiClient The api client to use
     */
    public function __construct(\Trollweb\EaccountingApi\ApiClient $apiClient = null)
    {
        if ($apiClient == null) {
            $apiClient = new ApiClient();
            $apiClient->getConfig()->setHost('https://eaccountingapi-sandbox.test.vismaonline.com');
        }

        $this->apiClient = $apiClient;
    }

    /**
     * Get API client
     *
     * @return \Trollweb\EaccountingApi\ApiClient get the API client
     */
    public function getApiClient()
    {
        return $this->apiClient;
    }

    /**
     * Set the API client
     *
     * @param \Trollweb\EaccountingApi\ApiClient $apiClient set the API client
     *
     * @return PartnerResourceLinksVApi
     */
    public function setApiClient(\Trollweb\EaccountingApi\ApiClient $apiClient)
    {
        $this->apiClient = $apiClient;
        return $this;
    }

    /**
     * Operation partnerResourceLinksV2Delete
     *
     * Delete a partner resource link
     *
     * @param string $partner_resource_link_id  (required)
     * @return object
     * @throws \Trollweb\EaccountingApi\ApiException on non-2xx response
     */
    public function partnerResourceLinksV2Delete($partner_resource_link_id)
    {
        list($response) = $this->partnerResourceLinksV2DeleteWithHttpInfo($partner_resource_link_id);
        return $response;
    }

    /**
     * Operation partnerResourceLinksV2DeleteWithHttpInfo
     *
     * Delete a partner resource link
     *
     * @param string $partner_resource_link_id  (required)
     * @return Array of object, HTTP status code, HTTP response headers (array of strings)
     * @throws \Trollweb\EaccountingApi\ApiException on non-2xx response
     */
    public function partnerResourceLinksV2DeleteWithHttpInfo($partner_resource_link_id)
    {
        // verify the required parameter 'partner_resource_link_id' is set
        if ($partner_resource_link_id === null) {
            throw new \InvalidArgumentException('Missing the required parameter $partner_resource_link_id when calling partnerResourceLinksV2Delete');
        }
        // parse inputs
        $resourcePath = "/v2/partnerresourcelinks/{partnerResourceLinkId}";
        $httpBody = '';
        $queryParams = array();
        $headerParams = array();
        $formParams = array();
        $_header_accept = $this->apiClient->selectHeaderAccept(array('application/json', 'text/json', 'application/xml', 'text/xml'));
        if (!is_null($_header_accept)) {
            $headerParams['Accept'] = $_header_accept;
        }
        $headerParams['Content-Type'] = $this->apiClient->selectHeaderContentType(array());

        // path params
        if ($partner_resource_link_id !== null) {
            $resourcePath = str_replace(
                "{" . "partnerResourceLinkId" . "}",
                $this->apiClient->getSerializer()->toPathValue($partner_resource_link_id),
                $resourcePath
            );
        }
        // default format to json
        $resourcePath = str_replace("{format}", "json", $resourcePath);

        
        // for model (json/xml)
        if (isset($_tempBody)) {
            $httpBody = $_tempBody; // $_tempBody is the method argument, if present
        } elseif (count($formParams) > 0) {
            $httpBody = $formParams; // for HTTP post (form)
        }
        // make the API Call
        try {
            list($response, $statusCode, $httpHeader) = $this->apiClient->callApi(
                $resourcePath,
                'DELETE',
                $queryParams,
                $httpBody,
                $headerParams,
                'object',
                '/v2/partnerresourcelinks/{partnerResourceLinkId}'
            );

            return array($this->apiClient->getSerializer()->deserialize($response, 'object', $httpHeader), $statusCode, $httpHeader);
        } catch (ApiException $e) {
            switch ($e->getCode()) {
                case 200:
                    $data = $this->apiClient->getSerializer()->deserialize($e->getResponseBody(), 'object', $e->getResponseHeaders());
                    $e->setResponseObject($data);
                    break;
            }

            throw $e;
        }
    }

    /**
     * Operation partnerResourceLinksV2Get
     *
     * Get a list of partner resource links.
     *
     * @return \Trollweb\EaccountingApi\Model\PaginatedResponsePartnerResourceLinkApi
     * @throws \Trollweb\EaccountingApi\ApiException on non-2xx response
     */
    public function partnerResourceLinksV2Get()
    {
        list($response) = $this->partnerResourceLinksV2GetWithHttpInfo();
        return $response;
    }

    /**
     * Operation partnerResourceLinksV2GetWithHttpInfo
     *
     * Get a list of partner resource links.
     *
     * @return Array of \Trollweb\EaccountingApi\Model\PaginatedResponsePartnerResourceLinkApi, HTTP status code, HTTP response headers (array of strings)
     * @throws \Trollweb\EaccountingApi\ApiException on non-2xx response
     */
    public function partnerResourceLinksV2GetWithHttpInfo()
    {
        // parse inputs
        $resourcePath = "/v2/partnerresourcelinks";
        $httpBody = '';
        $queryParams = array();
        $headerParams = array();
        $formParams = array();
        $_header_accept = $this->apiClient->selectHeaderAccept(array('application/json', 'text/json', 'application/xml', 'text/xml'));
        if (!is_null($_header_accept)) {
            $headerParams['Accept'] = $_header_accept;
        }
        $headerParams['Content-Type'] = $this->apiClient->selectHeaderContentType(array());

        // default format to json
        $resourcePath = str_replace("{format}", "json", $resourcePath);

        
        // for model (json/xml)
        if (isset($_tempBody)) {
            $httpBody = $_tempBody; // $_tempBody is the method argument, if present
        } elseif (count($formParams) > 0) {
            $httpBody = $formParams; // for HTTP post (form)
        }
        // make the API Call
        try {
            list($response, $statusCode, $httpHeader) = $this->apiClient->callApi(
                $resourcePath,
                'GET',
                $queryParams,
                $httpBody,
                $headerParams,
                '\Trollweb\EaccountingApi\Model\PaginatedResponsePartnerResourceLinkApi',
                '/v2/partnerresourcelinks'
            );

            return array($this->apiClient->getSerializer()->deserialize($response, '\Trollweb\EaccountingApi\Model\PaginatedResponsePartnerResourceLinkApi', $httpHeader), $statusCode, $httpHeader);
        } catch (ApiException $e) {
            switch ($e->getCode()) {
                case 200:
                    $data = $this->apiClient->getSerializer()->deserialize($e->getResponseBody(), '\Trollweb\EaccountingApi\Model\PaginatedResponsePartnerResourceLinkApi', $e->getResponseHeaders());
                    $e->setResponseObject($data);
                    break;
            }

            throw $e;
        }
    }

    /**
     * Operation partnerResourceLinksV2Get_0
     *
     * Get a partner resource link by id.
     *
     * @param string $partner_resource_link_id  (required)
     * @return \Trollweb\EaccountingApi\Model\PartnerResourceLinkApi
     * @throws \Trollweb\EaccountingApi\ApiException on non-2xx response
     */
    public function partnerResourceLinksV2Get_0($partner_resource_link_id)
    {
        list($response) = $this->partnerResourceLinksV2Get_0WithHttpInfo($partner_resource_link_id);
        return $response;
    }

    /**
     * Operation partnerResourceLinksV2Get_0WithHttpInfo
     *
     * Get a partner resource link by id.
     *
     * @param string $partner_resource_link_id  (required)
     * @return Array of \Trollweb\EaccountingApi\Model\PartnerResourceLinkApi, HTTP status code, HTTP response headers (array of strings)
     * @throws \Trollweb\EaccountingApi\ApiException on non-2xx response
     */
    public function partnerResourceLinksV2Get_0WithHttpInfo($partner_resource_link_id)
    {
        // verify the required parameter 'partner_resource_link_id' is set
        if ($partner_resource_link_id === null) {
            throw new \InvalidArgumentException('Missing the required parameter $partner_resource_link_id when calling partnerResourceLinksV2Get_0');
        }
        // parse inputs
        $resourcePath = "/v2/partnerresourcelinks/{partnerResourceLinkId}";
        $httpBody = '';
        $queryParams = array();
        $headerParams = array();
        $formParams = array();
        $_header_accept = $this->apiClient->selectHeaderAccept(array('application/json', 'text/json', 'application/xml', 'text/xml'));
        if (!is_null($_header_accept)) {
            $headerParams['Accept'] = $_header_accept;
        }
        $headerParams['Content-Type'] = $this->apiClient->selectHeaderContentType(array());

        // path params
        if ($partner_resource_link_id !== null) {
            $resourcePath = str_replace(
                "{" . "partnerResourceLinkId" . "}",
                $this->apiClient->getSerializer()->toPathValue($partner_resource_link_id),
                $resourcePath
            );
        }
        // default format to json
        $resourcePath = str_replace("{format}", "json", $resourcePath);

        
        // for model (json/xml)
        if (isset($_tempBody)) {
            $httpBody = $_tempBody; // $_tempBody is the method argument, if present
        } elseif (count($formParams) > 0) {
            $httpBody = $formParams; // for HTTP post (form)
        }
        // make the API Call
        try {
            list($response, $statusCode, $httpHeader) = $this->apiClient->callApi(
                $resourcePath,
                'GET',
                $queryParams,
                $httpBody,
                $headerParams,
                '\Trollweb\EaccountingApi\Model\PartnerResourceLinkApi',
                '/v2/partnerresourcelinks/{partnerResourceLinkId}'
            );

            return array($this->apiClient->getSerializer()->deserialize($response, '\Trollweb\EaccountingApi\Model\PartnerResourceLinkApi', $httpHeader), $statusCode, $httpHeader);
        } catch (ApiException $e) {
            switch ($e->getCode()) {
                case 200:
                    $data = $this->apiClient->getSerializer()->deserialize($e->getResponseBody(), '\Trollweb\EaccountingApi\Model\PartnerResourceLinkApi', $e->getResponseHeaders());
                    $e->setResponseObject($data);
                    break;
            }

            throw $e;
        }
    }

    /**
     * Operation partnerResourceLinksV2Post
     *
     * Create a partner resource link
     *
     * @param \Trollweb\EaccountingApi\Model\PartnerResourceLinkApi $partner_resource_link  (required)
     * @return \Trollweb\EaccountingApi\Model\PartnerResourceLinkApi
     * @throws \Trollweb\EaccountingApi\ApiException on non-2xx response
     */
    public function partnerResourceLinksV2Post($partner_resource_link)
    {
        list($response) = $this->partnerResourceLinksV2PostWithHttpInfo($partner_resource_link);
        return $response;
    }

    /**
     * Operation partnerResourceLinksV2PostWithHttpInfo
     *
     * Create a partner resource link
     *
     * @param \Trollweb\EaccountingApi\Model\PartnerResourceLinkApi $partner_resource_link  (required)
     * @return Array of \Trollweb\EaccountingApi\Model\PartnerResourceLinkApi, HTTP status code, HTTP response headers (array of strings)
     * @throws \Trollweb\EaccountingApi\ApiException on non-2xx response
     */
    public function partnerResourceLinksV2PostWithHttpInfo($partner_resource_link)
    {
        // verify the required parameter 'partner_resource_link' is set
        if ($partner_resource_link === null) {
            throw new \InvalidArgumentException('Missing the required parameter $partner_resource_link when calling partnerResourceLinksV2Post');
        }
        // parse inputs
        $resourcePath = "/v2/partnerresourcelinks";
        $httpBody = '';
        $queryParams = array();
        $headerParams = array();
        $formParams = array();
        $_header_accept = $this->apiClient->selectHeaderAccept(array('application/json', 'text/json', 'application/xml', 'text/xml'));
        if (!is_null($_header_accept)) {
            $headerParams['Accept'] = $_header_accept;
        }
        $headerParams['Content-Type'] = $this->apiClient->selectHeaderContentType(array('application/json','text/json','application/xml','text/xml','application/x-www-form-urlencoded'));

        // default format to json
        $resourcePath = str_replace("{format}", "json", $resourcePath);

        // body params
        $_tempBody = null;
        if (isset($partner_resource_link)) {
            $_tempBody = $partner_resource_link;
        }

        // for model (json/xml)
        if (isset($_tempBody)) {
            $httpBody = $_tempBody; // $_tempBody is the method argument, if present
        } elseif (count($formParams) > 0) {
            $httpBody = $formParams; // for HTTP post (form)
        }
        // make the API Call
        try {
            list($response, $statusCode, $httpHeader) = $this->apiClient->callApi(
                $resourcePath,
                'POST',
                $queryParams,
                $httpBody,
                $headerParams,
                '\Trollweb\EaccountingApi\Model\PartnerResourceLinkApi',
                '/v2/partnerresourcelinks'
            );

            return array($this->apiClient->getSerializer()->deserialize($response, '\Trollweb\EaccountingApi\Model\PartnerResourceLinkApi', $httpHeader), $statusCode, $httpHeader);
        } catch (ApiException $e) {
            switch ($e->getCode()) {
                case 201:
                    $data = $this->apiClient->getSerializer()->deserialize($e->getResponseBody(), '\Trollweb\EaccountingApi\Model\PartnerResourceLinkApi', $e->getResponseHeaders());
                    $e->setResponseObject($data);
                    break;
            }

            throw $e;
        }
    }

    /**
     * Operation partnerResourceLinksV2Put
     *
     * Update a partner resource link
     *
     * @param string $partner_resource_link_id  (required)
     * @param \Trollweb\EaccountingApi\Model\PartnerResourceLinkApi $partner_resource_link  (required)
     * @return \Trollweb\EaccountingApi\Model\PartnerResourceLinkApi
     * @throws \Trollweb\EaccountingApi\ApiException on non-2xx response
     */
    public function partnerResourceLinksV2Put($partner_resource_link_id, $partner_resource_link)
    {
        list($response) = $this->partnerResourceLinksV2PutWithHttpInfo($partner_resource_link_id, $partner_resource_link);
        return $response;
    }

    /**
     * Operation partnerResourceLinksV2PutWithHttpInfo
     *
     * Update a partner resource link
     *
     * @param string $partner_resource_link_id  (required)
     * @param \Trollweb\EaccountingApi\Model\PartnerResourceLinkApi $partner_resource_link  (required)
     * @return Array of \Trollweb\EaccountingApi\Model\PartnerResourceLinkApi, HTTP status code, HTTP response headers (array of strings)
     * @throws \Trollweb\EaccountingApi\ApiException on non-2xx response
     */
    public function partnerResourceLinksV2PutWithHttpInfo($partner_resource_link_id, $partner_resource_link)
    {
        // verify the required parameter 'partner_resource_link_id' is set
        if ($partner_resource_link_id === null) {
            throw new \InvalidArgumentException('Missing the required parameter $partner_resource_link_id when calling partnerResourceLinksV2Put');
        }
        // verify the required parameter 'partner_resource_link' is set
        if ($partner_resource_link === null) {
            throw new \InvalidArgumentException('Missing the required parameter $partner_resource_link when calling partnerResourceLinksV2Put');
        }
        // parse inputs
        $resourcePath = "/v2/partnerresourcelinks/{partnerResourceLinkId}";
        $httpBody = '';
        $queryParams = array();
        $headerParams = array();
        $formParams = array();
        $_header_accept = $this->apiClient->selectHeaderAccept(array('application/json', 'text/json', 'application/xml', 'text/xml'));
        if (!is_null($_header_accept)) {
            $headerParams['Accept'] = $_header_accept;
        }
        $headerParams['Content-Type'] = $this->apiClient->selectHeaderContentType(array('application/json','text/json','application/xml','text/xml','application/x-www-form-urlencoded'));

        // path params
        if ($partner_resource_link_id !== null) {
            $resourcePath = str_replace(
                "{" . "partnerResourceLinkId" . "}",
                $this->apiClient->getSerializer()->toPathValue($partner_resource_link_id),
                $resourcePath
            );
        }
        // default format to json
        $resourcePath = str_replace("{format}", "json", $resourcePath);

        // body params
        $_tempBody = null;
        if (isset($partner_resource_link)) {
            $_tempBody = $partner_resource_link;
        }

        // for model (json/xml)
        if (isset($_tempBody)) {
            $httpBody = $_tempBody; // $_tempBody is the method argument, if present
        } elseif (count($formParams) > 0) {
            $httpBody = $formParams; // for HTTP post (form)
        }
        // make the API Call
        try {
            list($response, $statusCode, $httpHeader) = $this->apiClient->callApi(
                $resourcePath,
                'PUT',
                $queryParams,
                $httpBody,
                $headerParams,
                '\Trollweb\EaccountingApi\Model\PartnerResourceLinkApi',
                '/v2/partnerresourcelinks/{partnerResourceLinkId}'
            );

            return array($this->apiClient->getSerializer()->deserialize($response, '\Trollweb\EaccountingApi\Model\PartnerResourceLinkApi', $httpHeader), $statusCode, $httpHeader);
        } catch (ApiException $e) {
            switch ($e->getCode()) {
                case 200:
                    $data = $this->apiClient->getSerializer()->deserialize($e->getResponseBody(), '\Trollweb\EaccountingApi\Model\PartnerResourceLinkApi', $e->getResponseHeaders());
                    $e->setResponseObject($data);
                    break;
            }

            throw $e;
        }
    }

}
