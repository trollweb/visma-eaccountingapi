<?php
/**
 * OrderByClause
 *
 * PHP version 5
 *
 * @category Class
 * @package  Trollweb\EaccountingApi
 * @author   http://github.com/swagger-api/swagger-codegen
 * @license  http://www.apache.org/licenses/LICENSE-2.0 Apache Licene v2
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Visma eAccounting API V2
 *
 * No descripton provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v2
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace Trollweb\EaccountingApi\Model;

use \ArrayAccess;

/**
 * OrderByClause Class Doc Comment
 *
 * @category    Class */
/** 
 * @package     Trollweb\EaccountingApi
 * @author      http://github.com/swagger-api/swagger-codegen
 * @license     http://www.apache.org/licenses/LICENSE-2.0 Apache Licene v2
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class OrderByClause implements ArrayAccess
{
    /**
      * The original name of the model.
      * @var string
      */
    protected static $swaggerModelName = 'OrderByClause';

    /**
      * Array of property to type mappings. Used for (de)serialization
      * @var string[]
      */
    protected static $swaggerTypes = array(
        'then_by' => '\Trollweb\EaccountingApi\Model\OrderByClause',
        'expression' => '\Trollweb\EaccountingApi\Model\SingleValueNode',
        'direction' => 'int',
        'range_variable' => '\Trollweb\EaccountingApi\Model\RangeVariable',
        'item_type' => '\Trollweb\EaccountingApi\Model\IEdmTypeReference'
    );

    public static function swaggerTypes()
    {
        return self::$swaggerTypes;
    }

    /**
     * Array of attributes where the key is the local name, and the value is the original name
     * @var string[]
     */
    protected static $attributeMap = array(
        'then_by' => 'ThenBy',
        'expression' => 'Expression',
        'direction' => 'Direction',
        'range_variable' => 'RangeVariable',
        'item_type' => 'ItemType'
    );

    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     * @var string[]
     */
    protected static $setters = array(
        'then_by' => 'setThenBy',
        'expression' => 'setExpression',
        'direction' => 'setDirection',
        'range_variable' => 'setRangeVariable',
        'item_type' => 'setItemType'
    );

    public static function setters()
    {
        return self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     * @var string[]
     */
    protected static $getters = array(
        'then_by' => 'getThenBy',
        'expression' => 'getExpression',
        'direction' => 'getDirection',
        'range_variable' => 'getRangeVariable',
        'item_type' => 'getItemType'
    );

    public static function getters()
    {
        return self::$getters;
    }

    const DIRECTION_0 = 0;
    const DIRECTION_1 = 1;
    

    
    /**
     * Gets allowable values of the enum
     * @return string[]
     */
    public function getDirectionAllowableValues()
    {
        return [
            self::DIRECTION_0,
            self::DIRECTION_1,
        ];
    }
    

    /**
     * Associative array for storing property values
     * @var mixed[]
     */
    protected $container = array();

    /**
     * Constructor
     * @param mixed[] $data Associated array of property value initalizing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['then_by'] = isset($data['then_by']) ? $data['then_by'] : null;
        $this->container['expression'] = isset($data['expression']) ? $data['expression'] : null;
        $this->container['direction'] = isset($data['direction']) ? $data['direction'] : null;
        $this->container['range_variable'] = isset($data['range_variable']) ? $data['range_variable'] : null;
        $this->container['item_type'] = isset($data['item_type']) ? $data['item_type'] : null;
    }

    /**
     * show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalid_properties = array();
        $allowed_values = array("0", "1");
        if (!in_array($this->container['direction'], $allowed_values)) {
            $invalid_properties[] = "invalid value for 'direction', must be one of #{allowed_values}.";
        }

        return $invalid_properties;
    }

    /**
     * validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properteis are valid
     */
    public function valid()
    {
        $allowed_values = array("0", "1");
        if (!in_array($this->container['direction'], $allowed_values)) {
            return false;
        }
        return true;
    }


    /**
     * Gets then_by
     * @return \Trollweb\EaccountingApi\Model\OrderByClause
     */
    public function getThenBy()
    {
        return $this->container['then_by'];
    }

    /**
     * Sets then_by
     * @param \Trollweb\EaccountingApi\Model\OrderByClause $then_by
     * @return $this
     */
    public function setThenBy($then_by)
    {
        $this->container['then_by'] = $then_by;

        return $this;
    }

    /**
     * Gets expression
     * @return \Trollweb\EaccountingApi\Model\SingleValueNode
     */
    public function getExpression()
    {
        return $this->container['expression'];
    }

    /**
     * Sets expression
     * @param \Trollweb\EaccountingApi\Model\SingleValueNode $expression
     * @return $this
     */
    public function setExpression($expression)
    {
        $this->container['expression'] = $expression;

        return $this;
    }

    /**
     * Gets direction
     * @return int
     */
    public function getDirection()
    {
        return $this->container['direction'];
    }

    /**
     * Sets direction
     * @param int $direction
     * @return $this
     */
    public function setDirection($direction)
    {
        $allowed_values = array('0', '1');
        if (!in_array($direction, $allowed_values)) {
            throw new \InvalidArgumentException("Invalid value for 'direction', must be one of '0', '1'");
        }
        $this->container['direction'] = $direction;

        return $this;
    }

    /**
     * Gets range_variable
     * @return \Trollweb\EaccountingApi\Model\RangeVariable
     */
    public function getRangeVariable()
    {
        return $this->container['range_variable'];
    }

    /**
     * Sets range_variable
     * @param \Trollweb\EaccountingApi\Model\RangeVariable $range_variable
     * @return $this
     */
    public function setRangeVariable($range_variable)
    {
        $this->container['range_variable'] = $range_variable;

        return $this;
    }

    /**
     * Gets item_type
     * @return \Trollweb\EaccountingApi\Model\IEdmTypeReference
     */
    public function getItemType()
    {
        return $this->container['item_type'];
    }

    /**
     * Sets item_type
     * @param \Trollweb\EaccountingApi\Model\IEdmTypeReference $item_type
     * @return $this
     */
    public function setItemType($item_type)
    {
        $this->container['item_type'] = $item_type;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     * @param  integer $offset Offset
     * @return boolean
     */
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     * @param  integer $offset Offset
     * @return mixed
     */
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     * @param  integer $offset Offset
     * @param  mixed   $value  Value to be set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     * @param  integer $offset Offset
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     * @return string
     */
    public function __toString()
    {
        if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
            return json_encode(\Trollweb\EaccountingApi\ObjectSerializer::sanitizeForSerialization($this), JSON_PRETTY_PRINT);
        }

        return json_encode(\Trollweb\EaccountingApi\ObjectSerializer::sanitizeForSerialization($this));
    }
}


