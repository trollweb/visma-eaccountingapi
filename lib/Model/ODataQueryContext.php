<?php
/**
 * ODataQueryContext
 *
 * PHP version 5
 *
 * @category Class
 * @package  Trollweb\EaccountingApi
 * @author   http://github.com/swagger-api/swagger-codegen
 * @license  http://www.apache.org/licenses/LICENSE-2.0 Apache Licene v2
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Visma eAccounting API V2
 *
 * No descripton provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v2
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace Trollweb\EaccountingApi\Model;

use \ArrayAccess;

/**
 * ODataQueryContext Class Doc Comment
 *
 * @category    Class */
/** 
 * @package     Trollweb\EaccountingApi
 * @author      http://github.com/swagger-api/swagger-codegen
 * @license     http://www.apache.org/licenses/LICENSE-2.0 Apache Licene v2
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class ODataQueryContext implements ArrayAccess
{
    /**
      * The original name of the model.
      * @var string
      */
    protected static $swaggerModelName = 'ODataQueryContext';

    /**
      * Array of property to type mappings. Used for (de)serialization
      * @var string[]
      */
    protected static $swaggerTypes = array(
        'default_query_settings' => '\Trollweb\EaccountingApi\Model\DefaultQuerySettings',
        'model' => '\Trollweb\EaccountingApi\Model\IEdmModel',
        'element_type' => '\Trollweb\EaccountingApi\Model\IEdmType',
        'navigation_source' => '\Trollweb\EaccountingApi\Model\IEdmNavigationSource',
        'element_clr_type' => 'string',
        'path' => '\Trollweb\EaccountingApi\Model\ODataPath',
        'request_container' => '\Trollweb\EaccountingApi\Model\IServiceProvider'
    );

    public static function swaggerTypes()
    {
        return self::$swaggerTypes;
    }

    /**
     * Array of attributes where the key is the local name, and the value is the original name
     * @var string[]
     */
    protected static $attributeMap = array(
        'default_query_settings' => 'DefaultQuerySettings',
        'model' => 'Model',
        'element_type' => 'ElementType',
        'navigation_source' => 'NavigationSource',
        'element_clr_type' => 'ElementClrType',
        'path' => 'Path',
        'request_container' => 'RequestContainer'
    );

    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     * @var string[]
     */
    protected static $setters = array(
        'default_query_settings' => 'setDefaultQuerySettings',
        'model' => 'setModel',
        'element_type' => 'setElementType',
        'navigation_source' => 'setNavigationSource',
        'element_clr_type' => 'setElementClrType',
        'path' => 'setPath',
        'request_container' => 'setRequestContainer'
    );

    public static function setters()
    {
        return self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     * @var string[]
     */
    protected static $getters = array(
        'default_query_settings' => 'getDefaultQuerySettings',
        'model' => 'getModel',
        'element_type' => 'getElementType',
        'navigation_source' => 'getNavigationSource',
        'element_clr_type' => 'getElementClrType',
        'path' => 'getPath',
        'request_container' => 'getRequestContainer'
    );

    public static function getters()
    {
        return self::$getters;
    }

    

    

    /**
     * Associative array for storing property values
     * @var mixed[]
     */
    protected $container = array();

    /**
     * Constructor
     * @param mixed[] $data Associated array of property value initalizing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['default_query_settings'] = isset($data['default_query_settings']) ? $data['default_query_settings'] : null;
        $this->container['model'] = isset($data['model']) ? $data['model'] : null;
        $this->container['element_type'] = isset($data['element_type']) ? $data['element_type'] : null;
        $this->container['navigation_source'] = isset($data['navigation_source']) ? $data['navigation_source'] : null;
        $this->container['element_clr_type'] = isset($data['element_clr_type']) ? $data['element_clr_type'] : null;
        $this->container['path'] = isset($data['path']) ? $data['path'] : null;
        $this->container['request_container'] = isset($data['request_container']) ? $data['request_container'] : null;
    }

    /**
     * show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalid_properties = array();
        return $invalid_properties;
    }

    /**
     * validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properteis are valid
     */
    public function valid()
    {
        return true;
    }


    /**
     * Gets default_query_settings
     * @return \Trollweb\EaccountingApi\Model\DefaultQuerySettings
     */
    public function getDefaultQuerySettings()
    {
        return $this->container['default_query_settings'];
    }

    /**
     * Sets default_query_settings
     * @param \Trollweb\EaccountingApi\Model\DefaultQuerySettings $default_query_settings
     * @return $this
     */
    public function setDefaultQuerySettings($default_query_settings)
    {
        $this->container['default_query_settings'] = $default_query_settings;

        return $this;
    }

    /**
     * Gets model
     * @return \Trollweb\EaccountingApi\Model\IEdmModel
     */
    public function getModel()
    {
        return $this->container['model'];
    }

    /**
     * Sets model
     * @param \Trollweb\EaccountingApi\Model\IEdmModel $model
     * @return $this
     */
    public function setModel($model)
    {
        $this->container['model'] = $model;

        return $this;
    }

    /**
     * Gets element_type
     * @return \Trollweb\EaccountingApi\Model\IEdmType
     */
    public function getElementType()
    {
        return $this->container['element_type'];
    }

    /**
     * Sets element_type
     * @param \Trollweb\EaccountingApi\Model\IEdmType $element_type
     * @return $this
     */
    public function setElementType($element_type)
    {
        $this->container['element_type'] = $element_type;

        return $this;
    }

    /**
     * Gets navigation_source
     * @return \Trollweb\EaccountingApi\Model\IEdmNavigationSource
     */
    public function getNavigationSource()
    {
        return $this->container['navigation_source'];
    }

    /**
     * Sets navigation_source
     * @param \Trollweb\EaccountingApi\Model\IEdmNavigationSource $navigation_source
     * @return $this
     */
    public function setNavigationSource($navigation_source)
    {
        $this->container['navigation_source'] = $navigation_source;

        return $this;
    }

    /**
     * Gets element_clr_type
     * @return string
     */
    public function getElementClrType()
    {
        return $this->container['element_clr_type'];
    }

    /**
     * Sets element_clr_type
     * @param string $element_clr_type
     * @return $this
     */
    public function setElementClrType($element_clr_type)
    {
        $this->container['element_clr_type'] = $element_clr_type;

        return $this;
    }

    /**
     * Gets path
     * @return \Trollweb\EaccountingApi\Model\ODataPath
     */
    public function getPath()
    {
        return $this->container['path'];
    }

    /**
     * Sets path
     * @param \Trollweb\EaccountingApi\Model\ODataPath $path
     * @return $this
     */
    public function setPath($path)
    {
        $this->container['path'] = $path;

        return $this;
    }

    /**
     * Gets request_container
     * @return \Trollweb\EaccountingApi\Model\IServiceProvider
     */
    public function getRequestContainer()
    {
        return $this->container['request_container'];
    }

    /**
     * Sets request_container
     * @param \Trollweb\EaccountingApi\Model\IServiceProvider $request_container
     * @return $this
     */
    public function setRequestContainer($request_container)
    {
        $this->container['request_container'] = $request_container;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     * @param  integer $offset Offset
     * @return boolean
     */
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     * @param  integer $offset Offset
     * @return mixed
     */
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     * @param  integer $offset Offset
     * @param  mixed   $value  Value to be set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     * @param  integer $offset Offset
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     * @return string
     */
    public function __toString()
    {
        if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
            return json_encode(\Trollweb\EaccountingApi\ObjectSerializer::sanitizeForSerialization($this), JSON_PRETTY_PRINT);
        }

        return json_encode(\Trollweb\EaccountingApi\ObjectSerializer::sanitizeForSerialization($this));
    }
}


