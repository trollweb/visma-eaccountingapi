<?php
/**
 * AttachmentResultApi
 *
 * PHP version 5
 *
 * @category Class
 * @package  Trollweb\EaccountingApi
 * @author   http://github.com/swagger-api/swagger-codegen
 * @license  http://www.apache.org/licenses/LICENSE-2.0 Apache Licene v2
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Visma eAccounting API V2
 *
 * No descripton provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v2
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace Trollweb\EaccountingApi\Model;

use \ArrayAccess;

/**
 * AttachmentResultApi Class Doc Comment
 *
 * @category    Class */
/** 
 * @package     Trollweb\EaccountingApi
 * @author      http://github.com/swagger-api/swagger-codegen
 * @license     http://www.apache.org/licenses/LICENSE-2.0 Apache Licene v2
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class AttachmentResultApi implements ArrayAccess
{
    /**
      * The original name of the model.
      * @var string
      */
    protected static $swaggerModelName = 'AttachmentResultApi';

    /**
      * Array of property to type mappings. Used for (de)serialization
      * @var string[]
      */
    protected static $swaggerTypes = array(
        'id' => 'string',
        'content_type' => 'string',
        'document_id' => 'string',
        'attached_document_type' => 'int',
        'file_name' => 'string',
        'temporary_url' => 'string',
        'comment' => 'string',
        'supplier_name' => 'string',
        'amount_invoice_currency' => 'double',
        'type' => 'int',
        'attachment_status' => 'int'
    );

    public static function swaggerTypes()
    {
        return self::$swaggerTypes;
    }

    /**
     * Array of attributes where the key is the local name, and the value is the original name
     * @var string[]
     */
    protected static $attributeMap = array(
        'id' => 'Id',
        'content_type' => 'ContentType',
        'document_id' => 'DocumentId',
        'attached_document_type' => 'AttachedDocumentType',
        'file_name' => 'FileName',
        'temporary_url' => 'TemporaryUrl',
        'comment' => 'Comment',
        'supplier_name' => 'SupplierName',
        'amount_invoice_currency' => 'AmountInvoiceCurrency',
        'type' => 'Type',
        'attachment_status' => 'AttachmentStatus'
    );

    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     * @var string[]
     */
    protected static $setters = array(
        'id' => 'setId',
        'content_type' => 'setContentType',
        'document_id' => 'setDocumentId',
        'attached_document_type' => 'setAttachedDocumentType',
        'file_name' => 'setFileName',
        'temporary_url' => 'setTemporaryUrl',
        'comment' => 'setComment',
        'supplier_name' => 'setSupplierName',
        'amount_invoice_currency' => 'setAmountInvoiceCurrency',
        'type' => 'setType',
        'attachment_status' => 'setAttachmentStatus'
    );

    public static function setters()
    {
        return self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     * @var string[]
     */
    protected static $getters = array(
        'id' => 'getId',
        'content_type' => 'getContentType',
        'document_id' => 'getDocumentId',
        'attached_document_type' => 'getAttachedDocumentType',
        'file_name' => 'getFileName',
        'temporary_url' => 'getTemporaryUrl',
        'comment' => 'getComment',
        'supplier_name' => 'getSupplierName',
        'amount_invoice_currency' => 'getAmountInvoiceCurrency',
        'type' => 'getType',
        'attachment_status' => 'getAttachmentStatus'
    );

    public static function getters()
    {
        return self::$getters;
    }

    const ATTACHED_DOCUMENT_TYPE_0 = 0;
    const ATTACHED_DOCUMENT_TYPE_1 = 1;
    const ATTACHED_DOCUMENT_TYPE_2 = 2;
    const ATTACHED_DOCUMENT_TYPE_3 = 3;
    const ATTACHED_DOCUMENT_TYPE_4 = 4;
    const ATTACHED_DOCUMENT_TYPE_5 = 5;
    const ATTACHED_DOCUMENT_TYPE_6 = 6;
    const TYPE_0 = 0;
    const TYPE_1 = 1;
    const TYPE_2 = 2;
    const ATTACHMENT_STATUS_0 = 0;
    const ATTACHMENT_STATUS_1 = 1;
    

    
    /**
     * Gets allowable values of the enum
     * @return string[]
     */
    public function getAttachedDocumentTypeAllowableValues()
    {
        return [
            self::ATTACHED_DOCUMENT_TYPE_0,
            self::ATTACHED_DOCUMENT_TYPE_1,
            self::ATTACHED_DOCUMENT_TYPE_2,
            self::ATTACHED_DOCUMENT_TYPE_3,
            self::ATTACHED_DOCUMENT_TYPE_4,
            self::ATTACHED_DOCUMENT_TYPE_5,
            self::ATTACHED_DOCUMENT_TYPE_6,
        ];
    }
    
    /**
     * Gets allowable values of the enum
     * @return string[]
     */
    public function getTypeAllowableValues()
    {
        return [
            self::TYPE_0,
            self::TYPE_1,
            self::TYPE_2,
        ];
    }
    
    /**
     * Gets allowable values of the enum
     * @return string[]
     */
    public function getAttachmentStatusAllowableValues()
    {
        return [
            self::ATTACHMENT_STATUS_0,
            self::ATTACHMENT_STATUS_1,
        ];
    }
    

    /**
     * Associative array for storing property values
     * @var mixed[]
     */
    protected $container = array();

    /**
     * Constructor
     * @param mixed[] $data Associated array of property value initalizing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['id'] = isset($data['id']) ? $data['id'] : null;
        $this->container['content_type'] = isset($data['content_type']) ? $data['content_type'] : null;
        $this->container['document_id'] = isset($data['document_id']) ? $data['document_id'] : null;
        $this->container['attached_document_type'] = isset($data['attached_document_type']) ? $data['attached_document_type'] : null;
        $this->container['file_name'] = isset($data['file_name']) ? $data['file_name'] : null;
        $this->container['temporary_url'] = isset($data['temporary_url']) ? $data['temporary_url'] : null;
        $this->container['comment'] = isset($data['comment']) ? $data['comment'] : null;
        $this->container['supplier_name'] = isset($data['supplier_name']) ? $data['supplier_name'] : null;
        $this->container['amount_invoice_currency'] = isset($data['amount_invoice_currency']) ? $data['amount_invoice_currency'] : null;
        $this->container['type'] = isset($data['type']) ? $data['type'] : null;
        $this->container['attachment_status'] = isset($data['attachment_status']) ? $data['attachment_status'] : null;
    }

    /**
     * show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalid_properties = array();
        $allowed_values = array("0", "1", "2", "3", "4", "5", "6");
        if (!in_array($this->container['attached_document_type'], $allowed_values)) {
            $invalid_properties[] = "invalid value for 'attached_document_type', must be one of #{allowed_values}.";
        }

        $allowed_values = array("0", "1", "2");
        if (!in_array($this->container['type'], $allowed_values)) {
            $invalid_properties[] = "invalid value for 'type', must be one of #{allowed_values}.";
        }

        $allowed_values = array("0", "1");
        if (!in_array($this->container['attachment_status'], $allowed_values)) {
            $invalid_properties[] = "invalid value for 'attachment_status', must be one of #{allowed_values}.";
        }

        return $invalid_properties;
    }

    /**
     * validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properteis are valid
     */
    public function valid()
    {
        $allowed_values = array("0", "1", "2", "3", "4", "5", "6");
        if (!in_array($this->container['attached_document_type'], $allowed_values)) {
            return false;
        }
        $allowed_values = array("0", "1", "2");
        if (!in_array($this->container['type'], $allowed_values)) {
            return false;
        }
        $allowed_values = array("0", "1");
        if (!in_array($this->container['attachment_status'], $allowed_values)) {
            return false;
        }
        return true;
    }


    /**
     * Gets id
     * @return string
     */
    public function getId()
    {
        return $this->container['id'];
    }

    /**
     * Sets id
     * @param string $id
     * @return $this
     */
    public function setId($id)
    {
        $this->container['id'] = $id;

        return $this;
    }

    /**
     * Gets content_type
     * @return string
     */
    public function getContentType()
    {
        return $this->container['content_type'];
    }

    /**
     * Sets content_type
     * @param string $content_type
     * @return $this
     */
    public function setContentType($content_type)
    {
        $this->container['content_type'] = $content_type;

        return $this;
    }

    /**
     * Gets document_id
     * @return string
     */
    public function getDocumentId()
    {
        return $this->container['document_id'];
    }

    /**
     * Sets document_id
     * @param string $document_id
     * @return $this
     */
    public function setDocumentId($document_id)
    {
        $this->container['document_id'] = $document_id;

        return $this;
    }

    /**
     * Gets attached_document_type
     * @return int
     */
    public function getAttachedDocumentType()
    {
        return $this->container['attached_document_type'];
    }

    /**
     * Sets attached_document_type
     * @param int $attached_document_type 0 = None, 1 = SupplierInvoice, 2 = Receipt, 3 = Voucher, 4 = SupplierInvoiceDraft, 5 = AllocationPeriod, 6 = Transfer
     * @return $this
     */
    public function setAttachedDocumentType($attached_document_type)
    {
        $allowed_values = array('0', '1', '2', '3', '4', '5', '6');
        if (!in_array($attached_document_type, $allowed_values)) {
            throw new \InvalidArgumentException("Invalid value for 'attached_document_type', must be one of '0', '1', '2', '3', '4', '5', '6'");
        }
        $this->container['attached_document_type'] = $attached_document_type;

        return $this;
    }

    /**
     * Gets file_name
     * @return string
     */
    public function getFileName()
    {
        return $this->container['file_name'];
    }

    /**
     * Sets file_name
     * @param string $file_name
     * @return $this
     */
    public function setFileName($file_name)
    {
        $this->container['file_name'] = $file_name;

        return $this;
    }

    /**
     * Gets temporary_url
     * @return string
     */
    public function getTemporaryUrl()
    {
        return $this->container['temporary_url'];
    }

    /**
     * Sets temporary_url
     * @param string $temporary_url
     * @return $this
     */
    public function setTemporaryUrl($temporary_url)
    {
        $this->container['temporary_url'] = $temporary_url;

        return $this;
    }

    /**
     * Gets comment
     * @return string
     */
    public function getComment()
    {
        return $this->container['comment'];
    }

    /**
     * Sets comment
     * @param string $comment
     * @return $this
     */
    public function setComment($comment)
    {
        $this->container['comment'] = $comment;

        return $this;
    }

    /**
     * Gets supplier_name
     * @return string
     */
    public function getSupplierName()
    {
        return $this->container['supplier_name'];
    }

    /**
     * Sets supplier_name
     * @param string $supplier_name
     * @return $this
     */
    public function setSupplierName($supplier_name)
    {
        $this->container['supplier_name'] = $supplier_name;

        return $this;
    }

    /**
     * Gets amount_invoice_currency
     * @return double
     */
    public function getAmountInvoiceCurrency()
    {
        return $this->container['amount_invoice_currency'];
    }

    /**
     * Sets amount_invoice_currency
     * @param double $amount_invoice_currency
     * @return $this
     */
    public function setAmountInvoiceCurrency($amount_invoice_currency)
    {
        $this->container['amount_invoice_currency'] = $amount_invoice_currency;

        return $this;
    }

    /**
     * Gets type
     * @return int
     */
    public function getType()
    {
        return $this->container['type'];
    }

    /**
     * Sets type
     * @param int $type 0 = Invoice, 1 = Receipt, 2 = Document
     * @return $this
     */
    public function setType($type)
    {
        $allowed_values = array('0', '1', '2');
        if (!in_array($type, $allowed_values)) {
            throw new \InvalidArgumentException("Invalid value for 'type', must be one of '0', '1', '2'");
        }
        $this->container['type'] = $type;

        return $this;
    }

    /**
     * Gets attachment_status
     * @return int
     */
    public function getAttachmentStatus()
    {
        return $this->container['attachment_status'];
    }

    /**
     * Sets attachment_status
     * @param int $attachment_status 0 = Matched, 1 = Unmatched
     * @return $this
     */
    public function setAttachmentStatus($attachment_status)
    {
        $allowed_values = array('0', '1');
        if (!in_array($attachment_status, $allowed_values)) {
            throw new \InvalidArgumentException("Invalid value for 'attachment_status', must be one of '0', '1'");
        }
        $this->container['attachment_status'] = $attachment_status;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     * @param  integer $offset Offset
     * @return boolean
     */
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     * @param  integer $offset Offset
     * @return mixed
     */
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     * @param  integer $offset Offset
     * @param  mixed   $value  Value to be set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     * @param  integer $offset Offset
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     * @return string
     */
    public function __toString()
    {
        if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
            return json_encode(\Trollweb\EaccountingApi\ObjectSerializer::sanitizeForSerialization($this), JSON_PRETTY_PRINT);
        }

        return json_encode(\Trollweb\EaccountingApi\ObjectSerializer::sanitizeForSerialization($this));
    }
}


