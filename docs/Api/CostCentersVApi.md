# Trollweb\EaccountingApi\CostCentersVApi

All URIs are relative to *https://eaccountingapi-sandbox.test.vismaonline.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**costCentersV2Get**](CostCentersVApi.md#costCentersV2Get) | **GET** /v2/costcenters | Get a list of Cost Centers
[**costCentersV2Get_0**](CostCentersVApi.md#costCentersV2Get_0) | **PUT** /v2/costcenters/{id} | Replace content in a cost center.


# **costCentersV2Get**
> \Trollweb\EaccountingApi\Model\PaginatedResponseCostCenterApi costCentersV2Get()

Get a list of Cost Centers

<p>Requires any of the following scopes: <br><b>ea:accounting, ea:accounting_readonly, ea:sales, ea:sales_readonly, ea.local:mobile_user</b></p><p>Available in any of the following variants: <br><b>Pro, Standard, Bookkeeping, Invoicing, Albert</b></p>

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Trollweb\EaccountingApi\Api\CostCentersVApi();

try {
    $result = $api_instance->costCentersV2Get();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CostCentersVApi->costCentersV2Get: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\Trollweb\EaccountingApi\Model\PaginatedResponseCostCenterApi**](../Model/PaginatedResponseCostCenterApi.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **costCentersV2Get_0**
> \Trollweb\EaccountingApi\Model\CostCenterApi costCentersV2Get_0($id, $cost_center)

Replace content in a cost center.

<p>Requires any of the following scopes: <br><b>ea:accounting, ea:sales, ea.local:mobile_user</b></p><p>Available in any of the following variants: <br><b>Pro, Standard, Bookkeeping, Invoicing, Albert</b></p>

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Trollweb\EaccountingApi\Api\CostCentersVApi();
$id = "id_example"; // string | 
$cost_center = new \Trollweb\EaccountingApi\Model\CostCenterApi(); // \Trollweb\EaccountingApi\Model\CostCenterApi | 

try {
    $result = $api_instance->costCentersV2Get_0($id, $cost_center);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CostCentersVApi->costCentersV2Get_0: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**|  |
 **cost_center** | [**\Trollweb\EaccountingApi\Model\CostCenterApi**](../Model/\Trollweb\EaccountingApi\Model\CostCenterApi.md)|  |

### Return type

[**\Trollweb\EaccountingApi\Model\CostCenterApi**](../Model/CostCenterApi.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/xml, text/xml, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

