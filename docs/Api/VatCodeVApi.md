# Trollweb\EaccountingApi\VatCodeVApi

All URIs are relative to *https://eaccountingapi-sandbox.test.vismaonline.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**vatCodeV2Get**](VatCodeVApi.md#vatCodeV2Get) | **GET** /v2/vatcodes | Gets a list of all Vat Codes
[**vatCodeV2Get_0**](VatCodeVApi.md#vatCodeV2Get_0) | **GET** /v2/vatcodes/{id} | Get a vat code item by it&#39;s id.


# **vatCodeV2Get**
> \Trollweb\EaccountingApi\Model\VatCode[] vatCodeV2Get($vat_rate_date)

Gets a list of all Vat Codes

<p>Requires any of the following scopes: <br><b>ea:accounting, ea:accounting_readonly, ea.local:mobile_user</b></p><p>Available in any of the following variants: <br><b>Pro, Standard, Bookkeeping, Invoicing, Solo, Albert</b></p>

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Trollweb\EaccountingApi\Api\VatCodeVApi();
$vat_rate_date = "vat_rate_date_example"; // string | Default value: Today

try {
    $result = $api_instance->vatCodeV2Get($vat_rate_date);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling VatCodeVApi->vatCodeV2Get: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **vat_rate_date** | **string**| Default value: Today | [optional]

### Return type

[**\Trollweb\EaccountingApi\Model\VatCode[]**](../Model/VatCode.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **vatCodeV2Get_0**
> \Trollweb\EaccountingApi\Model\VatCode[] vatCodeV2Get_0($id, $vat_rate_date)

Get a vat code item by it's id.

<p>Requires any of the following scopes: <br><b>ea:accounting, ea:accounting_readonly, ea.local:mobile_user</b></p><p>Available in any of the following variants: <br><b>Pro, Standard, Bookkeeping, Invoicing, Solo, Albert</b></p>

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Trollweb\EaccountingApi\Api\VatCodeVApi();
$id = "id_example"; // string | 
$vat_rate_date = "vat_rate_date_example"; // string | 

try {
    $result = $api_instance->vatCodeV2Get_0($id, $vat_rate_date);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling VatCodeVApi->vatCodeV2Get_0: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**|  |
 **vat_rate_date** | **string**|  | [optional]

### Return type

[**\Trollweb\EaccountingApi\Model\VatCode[]**](../Model/VatCode.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

