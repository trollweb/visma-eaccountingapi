# Trollweb\EaccountingApi\CompanySettingsVApi

All URIs are relative to *https://eaccountingapi-sandbox.test.vismaonline.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**companySettingsV2Get**](CompanySettingsVApi.md#companySettingsV2Get) | **GET** /v2/companysettings | Get company settings.
[**companySettingsV2Put**](CompanySettingsVApi.md#companySettingsV2Put) | **PUT** /v2/companysettings | Replace company settings.


# **companySettingsV2Get**
> \Trollweb\EaccountingApi\Model\CompanySettingsApi companySettingsV2Get()

Get company settings.

<p>Requires any of the following scopes: <br><b>ea:sales, ea:sales_readonly, ea.local:mobile_user</b></p><p>Available in any of the following variants: <br><b>Pro, Standard, Invoicing, Bookkeeping, Solo, Albert</b></p>

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Trollweb\EaccountingApi\Api\CompanySettingsVApi();

try {
    $result = $api_instance->companySettingsV2Get();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CompanySettingsVApi->companySettingsV2Get: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\Trollweb\EaccountingApi\Model\CompanySettingsApi**](../Model/CompanySettingsApi.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **companySettingsV2Put**
> \Trollweb\EaccountingApi\Model\CompanySettingsApi companySettingsV2Put($company_settings)

Replace company settings.

<p>Requires any of the following scopes: <br><b>ea:sales, ea.local:mobile_user</b></p><p>Available in any of the following variants: <br><b>Pro, Standard, Invoicing, Bookkeeping, Solo, Albert</b></p>

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Trollweb\EaccountingApi\Api\CompanySettingsVApi();
$company_settings = new \Trollweb\EaccountingApi\Model\CompanySettingsApi(); // \Trollweb\EaccountingApi\Model\CompanySettingsApi | 

try {
    $result = $api_instance->companySettingsV2Put($company_settings);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CompanySettingsVApi->companySettingsV2Put: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **company_settings** | [**\Trollweb\EaccountingApi\Model\CompanySettingsApi**](../Model/\Trollweb\EaccountingApi\Model\CompanySettingsApi.md)|  |

### Return type

[**\Trollweb\EaccountingApi\Model\CompanySettingsApi**](../Model/CompanySettingsApi.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/xml, text/xml, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

