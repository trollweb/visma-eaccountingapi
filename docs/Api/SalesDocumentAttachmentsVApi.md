# Trollweb\EaccountingApi\SalesDocumentAttachmentsVApi

All URIs are relative to *https://eaccountingapi-sandbox.test.vismaonline.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**salesDocumentAttachmentsV2Delete**](SalesDocumentAttachmentsVApi.md#salesDocumentAttachmentsV2Delete) | **DELETE** /v2/salesdocumentattachments/{customerInvoiceDraftId}/{attachmentId} | Delete an attachment.
[**salesDocumentAttachmentsV2Post**](SalesDocumentAttachmentsVApi.md#salesDocumentAttachmentsV2Post) | **POST** /v2/salesdocumentattachments | Create a sales document attachment.


# **salesDocumentAttachmentsV2Delete**
> object salesDocumentAttachmentsV2Delete($customer_invoice_draft_id, $attachment_id)

Delete an attachment.

<p>Requires any of the following scopes: <br><b>ea:sales, ea.local:mobile_user</b></p><p>Available in any of the following variants: <br><b>Pro, Standard, Invoicing, Solo, Albert</b></p>

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Trollweb\EaccountingApi\Api\SalesDocumentAttachmentsVApi();
$customer_invoice_draft_id = "customer_invoice_draft_id_example"; // string | 
$attachment_id = "attachment_id_example"; // string | 

try {
    $result = $api_instance->salesDocumentAttachmentsV2Delete($customer_invoice_draft_id, $attachment_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SalesDocumentAttachmentsVApi->salesDocumentAttachmentsV2Delete: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **customer_invoice_draft_id** | **string**|  |
 **attachment_id** | **string**|  |

### Return type

**object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **salesDocumentAttachmentsV2Post**
> \Trollweb\EaccountingApi\Model\SalesDocumentAttachmentApi salesDocumentAttachmentsV2Post($attachment)

Create a sales document attachment.

<p>Requires any of the following scopes: <br><b>ea:sales, ea.local:mobile_user</b></p><p>Available in any of the following variants: <br><b>Pro, Standard, Invoicing, Solo, Albert</b></p>

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Trollweb\EaccountingApi\Api\SalesDocumentAttachmentsVApi();
$attachment = new \Trollweb\EaccountingApi\Model\SalesDocumentAttachmentUploadApi(); // \Trollweb\EaccountingApi\Model\SalesDocumentAttachmentUploadApi | 

try {
    $result = $api_instance->salesDocumentAttachmentsV2Post($attachment);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SalesDocumentAttachmentsVApi->salesDocumentAttachmentsV2Post: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **attachment** | [**\Trollweb\EaccountingApi\Model\SalesDocumentAttachmentUploadApi**](../Model/\Trollweb\EaccountingApi\Model\SalesDocumentAttachmentUploadApi.md)|  |

### Return type

[**\Trollweb\EaccountingApi\Model\SalesDocumentAttachmentApi**](../Model/SalesDocumentAttachmentApi.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/xml, text/xml, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

