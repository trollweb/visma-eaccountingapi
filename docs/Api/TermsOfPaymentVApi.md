# Trollweb\EaccountingApi\TermsOfPaymentVApi

All URIs are relative to *https://eaccountingapi-sandbox.test.vismaonline.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**termsOfPaymentV2Get**](TermsOfPaymentVApi.md#termsOfPaymentV2Get) | **GET** /v2/termsofpayments | Get a list of TermsOfPayment items
[**termsOfPaymentV2Get_0**](TermsOfPaymentVApi.md#termsOfPaymentV2Get_0) | **GET** /v2/termsofpayments/{id} | Get a TermsOfPayment item by id


# **termsOfPaymentV2Get**
> \Trollweb\EaccountingApi\Model\PaginatedResponseTermsOfPaymentApi termsOfPaymentV2Get()

Get a list of TermsOfPayment items

<p>Requires any of the following scopes: <br><b>ea:sales, ea:sales_readonly, ea:purchase, ea:purchase_readonly, ea.local:mobile_user</b></p><p>Available in any of the following variants: <br><b>Pro, Standard, Invoicing, Solo, Albert</b></p>

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Trollweb\EaccountingApi\Api\TermsOfPaymentVApi();

try {
    $result = $api_instance->termsOfPaymentV2Get();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TermsOfPaymentVApi->termsOfPaymentV2Get: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\Trollweb\EaccountingApi\Model\PaginatedResponseTermsOfPaymentApi**](../Model/PaginatedResponseTermsOfPaymentApi.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **termsOfPaymentV2Get_0**
> \Trollweb\EaccountingApi\Model\TermsOfPaymentApi termsOfPaymentV2Get_0($id)

Get a TermsOfPayment item by id

<p>Requires any of the following scopes: <br><b>ea:sales, ea:sales_readonly, ea:purchase, ea:purchase_readonly, ea.local:mobile_user</b></p><p>Available in any of the following variants: <br><b>Pro, Standard, Invoicing, Solo, Albert</b></p>

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Trollweb\EaccountingApi\Api\TermsOfPaymentVApi();
$id = "id_example"; // string | 

try {
    $result = $api_instance->termsOfPaymentV2Get_0($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TermsOfPaymentVApi->termsOfPaymentV2Get_0: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**|  |

### Return type

[**\Trollweb\EaccountingApi\Model\TermsOfPaymentApi**](../Model/TermsOfPaymentApi.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

