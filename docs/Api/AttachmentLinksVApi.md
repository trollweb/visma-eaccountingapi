# Trollweb\EaccountingApi\AttachmentLinksVApi

All URIs are relative to *https://eaccountingapi-sandbox.test.vismaonline.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**attachmentLinksV2Delete**](AttachmentLinksVApi.md#attachmentLinksV2Delete) | **DELETE** /v2/attachmentlinks/{attachmentId} | Delete the link to an attachment.
[**attachmentLinksV2Post**](AttachmentLinksVApi.md#attachmentLinksV2Post) | **POST** /v2/attachmentlinks | Create a new links between a document and a set of attachments.


# **attachmentLinksV2Delete**
> object attachmentLinksV2Delete($attachment_id)

Delete the link to an attachment.

<p>Requires any of the following scopes: <br><b>ea:accounting, ea:purchase</b></p><p>Available in any of the following variants: <br><b>Pro, Standard, Invoicing, Bookkeeping, Solo, Albert</b></p>

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Trollweb\EaccountingApi\Api\AttachmentLinksVApi();
$attachment_id = "attachment_id_example"; // string | 

try {
    $result = $api_instance->attachmentLinksV2Delete($attachment_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AttachmentLinksVApi->attachmentLinksV2Delete: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **attachment_id** | **string**|  |

### Return type

**object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **attachmentLinksV2Post**
> \Trollweb\EaccountingApi\Model\AttachmentLinkApi attachmentLinksV2Post($attachment_links)

Create a new links between a document and a set of attachments.

<p>Requires any of the following scopes: <br><b>ea:accounting, ea:purchase</b></p><p>Available in any of the following variants: <br><b>Pro, Standard, Invoicing, Bookkeeping, Solo, Albert</b></p>

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Trollweb\EaccountingApi\Api\AttachmentLinksVApi();
$attachment_links = new \Trollweb\EaccountingApi\Model\AttachmentLinkApi(); // \Trollweb\EaccountingApi\Model\AttachmentLinkApi | 

try {
    $result = $api_instance->attachmentLinksV2Post($attachment_links);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AttachmentLinksVApi->attachmentLinksV2Post: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **attachment_links** | [**\Trollweb\EaccountingApi\Model\AttachmentLinkApi**](../Model/\Trollweb\EaccountingApi\Model\AttachmentLinkApi.md)|  |

### Return type

[**\Trollweb\EaccountingApi\Model\AttachmentLinkApi**](../Model/AttachmentLinkApi.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/xml, text/xml, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

