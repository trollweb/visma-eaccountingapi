# Trollweb\EaccountingApi\SupplierInvoiceDraftsVApi

All URIs are relative to *https://eaccountingapi-sandbox.test.vismaonline.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**supplierInvoiceDraftsV2Get**](SupplierInvoiceDraftsVApi.md#supplierInvoiceDraftsV2Get) | **GET** /v2/supplierinvoicedrafts | Get a paginated list of all supplier invoice drafts.
[**supplierInvoiceDraftsV2Get_0**](SupplierInvoiceDraftsVApi.md#supplierInvoiceDraftsV2Get_0) | **GET** /v2/supplierinvoicedrafts/{supplierInvoiceDraftId} | Get a single supplier invoice draft.
[**supplierInvoiceDraftsV2Post**](SupplierInvoiceDraftsVApi.md#supplierInvoiceDraftsV2Post) | **POST** /v2/supplierinvoicedrafts | Create a supplier invoice draft.
[**supplierInvoiceDraftsV2Put**](SupplierInvoiceDraftsVApi.md#supplierInvoiceDraftsV2Put) | **PUT** /v2/supplierinvoicedrafts/{supplierInvoiceDraftId} | Relpace content in a supplier invoice draft.


# **supplierInvoiceDraftsV2Get**
> \Trollweb\EaccountingApi\Model\PaginatedResponseSupplierInvoiceDraftApi supplierInvoiceDraftsV2Get()

Get a paginated list of all supplier invoice drafts.

<p>Requires any of the following scopes: <br><b>ea:purchase, ea:purchase_readonly, ea.local:mobile_user</b></p><p>Available in any of the following variants: <br><b>Pro, Standard, Solo, Albert</b></p>

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Trollweb\EaccountingApi\Api\SupplierInvoiceDraftsVApi();

try {
    $result = $api_instance->supplierInvoiceDraftsV2Get();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SupplierInvoiceDraftsVApi->supplierInvoiceDraftsV2Get: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\Trollweb\EaccountingApi\Model\PaginatedResponseSupplierInvoiceDraftApi**](../Model/PaginatedResponseSupplierInvoiceDraftApi.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **supplierInvoiceDraftsV2Get_0**
> \Trollweb\EaccountingApi\Model\SupplierInvoiceDraft supplierInvoiceDraftsV2Get_0($supplier_invoice_draft_id)

Get a single supplier invoice draft.

<p>Requires any of the following scopes: <br><b>ea:purchase, ea:purchase_readonly, ea.local:mobile_user</b></p><p>Available in any of the following variants: <br><b>Pro, Standard, Solo, Albert</b></p>

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Trollweb\EaccountingApi\Api\SupplierInvoiceDraftsVApi();
$supplier_invoice_draft_id = "supplier_invoice_draft_id_example"; // string | 

try {
    $result = $api_instance->supplierInvoiceDraftsV2Get_0($supplier_invoice_draft_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SupplierInvoiceDraftsVApi->supplierInvoiceDraftsV2Get_0: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **supplier_invoice_draft_id** | **string**|  |

### Return type

[**\Trollweb\EaccountingApi\Model\SupplierInvoiceDraft**](../Model/SupplierInvoiceDraft.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **supplierInvoiceDraftsV2Post**
> \Trollweb\EaccountingApi\Model\SupplierInvoiceDraft supplierInvoiceDraftsV2Post($supplier_invoice_draft, $use_default_vat_codes, $calculate_vat_on_cost_accounts, $batch_process_extended_validation, $duplicate_check_extended_validation)

Create a supplier invoice draft.

<p>Requires any of the following scopes: <br><b>ea:purchase, ea.local:mobile_user</b></p><p>Available in any of the following variants: <br><b>Pro, Standard, Solo, Albert</b></p>

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Trollweb\EaccountingApi\Api\SupplierInvoiceDraftsVApi();
$supplier_invoice_draft = new \Trollweb\EaccountingApi\Model\SupplierInvoiceDraftApi(); // \Trollweb\EaccountingApi\Model\SupplierInvoiceDraftApi | 
$use_default_vat_codes = true; // bool | 
$calculate_vat_on_cost_accounts = true; // bool | Automatic calculation of VAT based on vat code. DK and NL only
$batch_process_extended_validation = true; // bool | Validate for batch process and fiscal year.
$duplicate_check_extended_validation = true; // bool | Check if the invoice is duplicate.

try {
    $result = $api_instance->supplierInvoiceDraftsV2Post($supplier_invoice_draft, $use_default_vat_codes, $calculate_vat_on_cost_accounts, $batch_process_extended_validation, $duplicate_check_extended_validation);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SupplierInvoiceDraftsVApi->supplierInvoiceDraftsV2Post: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **supplier_invoice_draft** | [**\Trollweb\EaccountingApi\Model\SupplierInvoiceDraftApi**](../Model/\Trollweb\EaccountingApi\Model\SupplierInvoiceDraftApi.md)|  |
 **use_default_vat_codes** | **bool**|  | [optional]
 **calculate_vat_on_cost_accounts** | **bool**| Automatic calculation of VAT based on vat code. DK and NL only | [optional]
 **batch_process_extended_validation** | **bool**| Validate for batch process and fiscal year. | [optional]
 **duplicate_check_extended_validation** | **bool**| Check if the invoice is duplicate. | [optional]

### Return type

[**\Trollweb\EaccountingApi\Model\SupplierInvoiceDraft**](../Model/SupplierInvoiceDraft.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/xml, text/xml, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **supplierInvoiceDraftsV2Put**
> \Trollweb\EaccountingApi\Model\SupplierInvoiceDraft supplierInvoiceDraftsV2Put($supplier_invoice_draft_id, $supplier_invoice_draft)

Relpace content in a supplier invoice draft.

To update attachments us the attachmenst endpoint.<p>Requires any of the following scopes: <br><b>ea:purchase, ea.local:mobile_user</b></p><p>Available in any of the following variants: <br><b>Pro, Standard, Solo, Albert</b></p>

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Trollweb\EaccountingApi\Api\SupplierInvoiceDraftsVApi();
$supplier_invoice_draft_id = "supplier_invoice_draft_id_example"; // string | 
$supplier_invoice_draft = new \Trollweb\EaccountingApi\Model\SupplierInvoiceDraftApi(); // \Trollweb\EaccountingApi\Model\SupplierInvoiceDraftApi | 

try {
    $result = $api_instance->supplierInvoiceDraftsV2Put($supplier_invoice_draft_id, $supplier_invoice_draft);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SupplierInvoiceDraftsVApi->supplierInvoiceDraftsV2Put: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **supplier_invoice_draft_id** | **string**|  |
 **supplier_invoice_draft** | [**\Trollweb\EaccountingApi\Model\SupplierInvoiceDraftApi**](../Model/\Trollweb\EaccountingApi\Model\SupplierInvoiceDraftApi.md)|  |

### Return type

[**\Trollweb\EaccountingApi\Model\SupplierInvoiceDraft**](../Model/SupplierInvoiceDraft.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/xml, text/xml, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

