# Trollweb\EaccountingApi\AccountBalanceVApi

All URIs are relative to *https://eaccountingapi-sandbox.test.vismaonline.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**accountBalanceV2Get**](AccountBalanceVApi.md#accountBalanceV2Get) | **GET** /v2/accountbalances/{accountNumber}/{date} | Get Account Balance on a specific account and date.
[**accountBalanceV2Get_0**](AccountBalanceVApi.md#accountBalanceV2Get_0) | **GET** /v2/accountbalances/{date} | Get Account balance on a specific date (yyyy-MM-dd). Filter to include accounts where balance is 0.


# **accountBalanceV2Get**
> \Trollweb\EaccountingApi\Model\AccountBalanceAPI accountBalanceV2Get($account_number, $date)

Get Account Balance on a specific account and date.

<p>Requires any of the following scopes: <br><b>ea:accounting, ea:accounting_readonly, ea.local:mobile_user</b></p><p>Available in any of the following variants: <br><b>Pro, Standard, Bookkeeping, Solo, Albert</b></p>

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Trollweb\EaccountingApi\Api\AccountBalanceVApi();
$account_number = 56; // int | 
$date = new \DateTime(); // \DateTime | Format: yyyy-MM-dd

try {
    $result = $api_instance->accountBalanceV2Get($account_number, $date);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AccountBalanceVApi->accountBalanceV2Get: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **account_number** | **int**|  |
 **date** | **\DateTime**| Format: yyyy-MM-dd |

### Return type

[**\Trollweb\EaccountingApi\Model\AccountBalanceAPI**](../Model/AccountBalanceAPI.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **accountBalanceV2Get_0**
> \Trollweb\EaccountingApi\Model\PaginatedResponseAccountBalanceAPI accountBalanceV2Get_0($date)

Get Account balance on a specific date (yyyy-MM-dd). Filter to include accounts where balance is 0.

<p>Requires any of the following scopes: <br><b>ea:accounting, ea:accounting_readonly, ea.local:mobile_user</b></p><p>Available in any of the following variants: <br><b>Pro, Standard, Bookkeeping, Solo, Albert</b></p>

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Trollweb\EaccountingApi\Api\AccountBalanceVApi();
$date = new \DateTime(); // \DateTime | Format: yyyy-MM-dd

try {
    $result = $api_instance->accountBalanceV2Get_0($date);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AccountBalanceVApi->accountBalanceV2Get_0: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **date** | **\DateTime**| Format: yyyy-MM-dd |

### Return type

[**\Trollweb\EaccountingApi\Model\PaginatedResponseAccountBalanceAPI**](../Model/PaginatedResponseAccountBalanceAPI.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

