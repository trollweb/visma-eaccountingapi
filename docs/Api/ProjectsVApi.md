# Trollweb\EaccountingApi\ProjectsVApi

All URIs are relative to *https://eaccountingapi-sandbox.test.vismaonline.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**projectsV2Get**](ProjectsVApi.md#projectsV2Get) | **GET** /v2/projects | Get a list of projects.
[**projectsV2Get_0**](ProjectsVApi.md#projectsV2Get_0) | **GET** /v2/projects/{id} | Get a specific project.
[**projectsV2Post**](ProjectsVApi.md#projectsV2Post) | **POST** /v2/projects | Create a new project.
[**projectsV2Put**](ProjectsVApi.md#projectsV2Put) | **PUT** /v2/projects/{id} | Replace content in a project.


# **projectsV2Get**
> \Trollweb\EaccountingApi\Model\PaginatedResponseProjectApi projectsV2Get()

Get a list of projects.

<p>Requires any of the following scopes: <br><b>ea:accounting, ea:accounting_readonly, ea:sales, ea:sales_readonly, ea:purchase, ea:purchase_readonly, ea.local:mobile_user</b></p><p>Available in any of the following variants: <br><b>Pro, Standard, Bookkeeping, Invoicing, Albert</b></p>

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Trollweb\EaccountingApi\Api\ProjectsVApi();

try {
    $result = $api_instance->projectsV2Get();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProjectsVApi->projectsV2Get: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\Trollweb\EaccountingApi\Model\PaginatedResponseProjectApi**](../Model/PaginatedResponseProjectApi.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **projectsV2Get_0**
> \Trollweb\EaccountingApi\Model\ProjectApi projectsV2Get_0($id)

Get a specific project.

<p>Requires any of the following scopes: <br><b>ea:accounting, ea:accounting_readonly, ea:sales, ea:sales_readonly, ea:purchase, ea:purchase_readonly, ea.local:mobile_user</b></p><p>Available in any of the following variants: <br><b>Pro, Standard, Bookkeeping, Invoicing, Albert</b></p>

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Trollweb\EaccountingApi\Api\ProjectsVApi();
$id = "id_example"; // string | Project Id

try {
    $result = $api_instance->projectsV2Get_0($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProjectsVApi->projectsV2Get_0: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Project Id |

### Return type

[**\Trollweb\EaccountingApi\Model\ProjectApi**](../Model/ProjectApi.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **projectsV2Post**
> \Trollweb\EaccountingApi\Model\ProjectApi projectsV2Post($project)

Create a new project.

<p>Requires any of the following scopes: <br><b>ea:accounting, ea.local:mobile_user</b></p><p>Available in any of the following variants: <br><b>Pro, Standard, Bookkeeping, Invoicing, Albert</b></p>

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Trollweb\EaccountingApi\Api\ProjectsVApi();
$project = new \Trollweb\EaccountingApi\Model\ProjectApi(); // \Trollweb\EaccountingApi\Model\ProjectApi | 

try {
    $result = $api_instance->projectsV2Post($project);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProjectsVApi->projectsV2Post: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **project** | [**\Trollweb\EaccountingApi\Model\ProjectApi**](../Model/\Trollweb\EaccountingApi\Model\ProjectApi.md)|  |

### Return type

[**\Trollweb\EaccountingApi\Model\ProjectApi**](../Model/ProjectApi.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/xml, text/xml, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **projectsV2Put**
> \Trollweb\EaccountingApi\Model\ProjectApi projectsV2Put($id, $project)

Replace content in a project.

<p>Requires any of the following scopes: <br><b>ea:accounting, ea.local:mobile_user</b></p><p>Available in any of the following variants: <br><b>Pro, Standard, Bookkeeping, Invoicing, Albert</b></p>

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Trollweb\EaccountingApi\Api\ProjectsVApi();
$id = "id_example"; // string | 
$project = new \Trollweb\EaccountingApi\Model\ProjectApi(); // \Trollweb\EaccountingApi\Model\ProjectApi | 

try {
    $result = $api_instance->projectsV2Put($id, $project);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProjectsVApi->projectsV2Put: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**|  |
 **project** | [**\Trollweb\EaccountingApi\Model\ProjectApi**](../Model/\Trollweb\EaccountingApi\Model\ProjectApi.md)|  |

### Return type

[**\Trollweb\EaccountingApi\Model\ProjectApi**](../Model/ProjectApi.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/xml, text/xml, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

