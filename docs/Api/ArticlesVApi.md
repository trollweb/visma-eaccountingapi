# Trollweb\EaccountingApi\ArticlesVApi

All URIs are relative to *https://eaccountingapi-sandbox.test.vismaonline.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**articlesV2Get**](ArticlesVApi.md#articlesV2Get) | **GET** /v2/articles | Gets articles.
[**articlesV2Get_0**](ArticlesVApi.md#articlesV2Get_0) | **GET** /v2/articles/{articleId} | Gets an article by id.
[**articlesV2Post**](ArticlesVApi.md#articlesV2Post) | **POST** /v2/articles | Create a single article.
[**articlesV2Put**](ArticlesVApi.md#articlesV2Put) | **PUT** /v2/articles/{articleId} | Replace the data in an article.


# **articlesV2Get**
> \Trollweb\EaccountingApi\Model\PaginatedResponseArticleApi articlesV2Get($show_prices_with_two_decimals)

Gets articles.

<p>Requires any of the following scopes: <br><b>ea:sales, ea:sales_readonly, ea.local:mobile_user</b></p><p>Available in any of the following variants: <br><b>Pro, Standard, Invoicing, Solo, Albert</b></p>

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Trollweb\EaccountingApi\Api\ArticlesVApi();
$show_prices_with_two_decimals = true; // bool | 

try {
    $result = $api_instance->articlesV2Get($show_prices_with_two_decimals);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ArticlesVApi->articlesV2Get: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **show_prices_with_two_decimals** | **bool**|  | [optional]

### Return type

[**\Trollweb\EaccountingApi\Model\PaginatedResponseArticleApi**](../Model/PaginatedResponseArticleApi.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **articlesV2Get_0**
> \Trollweb\EaccountingApi\Model\ArticleApi articlesV2Get_0($article_id, $show_prices_with_two_decimals)

Gets an article by id.

<p>Requires any of the following scopes: <br><b>ea:sales, ea:sales_readonly, ea.local:mobile_user</b></p><p>Available in any of the following variants: <br><b>Pro, Standard, Invoicing, Solo, Albert</b></p>

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Trollweb\EaccountingApi\Api\ArticlesVApi();
$article_id = "article_id_example"; // string | 
$show_prices_with_two_decimals = true; // bool | 

try {
    $result = $api_instance->articlesV2Get_0($article_id, $show_prices_with_two_decimals);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ArticlesVApi->articlesV2Get_0: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **article_id** | **string**|  |
 **show_prices_with_two_decimals** | **bool**|  | [optional]

### Return type

[**\Trollweb\EaccountingApi\Model\ArticleApi**](../Model/ArticleApi.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **articlesV2Post**
> \Trollweb\EaccountingApi\Model\ArticleApi articlesV2Post($article)

Create a single article.

<p>Requires any of the following scopes: <br><b>ea:sales, ea.local:mobile_user</b></p><p>Available in any of the following variants: <br><b>Pro, Standard, Invoicing, Solo, Albert</b></p>

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Trollweb\EaccountingApi\Api\ArticlesVApi();
$article = new \Trollweb\EaccountingApi\Model\ArticleApi(); // \Trollweb\EaccountingApi\Model\ArticleApi | 

try {
    $result = $api_instance->articlesV2Post($article);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ArticlesVApi->articlesV2Post: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **article** | [**\Trollweb\EaccountingApi\Model\ArticleApi**](../Model/\Trollweb\EaccountingApi\Model\ArticleApi.md)|  |

### Return type

[**\Trollweb\EaccountingApi\Model\ArticleApi**](../Model/ArticleApi.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/xml, text/xml, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **articlesV2Put**
> \Trollweb\EaccountingApi\Model\ArticleApi articlesV2Put($article_id, $article)

Replace the data in an article.

<p>Requires any of the following scopes: <br><b>ea:sales, ea.local:mobile_user</b></p><p>Available in any of the following variants: <br><b>Pro, Standard, Invoicing, Solo, Albert</b></p>

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Trollweb\EaccountingApi\Api\ArticlesVApi();
$article_id = "article_id_example"; // string | 
$article = new \Trollweb\EaccountingApi\Model\ArticleApi(); // \Trollweb\EaccountingApi\Model\ArticleApi | 

try {
    $result = $api_instance->articlesV2Put($article_id, $article);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ArticlesVApi->articlesV2Put: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **article_id** | **string**|  |
 **article** | [**\Trollweb\EaccountingApi\Model\ArticleApi**](../Model/\Trollweb\EaccountingApi\Model\ArticleApi.md)|  |

### Return type

[**\Trollweb\EaccountingApi\Model\ArticleApi**](../Model/ArticleApi.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/xml, text/xml, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

