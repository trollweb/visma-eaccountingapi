# Trollweb\EaccountingApi\CustomerInvoicesVApi

All URIs are relative to *https://eaccountingapi-sandbox.test.vismaonline.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**customerInvoicesV2Get**](CustomerInvoicesVApi.md#customerInvoicesV2Get) | **GET** /v2/customerinvoices | Get all customer invoices.
[**customerInvoicesV2GetPdfReplacement**](CustomerInvoicesVApi.md#customerInvoicesV2GetPdfReplacement) | **GET** /v2/customerinvoices/{invoiceId}/pdf | Gets a customer invoice in Portable Document Format (PDF).
[**customerInvoicesV2Get_0**](CustomerInvoicesVApi.md#customerInvoicesV2Get_0) | **GET** /v2/customerinvoices/{invoiceId} | Gets a customer invoice with a specific id.


# **customerInvoicesV2Get**
> \Trollweb\EaccountingApi\Model\PaginatedResponseCustomerInvoiceApi customerInvoicesV2Get()

Get all customer invoices.

<p>Requires any of the following scopes: <br><b>ea:sales, ea:sales_readonly, ea.local:mobile_user</b></p><p>Available in any of the following variants: <br><b>Pro, Standard, Invoicing, Solo, Albert</b></p>

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Trollweb\EaccountingApi\Api\CustomerInvoicesVApi();

try {
    $result = $api_instance->customerInvoicesV2Get();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomerInvoicesVApi->customerInvoicesV2Get: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\Trollweb\EaccountingApi\Model\PaginatedResponseCustomerInvoiceApi**](../Model/PaginatedResponseCustomerInvoiceApi.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **customerInvoicesV2GetPdfReplacement**
> \Trollweb\EaccountingApi\Model\InvoiceUrlApi customerInvoicesV2GetPdfReplacement($invoice_id)

Gets a customer invoice in Portable Document Format (PDF).

As invoices are generated at request time, if not generated before, this endpoint sometimes has higher than average response time.<p>Requires any of the following scopes: <br><b>ea:sales, ea:sales_readonly, ea.local:mobile_user</b></p><p>Available in any of the following variants: <br><b>Pro, Standard, Invoicing, Solo, Albert</b></p>

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Trollweb\EaccountingApi\Api\CustomerInvoicesVApi();
$invoice_id = "invoice_id_example"; // string | 

try {
    $result = $api_instance->customerInvoicesV2GetPdfReplacement($invoice_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomerInvoicesVApi->customerInvoicesV2GetPdfReplacement: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **invoice_id** | **string**|  |

### Return type

[**\Trollweb\EaccountingApi\Model\InvoiceUrlApi**](../Model/InvoiceUrlApi.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **customerInvoicesV2Get_0**
> \Trollweb\EaccountingApi\Model\CustomerInvoiceApi customerInvoicesV2Get_0($invoice_id)

Gets a customer invoice with a specific id.

<p>Requires any of the following scopes: <br><b>ea:sales, ea:sales_readonly, ea.local:mobile_user</b></p><p>Available in any of the following variants: <br><b>Pro, Standard, Invoicing, Solo, Albert</b></p>

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Trollweb\EaccountingApi\Api\CustomerInvoicesVApi();
$invoice_id = "invoice_id_example"; // string | 

try {
    $result = $api_instance->customerInvoicesV2Get_0($invoice_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomerInvoicesVApi->customerInvoicesV2Get_0: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **invoice_id** | **string**|  |

### Return type

[**\Trollweb\EaccountingApi\Model\CustomerInvoiceApi**](../Model/CustomerInvoiceApi.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

