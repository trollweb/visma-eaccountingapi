# Trollweb\EaccountingApi\AttachmentsVApi

All URIs are relative to *https://eaccountingapi-sandbox.test.vismaonline.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**attachmentsV2Delete**](AttachmentsVApi.md#attachmentsV2Delete) | **DELETE** /v2/attachments/{attachmentId} | Delete an attachment.
[**attachmentsV2Get**](AttachmentsVApi.md#attachmentsV2Get) | **GET** /v2/attachments | Fetch attachments.
[**attachmentsV2Get_0**](AttachmentsVApi.md#attachmentsV2Get_0) | **GET** /v2/attachments/{attachmentId} | Get a specific attachment.
[**attachmentsV2Post**](AttachmentsVApi.md#attachmentsV2Post) | **POST** /v2/attachments | Create a new attachment.


# **attachmentsV2Delete**
> object attachmentsV2Delete($attachment_id)

Delete an attachment.

<p>Requires any of the following scopes: <br><b>ea:purchase, ea:accounting, ea.local:mobile_user</b></p><p>Available in any of the following variants: <br><b>Pro, Standard, Invoicing, Bookkeeping, Solo, Albert</b></p>

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Trollweb\EaccountingApi\Api\AttachmentsVApi();
$attachment_id = "attachment_id_example"; // string | 

try {
    $result = $api_instance->attachmentsV2Delete($attachment_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AttachmentsVApi->attachmentsV2Delete: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **attachment_id** | **string**|  |

### Return type

**object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **attachmentsV2Get**
> \Trollweb\EaccountingApi\Model\PaginatedResponseAttachmentResultApi attachmentsV2Get()

Fetch attachments.

<p>Requires any of the following scopes: <br><b>ea:purchase, ea:purchase_readonly, ea:accounting, ea:accounting_readonly, ea.local:mobile_user</b></p><p>Available in any of the following variants: <br><b>Pro, Standard, Invoicing, Bookkeeping, Solo, Albert</b></p>

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Trollweb\EaccountingApi\Api\AttachmentsVApi();

try {
    $result = $api_instance->attachmentsV2Get();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AttachmentsVApi->attachmentsV2Get: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\Trollweb\EaccountingApi\Model\PaginatedResponseAttachmentResultApi**](../Model/PaginatedResponseAttachmentResultApi.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **attachmentsV2Get_0**
> \Trollweb\EaccountingApi\Model\AttachmentResultApi attachmentsV2Get_0($attachment_id)

Get a specific attachment.

<p>Requires any of the following scopes: <br><b>ea:purchase, ea:purchase_readonly, ea:accounting, ea:accounting_readonly, ea.local:mobile_user</b></p><p>Available in any of the following variants: <br><b>Pro, Standard, Invoicing, Bookkeeping, Solo, Albert</b></p>

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Trollweb\EaccountingApi\Api\AttachmentsVApi();
$attachment_id = "attachment_id_example"; // string | 

try {
    $result = $api_instance->attachmentsV2Get_0($attachment_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AttachmentsVApi->attachmentsV2Get_0: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **attachment_id** | **string**|  |

### Return type

[**\Trollweb\EaccountingApi\Model\AttachmentResultApi**](../Model/AttachmentResultApi.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **attachmentsV2Post**
> \Trollweb\EaccountingApi\Model\AttachmentResultApi attachmentsV2Post($posted_attachment)

Create a new attachment.

<p>Requires any of the following scopes: <br><b>ea:purchase, ea:accounting, ea.local:mobile_user</b></p><p>Available in any of the following variants: <br><b>Pro, Standard, Invoicing, Bookkeeping, Solo, Albert</b></p>

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Trollweb\EaccountingApi\Api\AttachmentsVApi();
$posted_attachment = new \Trollweb\EaccountingApi\Model\AttachmentUploadApi(); // \Trollweb\EaccountingApi\Model\AttachmentUploadApi | 

try {
    $result = $api_instance->attachmentsV2Post($posted_attachment);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AttachmentsVApi->attachmentsV2Post: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **posted_attachment** | [**\Trollweb\EaccountingApi\Model\AttachmentUploadApi**](../Model/\Trollweb\EaccountingApi\Model\AttachmentUploadApi.md)|  |

### Return type

[**\Trollweb\EaccountingApi\Model\AttachmentResultApi**](../Model/AttachmentResultApi.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/xml, text/xml, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

