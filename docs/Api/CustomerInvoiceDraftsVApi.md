# Trollweb\EaccountingApi\CustomerInvoiceDraftsVApi

All URIs are relative to *https://eaccountingapi-sandbox.test.vismaonline.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**customerInvoiceDraftsV2ConvertToInvoice**](CustomerInvoiceDraftsVApi.md#customerInvoiceDraftsV2ConvertToInvoice) | **POST** /v2/customerinvoicedrafts/{customerInvoiceDraftId}/convert | Experimental Endpoint! This might be subject to changes. Converts a CustomerInvoiceDraft to a CustomerInvoice.
[**customerInvoiceDraftsV2Delete**](CustomerInvoiceDraftsVApi.md#customerInvoiceDraftsV2Delete) | **DELETE** /v2/customerinvoicedrafts/{customerInvoiceDraftId} | Delete a customer invoice draft.
[**customerInvoiceDraftsV2Get**](CustomerInvoiceDraftsVApi.md#customerInvoiceDraftsV2Get) | **GET** /v2/customerinvoicedrafts | Get all customer invoice drafts.
[**customerInvoiceDraftsV2Get_0**](CustomerInvoiceDraftsVApi.md#customerInvoiceDraftsV2Get_0) | **GET** /v2/customerinvoicedrafts/{invoiceDraftId} | Gets a customer invoice draft by id.
[**customerInvoiceDraftsV2Post**](CustomerInvoiceDraftsVApi.md#customerInvoiceDraftsV2Post) | **POST** /v2/customerinvoicedrafts | Create a single customer invoice draft.
[**customerInvoiceDraftsV2Put**](CustomerInvoiceDraftsVApi.md#customerInvoiceDraftsV2Put) | **PUT** /v2/customerinvoicedrafts/{customerInvoiceDraftId} | Replace the data in a customer invoice draft.


# **customerInvoiceDraftsV2ConvertToInvoice**
> \Trollweb\EaccountingApi\Model\CustomerInvoiceApi customerInvoiceDraftsV2ConvertToInvoice($customer_invoice_draft_id, $invoice_validations)

Experimental Endpoint! This might be subject to changes. Converts a CustomerInvoiceDraft to a CustomerInvoice.

<p>Requires any of the following scopes: <br><b>ea:sales, ea.local:mobile_user</b></p><p>Available in any of the following variants: <br><b>Pro, Standard, Invoicing, Solo, Albert</b></p>

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Trollweb\EaccountingApi\Api\CustomerInvoiceDraftsVApi();
$customer_invoice_draft_id = "customer_invoice_draft_id_example"; // string | The customer Invoice Draft Id.
$invoice_validations = new \Trollweb\EaccountingApi\Model\CustomerInvoiceDraftValidationApi(); // \Trollweb\EaccountingApi\Model\CustomerInvoiceDraftValidationApi | Optional set of properties to validate. Ignore the properties you do not wish to validate.

try {
    $result = $api_instance->customerInvoiceDraftsV2ConvertToInvoice($customer_invoice_draft_id, $invoice_validations);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomerInvoiceDraftsVApi->customerInvoiceDraftsV2ConvertToInvoice: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **customer_invoice_draft_id** | **string**| The customer Invoice Draft Id. |
 **invoice_validations** | [**\Trollweb\EaccountingApi\Model\CustomerInvoiceDraftValidationApi**](../Model/\Trollweb\EaccountingApi\Model\CustomerInvoiceDraftValidationApi.md)| Optional set of properties to validate. Ignore the properties you do not wish to validate. |

### Return type

[**\Trollweb\EaccountingApi\Model\CustomerInvoiceApi**](../Model/CustomerInvoiceApi.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/xml, text/xml, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **customerInvoiceDraftsV2Delete**
> object customerInvoiceDraftsV2Delete($customer_invoice_draft_id)

Delete a customer invoice draft.

<p>Requires any of the following scopes: <br><b>ea:sales, ea.local:mobile_user</b></p><p>Available in any of the following variants: <br><b>Pro, Standard, Invoicing, Solo, Albert</b></p>

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Trollweb\EaccountingApi\Api\CustomerInvoiceDraftsVApi();
$customer_invoice_draft_id = "customer_invoice_draft_id_example"; // string | 

try {
    $result = $api_instance->customerInvoiceDraftsV2Delete($customer_invoice_draft_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomerInvoiceDraftsVApi->customerInvoiceDraftsV2Delete: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **customer_invoice_draft_id** | **string**|  |

### Return type

**object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **customerInvoiceDraftsV2Get**
> \Trollweb\EaccountingApi\Model\PaginatedResponseCustomerInvoiceDraftApi customerInvoiceDraftsV2Get()

Get all customer invoice drafts.

<p>Requires any of the following scopes: <br><b>ea:sales, ea:sales_readonly, ea.local:mobile_user</b></p><p>Available in any of the following variants: <br><b>Pro, Standard, Invoicing, Solo, Albert</b></p>

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Trollweb\EaccountingApi\Api\CustomerInvoiceDraftsVApi();

try {
    $result = $api_instance->customerInvoiceDraftsV2Get();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomerInvoiceDraftsVApi->customerInvoiceDraftsV2Get: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\Trollweb\EaccountingApi\Model\PaginatedResponseCustomerInvoiceDraftApi**](../Model/PaginatedResponseCustomerInvoiceDraftApi.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **customerInvoiceDraftsV2Get_0**
> \Trollweb\EaccountingApi\Model\CustomerInvoiceDraftApi customerInvoiceDraftsV2Get_0($invoice_draft_id)

Gets a customer invoice draft by id.

<p>Requires any of the following scopes: <br><b>ea:sales, ea:sales_readonly, ea.local:mobile_user</b></p><p>Available in any of the following variants: <br><b>Pro, Standard, Invoicing, Solo, Albert</b></p>

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Trollweb\EaccountingApi\Api\CustomerInvoiceDraftsVApi();
$invoice_draft_id = "invoice_draft_id_example"; // string | 

try {
    $result = $api_instance->customerInvoiceDraftsV2Get_0($invoice_draft_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomerInvoiceDraftsVApi->customerInvoiceDraftsV2Get_0: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **invoice_draft_id** | **string**|  |

### Return type

[**\Trollweb\EaccountingApi\Model\CustomerInvoiceDraftApi**](../Model/CustomerInvoiceDraftApi.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **customerInvoiceDraftsV2Post**
> \Trollweb\EaccountingApi\Model\CustomerInvoiceDraftApi customerInvoiceDraftsV2Post($customer_invoice_draft)

Create a single customer invoice draft.

ReversedConstructionServicesVatFree attribute on a CustomerInvoiceDraftRow shall only be used for articels with reverse VAT charge.  For other VAT free articles ReversedConstructionServicesVatFree shall be set to 'false'.<p>Requires any of the following scopes: <br><b>ea:sales, ea.local:mobile_user</b></p><p>Available in any of the following variants: <br><b>Pro, Standard, Invoicing, Solo, Albert</b></p>

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Trollweb\EaccountingApi\Api\CustomerInvoiceDraftsVApi();
$customer_invoice_draft = new \Trollweb\EaccountingApi\Model\CustomerInvoiceDraftApi(); // \Trollweb\EaccountingApi\Model\CustomerInvoiceDraftApi | 

try {
    $result = $api_instance->customerInvoiceDraftsV2Post($customer_invoice_draft);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomerInvoiceDraftsVApi->customerInvoiceDraftsV2Post: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **customer_invoice_draft** | [**\Trollweb\EaccountingApi\Model\CustomerInvoiceDraftApi**](../Model/\Trollweb\EaccountingApi\Model\CustomerInvoiceDraftApi.md)|  |

### Return type

[**\Trollweb\EaccountingApi\Model\CustomerInvoiceDraftApi**](../Model/CustomerInvoiceDraftApi.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/xml, text/xml, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **customerInvoiceDraftsV2Put**
> \Trollweb\EaccountingApi\Model\CustomerInvoiceDraftApi customerInvoiceDraftsV2Put($customer_invoice_draft_id, $customer_invoice_draft)

Replace the data in a customer invoice draft.

<p>Requires any of the following scopes: <br><b>ea:sales, ea.local:mobile_user</b></p><p>Available in any of the following variants: <br><b>Pro, Standard, Invoicing, Solo, Albert</b></p>

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Trollweb\EaccountingApi\Api\CustomerInvoiceDraftsVApi();
$customer_invoice_draft_id = "customer_invoice_draft_id_example"; // string | 
$customer_invoice_draft = new \Trollweb\EaccountingApi\Model\CustomerInvoiceDraftApi(); // \Trollweb\EaccountingApi\Model\CustomerInvoiceDraftApi | 

try {
    $result = $api_instance->customerInvoiceDraftsV2Put($customer_invoice_draft_id, $customer_invoice_draft);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomerInvoiceDraftsVApi->customerInvoiceDraftsV2Put: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **customer_invoice_draft_id** | **string**|  |
 **customer_invoice_draft** | [**\Trollweb\EaccountingApi\Model\CustomerInvoiceDraftApi**](../Model/\Trollweb\EaccountingApi\Model\CustomerInvoiceDraftApi.md)|  |

### Return type

[**\Trollweb\EaccountingApi\Model\CustomerInvoiceDraftApi**](../Model/CustomerInvoiceDraftApi.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/xml, text/xml, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

