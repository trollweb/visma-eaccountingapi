# Trollweb\EaccountingApi\ArticleLabelsVApi

All URIs are relative to *https://eaccountingapi-sandbox.test.vismaonline.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**articleLabelsV2Delete**](ArticleLabelsVApi.md#articleLabelsV2Delete) | **DELETE** /v2/articlelabels/{articleLabelId} | Deletes an aticlelabel.
[**articleLabelsV2Get**](ArticleLabelsVApi.md#articleLabelsV2Get) | **GET** /v2/articlelabels | Gets articlelabels.
[**articleLabelsV2Get_0**](ArticleLabelsVApi.md#articleLabelsV2Get_0) | **GET** /v2/articlelabels/{articleLabelId} | Gets an articlelabel by id.
[**articleLabelsV2Post**](ArticleLabelsVApi.md#articleLabelsV2Post) | **POST** /v2/articlelabels | Create an articlelabel.
[**articleLabelsV2Put**](ArticleLabelsVApi.md#articleLabelsV2Put) | **PUT** /v2/articlelabels/{articleLabelId} | Replace content of an articlelabel.


# **articleLabelsV2Delete**
> \Trollweb\EaccountingApi\Model\ArticleLabelApi articleLabelsV2Delete($article_label_id)

Deletes an aticlelabel.

<p>Requires any of the following scopes: <br><b>ea:sales, ea.local:mobile_user</b></p><p>Available in any of the following variants: <br><b>Pro, Standard, Invoicing, Albert</b></p>

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Trollweb\EaccountingApi\Api\ArticleLabelsVApi();
$article_label_id = "article_label_id_example"; // string | 

try {
    $result = $api_instance->articleLabelsV2Delete($article_label_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ArticleLabelsVApi->articleLabelsV2Delete: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **article_label_id** | **string**|  |

### Return type

[**\Trollweb\EaccountingApi\Model\ArticleLabelApi**](../Model/ArticleLabelApi.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **articleLabelsV2Get**
> \Trollweb\EaccountingApi\Model\PaginatedResponseArticleLabelApi articleLabelsV2Get()

Gets articlelabels.

<p>Requires any of the following scopes: <br><b>ea:sales, ea:sales_readonly, ea.local:mobile_user</b></p><p>Available in any of the following variants: <br><b>Pro, Standard, Invoicing, Albert</b></p>

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Trollweb\EaccountingApi\Api\ArticleLabelsVApi();

try {
    $result = $api_instance->articleLabelsV2Get();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ArticleLabelsVApi->articleLabelsV2Get: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\Trollweb\EaccountingApi\Model\PaginatedResponseArticleLabelApi**](../Model/PaginatedResponseArticleLabelApi.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **articleLabelsV2Get_0**
> \Trollweb\EaccountingApi\Model\ArticleLabelApi articleLabelsV2Get_0($article_label_id)

Gets an articlelabel by id.

<p>Requires any of the following scopes: <br><b>ea:sales, ea:sales_readonly, ea.local:mobile_user</b></p><p>Available in any of the following variants: <br><b>Pro, Standard, Invoicing, Albert</b></p>

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Trollweb\EaccountingApi\Api\ArticleLabelsVApi();
$article_label_id = "article_label_id_example"; // string | 

try {
    $result = $api_instance->articleLabelsV2Get_0($article_label_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ArticleLabelsVApi->articleLabelsV2Get_0: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **article_label_id** | **string**|  |

### Return type

[**\Trollweb\EaccountingApi\Model\ArticleLabelApi**](../Model/ArticleLabelApi.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **articleLabelsV2Post**
> \Trollweb\EaccountingApi\Model\ArticleLabelApi articleLabelsV2Post($from_article_label)

Create an articlelabel.

<p>Requires any of the following scopes: <br><b>ea:sales, ea.local:mobile_user</b></p><p>Available in any of the following variants: <br><b>Pro, Standard, Invoicing, Albert</b></p>

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Trollweb\EaccountingApi\Api\ArticleLabelsVApi();
$from_article_label = new \Trollweb\EaccountingApi\Model\ArticleLabelApi(); // \Trollweb\EaccountingApi\Model\ArticleLabelApi | 

try {
    $result = $api_instance->articleLabelsV2Post($from_article_label);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ArticleLabelsVApi->articleLabelsV2Post: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **from_article_label** | [**\Trollweb\EaccountingApi\Model\ArticleLabelApi**](../Model/\Trollweb\EaccountingApi\Model\ArticleLabelApi.md)|  |

### Return type

[**\Trollweb\EaccountingApi\Model\ArticleLabelApi**](../Model/ArticleLabelApi.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/xml, text/xml, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **articleLabelsV2Put**
> \Trollweb\EaccountingApi\Model\ArticleLabelApi articleLabelsV2Put($article_label_id, $from_article_label)

Replace content of an articlelabel.

<p>Requires any of the following scopes: <br><b>ea:sales, ea.local:mobile_user</b></p><p>Available in any of the following variants: <br><b>Pro, Standard, Invoicing, Albert</b></p>

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Trollweb\EaccountingApi\Api\ArticleLabelsVApi();
$article_label_id = "article_label_id_example"; // string | 
$from_article_label = new \Trollweb\EaccountingApi\Model\ArticleLabelApi(); // \Trollweb\EaccountingApi\Model\ArticleLabelApi | 

try {
    $result = $api_instance->articleLabelsV2Put($article_label_id, $from_article_label);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ArticleLabelsVApi->articleLabelsV2Put: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **article_label_id** | **string**|  |
 **from_article_label** | [**\Trollweb\EaccountingApi\Model\ArticleLabelApi**](../Model/\Trollweb\EaccountingApi\Model\ArticleLabelApi.md)|  |

### Return type

[**\Trollweb\EaccountingApi\Model\ArticleLabelApi**](../Model/ArticleLabelApi.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/xml, text/xml, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

