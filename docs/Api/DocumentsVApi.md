# Trollweb\EaccountingApi\DocumentsVApi

All URIs are relative to *https://eaccountingapi-sandbox.test.vismaonline.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**documentsV2Get**](DocumentsVApi.md#documentsV2Get) | **GET** /v2/documents/{id} | Get a vat report pdf by document id.


# **documentsV2Get**
> \Trollweb\EaccountingApi\Model\DocumentApi documentsV2Get($id)

Get a vat report pdf by document id.

<p>Requires any of the following scopes: <br><b>ea:sales, ea:sales_readonly, ea:purchase, ea:purchase_readonly, ea.local:mobile_user</b></p><p>Available in any of the following variants: <br><b>Pro, Standard, Bookkeeping, Invoicing, Solo, Albert</b></p>

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Trollweb\EaccountingApi\Api\DocumentsVApi();
$id = "id_example"; // string | Document id

try {
    $result = $api_instance->documentsV2Get($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DocumentsVApi->documentsV2Get: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Document id |

### Return type

[**\Trollweb\EaccountingApi\Model\DocumentApi**](../Model/DocumentApi.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

