# Trollweb\EaccountingApi\AllocationPeriodsVApi

All URIs are relative to *https://eaccountingapi-sandbox.test.vismaonline.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**allocationPeriodsV2Get**](AllocationPeriodsVApi.md#allocationPeriodsV2Get) | **GET** /v2/allocationperiods | Get allocation periods.
[**allocationPeriodsV2Get_0**](AllocationPeriodsVApi.md#allocationPeriodsV2Get_0) | **GET** /v2/allocationperiods/{allocationPeriodId} | Get single allocation period.
[**allocationPeriodsV2Post**](AllocationPeriodsVApi.md#allocationPeriodsV2Post) | **POST** /v2/allocationperiods | Add allocation periods for voucher or supplier invoice.


# **allocationPeriodsV2Get**
> \Trollweb\EaccountingApi\Model\PaginatedResponseAllocationPeriodApi allocationPeriodsV2Get()

Get allocation periods.

<p>Requires any of the following scopes: <br><b>ea:purchase, ea:purchase_readonly, ea.local:mobile_user</b></p><p>Available in any of the following variants: <br><b>Pro, Standard, Bookkeeping, Albert</b></p>

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Trollweb\EaccountingApi\Api\AllocationPeriodsVApi();

try {
    $result = $api_instance->allocationPeriodsV2Get();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AllocationPeriodsVApi->allocationPeriodsV2Get: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\Trollweb\EaccountingApi\Model\PaginatedResponseAllocationPeriodApi**](../Model/PaginatedResponseAllocationPeriodApi.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **allocationPeriodsV2Get_0**
> \Trollweb\EaccountingApi\Model\AllocationPeriodApi allocationPeriodsV2Get_0($allocation_period_id)

Get single allocation period.

<p>Requires any of the following scopes: <br><b>ea:purchase, ea:purchase_readonly, ea.local:mobile_user</b></p><p>Available in any of the following variants: <br><b>Pro, Standard, Bookkeeping, Albert</b></p>

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Trollweb\EaccountingApi\Api\AllocationPeriodsVApi();
$allocation_period_id = "allocation_period_id_example"; // string | Id of the requsted allocation period.

try {
    $result = $api_instance->allocationPeriodsV2Get_0($allocation_period_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AllocationPeriodsVApi->allocationPeriodsV2Get_0: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **allocation_period_id** | **string**| Id of the requsted allocation period. |

### Return type

[**\Trollweb\EaccountingApi\Model\AllocationPeriodApi**](../Model/AllocationPeriodApi.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **allocationPeriodsV2Post**
> \Trollweb\EaccountingApi\Model\AllocationPeriodApi allocationPeriodsV2Post($allocation_plans)

Add allocation periods for voucher or supplier invoice.

<p>Requires any of the following scopes: <br><b>ea:purchase, ea.local:mobile_user</b></p><p>Available in any of the following variants: <br><b>Pro, Standard, Bookkeeping, Albert</b></p>

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Trollweb\EaccountingApi\Api\AllocationPeriodsVApi();
$allocation_plans = array(new AllocationPlan()); // \Trollweb\EaccountingApi\Model\AllocationPlan[] | 

try {
    $result = $api_instance->allocationPeriodsV2Post($allocation_plans);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AllocationPeriodsVApi->allocationPeriodsV2Post: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **allocation_plans** | [**\Trollweb\EaccountingApi\Model\AllocationPlan[]**](../Model/AllocationPlan.md)|  |

### Return type

[**\Trollweb\EaccountingApi\Model\AllocationPeriodApi**](../Model/AllocationPeriodApi.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/xml, text/xml, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

