# Trollweb\EaccountingApi\UnitsVApi

All URIs are relative to *https://eaccountingapi-sandbox.test.vismaonline.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**unitsV2Get**](UnitsVApi.md#unitsV2Get) | **GET** /v2/units | Get a list of Units
[**unitsV2Get_0**](UnitsVApi.md#unitsV2Get_0) | **GET** /v2/units/{id} | Get a singel unit.


# **unitsV2Get**
> \Trollweb\EaccountingApi\Model\PaginatedResponseUnitApi unitsV2Get()

Get a list of Units

<p>Requires any of the following scopes: <br><b>ea:sales, ea:sales_readonly, ea.local:mobile_user</b></p><p>Available in any of the following variants: <br><b>Pro, Standard, Invoicing, Solo, Albert</b></p>

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Trollweb\EaccountingApi\Api\UnitsVApi();

try {
    $result = $api_instance->unitsV2Get();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UnitsVApi->unitsV2Get: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\Trollweb\EaccountingApi\Model\PaginatedResponseUnitApi**](../Model/PaginatedResponseUnitApi.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **unitsV2Get_0**
> \Trollweb\EaccountingApi\Model\UnitApi unitsV2Get_0($id)

Get a singel unit.

<p>Requires any of the following scopes: <br><b>ea:sales, ea:sales_readonly, ea.local:mobile_user</b></p><p>Available in any of the following variants: <br><b>Pro, Standard, Invoicing, Solo, Albert</b></p>

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Trollweb\EaccountingApi\Api\UnitsVApi();
$id = "id_example"; // string | Id of requested unit.

try {
    $result = $api_instance->unitsV2Get_0($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UnitsVApi->unitsV2Get_0: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Id of requested unit. |

### Return type

[**\Trollweb\EaccountingApi\Model\UnitApi**](../Model/UnitApi.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

