# Trollweb\EaccountingApi\PartnerResourceLinksVApi

All URIs are relative to *https://eaccountingapi-sandbox.test.vismaonline.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**partnerResourceLinksV2Delete**](PartnerResourceLinksVApi.md#partnerResourceLinksV2Delete) | **DELETE** /v2/partnerresourcelinks/{partnerResourceLinkId} | Delete a partner resource link
[**partnerResourceLinksV2Get**](PartnerResourceLinksVApi.md#partnerResourceLinksV2Get) | **GET** /v2/partnerresourcelinks | Get a list of partner resource links.
[**partnerResourceLinksV2Get_0**](PartnerResourceLinksVApi.md#partnerResourceLinksV2Get_0) | **GET** /v2/partnerresourcelinks/{partnerResourceLinkId} | Get a partner resource link by id.
[**partnerResourceLinksV2Post**](PartnerResourceLinksVApi.md#partnerResourceLinksV2Post) | **POST** /v2/partnerresourcelinks | Create a partner resource link
[**partnerResourceLinksV2Put**](PartnerResourceLinksVApi.md#partnerResourceLinksV2Put) | **PUT** /v2/partnerresourcelinks/{partnerResourceLinkId} | Update a partner resource link


# **partnerResourceLinksV2Delete**
> object partnerResourceLinksV2Delete($partner_resource_link_id)

Delete a partner resource link

<p>Requires any of the following scopes: <br><b>ea:sales, ea:purchase, ea.local:mobile_user</b></p><p>Available in any of the following variants: <br><b>Pro, Standard, Invoicing, Bookkeeping, Solo, Albert</b></p>

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Trollweb\EaccountingApi\Api\PartnerResourceLinksVApi();
$partner_resource_link_id = "partner_resource_link_id_example"; // string | 

try {
    $result = $api_instance->partnerResourceLinksV2Delete($partner_resource_link_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PartnerResourceLinksVApi->partnerResourceLinksV2Delete: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **partner_resource_link_id** | **string**|  |

### Return type

**object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **partnerResourceLinksV2Get**
> \Trollweb\EaccountingApi\Model\PaginatedResponsePartnerResourceLinkApi partnerResourceLinksV2Get()

Get a list of partner resource links.

<p>Requires any of the following scopes: <br><b>ea:sales, ea:sales_readonly, ea:purchase, ea:purchase_readonly, ea.local:mobile_user</b></p><p>Available in any of the following variants: <br><b>Pro, Standard, Invoicing, Bookkeeping, Solo, Albert</b></p>

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Trollweb\EaccountingApi\Api\PartnerResourceLinksVApi();

try {
    $result = $api_instance->partnerResourceLinksV2Get();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PartnerResourceLinksVApi->partnerResourceLinksV2Get: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\Trollweb\EaccountingApi\Model\PaginatedResponsePartnerResourceLinkApi**](../Model/PaginatedResponsePartnerResourceLinkApi.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **partnerResourceLinksV2Get_0**
> \Trollweb\EaccountingApi\Model\PartnerResourceLinkApi partnerResourceLinksV2Get_0($partner_resource_link_id)

Get a partner resource link by id.

<p>Requires any of the following scopes: <br><b>ea:sales, ea:sales_readonly, ea:purchase, ea:purchase_readonly, ea.local:mobile_user</b></p><p>Available in any of the following variants: <br><b>Pro, Standard, Invoicing, Bookkeeping, Solo, Albert</b></p>

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Trollweb\EaccountingApi\Api\PartnerResourceLinksVApi();
$partner_resource_link_id = "partner_resource_link_id_example"; // string | 

try {
    $result = $api_instance->partnerResourceLinksV2Get_0($partner_resource_link_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PartnerResourceLinksVApi->partnerResourceLinksV2Get_0: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **partner_resource_link_id** | **string**|  |

### Return type

[**\Trollweb\EaccountingApi\Model\PartnerResourceLinkApi**](../Model/PartnerResourceLinkApi.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **partnerResourceLinksV2Post**
> \Trollweb\EaccountingApi\Model\PartnerResourceLinkApi partnerResourceLinksV2Post($partner_resource_link)

Create a partner resource link

<p>Requires any of the following scopes: <br><b>ea:sales, ea:purchase, ea.local:mobile_user</b></p><p>Available in any of the following variants: <br><b>Pro, Standard, Invoicing, Bookkeeping, Solo, Albert</b></p>

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Trollweb\EaccountingApi\Api\PartnerResourceLinksVApi();
$partner_resource_link = new \Trollweb\EaccountingApi\Model\PartnerResourceLinkApi(); // \Trollweb\EaccountingApi\Model\PartnerResourceLinkApi | 

try {
    $result = $api_instance->partnerResourceLinksV2Post($partner_resource_link);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PartnerResourceLinksVApi->partnerResourceLinksV2Post: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **partner_resource_link** | [**\Trollweb\EaccountingApi\Model\PartnerResourceLinkApi**](../Model/\Trollweb\EaccountingApi\Model\PartnerResourceLinkApi.md)|  |

### Return type

[**\Trollweb\EaccountingApi\Model\PartnerResourceLinkApi**](../Model/PartnerResourceLinkApi.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/xml, text/xml, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **partnerResourceLinksV2Put**
> \Trollweb\EaccountingApi\Model\PartnerResourceLinkApi partnerResourceLinksV2Put($partner_resource_link_id, $partner_resource_link)

Update a partner resource link

<p>Requires any of the following scopes: <br><b>ea:sales, ea:purchase, ea.local:mobile_user</b></p><p>Available in any of the following variants: <br><b>Pro, Standard, Invoicing, Bookkeeping, Solo, Albert</b></p>

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Trollweb\EaccountingApi\Api\PartnerResourceLinksVApi();
$partner_resource_link_id = "partner_resource_link_id_example"; // string | 
$partner_resource_link = new \Trollweb\EaccountingApi\Model\PartnerResourceLinkApi(); // \Trollweb\EaccountingApi\Model\PartnerResourceLinkApi | 

try {
    $result = $api_instance->partnerResourceLinksV2Put($partner_resource_link_id, $partner_resource_link);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PartnerResourceLinksVApi->partnerResourceLinksV2Put: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **partner_resource_link_id** | **string**|  |
 **partner_resource_link** | [**\Trollweb\EaccountingApi\Model\PartnerResourceLinkApi**](../Model/\Trollweb\EaccountingApi\Model\PartnerResourceLinkApi.md)|  |

### Return type

[**\Trollweb\EaccountingApi\Model\PartnerResourceLinkApi**](../Model/PartnerResourceLinkApi.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/xml, text/xml, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

