# Trollweb\EaccountingApi\BankVApi

All URIs are relative to *https://eaccountingapi-sandbox.test.vismaonline.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**bankV2Get**](BankVApi.md#bankV2Get) | **GET** /v2/banks | Get banks.


# **bankV2Get**
> \Trollweb\EaccountingApi\Model\PaginatedResponseBankApi bankV2Get()

Get banks.

<p>Requires any of the following scopes: <br><b>ea:purchase, ea:purchase_readonly, ea.local:mobile_user</b></p><p>Available in any of the following variants: <br><b>Pro, Standard, Invoicing, Bookkeeping, Solo, Albert</b></p>

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Trollweb\EaccountingApi\Api\BankVApi();

try {
    $result = $api_instance->bankV2Get();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BankVApi->bankV2Get: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\Trollweb\EaccountingApi\Model\PaginatedResponseBankApi**](../Model/PaginatedResponseBankApi.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

