# Trollweb\EaccountingApi\CostCenterItemsVApi

All URIs are relative to *https://eaccountingapi-sandbox.test.vismaonline.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**costCenterItemsV2Get**](CostCenterItemsVApi.md#costCenterItemsV2Get) | **GET** /v2/costcenteritems/{itemId} | Get a specific CostCenterItem.
[**costCenterItemsV2Post**](CostCenterItemsVApi.md#costCenterItemsV2Post) | **POST** /v2/costcenteritems | Create a single CostCenterItem.
[**costCenterItemsV2Put**](CostCenterItemsVApi.md#costCenterItemsV2Put) | **PUT** /v2/costcenteritems/{costCenterItemId} | Replace the data in an CostCenterItem.


# **costCenterItemsV2Get**
> \Trollweb\EaccountingApi\Model\CostCenterItemApi costCenterItemsV2Get($item_id)

Get a specific CostCenterItem.

<p>Requires any of the following scopes: <br><b>ea:accounting, ea:accounting_readonly, ea:sales, ea:sales_readonly, ea.local:mobile_user</b></p><p>Available in any of the following variants: <br><b>Pro, Standard, Bookkeeping, Invoicing, Albert</b></p>

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Trollweb\EaccountingApi\Api\CostCenterItemsVApi();
$item_id = "item_id_example"; // string | 

try {
    $result = $api_instance->costCenterItemsV2Get($item_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CostCenterItemsVApi->costCenterItemsV2Get: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **item_id** | **string**|  |

### Return type

[**\Trollweb\EaccountingApi\Model\CostCenterItemApi**](../Model/CostCenterItemApi.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **costCenterItemsV2Post**
> \Trollweb\EaccountingApi\Model\CostCenterItemApi costCenterItemsV2Post($cost_center_item)

Create a single CostCenterItem.

<p>Requires any of the following scopes: <br><b>ea:accounting, ea:sales, ea.local:mobile_user</b></p><p>Available in any of the following variants: <br><b>Pro, Standard, Bookkeeping, Invoicing, Albert</b></p>

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Trollweb\EaccountingApi\Api\CostCenterItemsVApi();
$cost_center_item = new \Trollweb\EaccountingApi\Model\CostCenterItemApi(); // \Trollweb\EaccountingApi\Model\CostCenterItemApi | 

try {
    $result = $api_instance->costCenterItemsV2Post($cost_center_item);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CostCenterItemsVApi->costCenterItemsV2Post: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cost_center_item** | [**\Trollweb\EaccountingApi\Model\CostCenterItemApi**](../Model/\Trollweb\EaccountingApi\Model\CostCenterItemApi.md)|  |

### Return type

[**\Trollweb\EaccountingApi\Model\CostCenterItemApi**](../Model/CostCenterItemApi.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/xml, text/xml, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **costCenterItemsV2Put**
> \Trollweb\EaccountingApi\Model\CostCenterItemApi costCenterItemsV2Put($cost_center_item_id, $cost_center_item)

Replace the data in an CostCenterItem.

<p>Requires any of the following scopes: <br><b>ea:accounting, ea:sales, ea.local:mobile_user</b></p><p>Available in any of the following variants: <br><b>Pro, Standard, Bookkeeping, Invoicing, Albert</b></p>

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Trollweb\EaccountingApi\Api\CostCenterItemsVApi();
$cost_center_item_id = "cost_center_item_id_example"; // string | 
$cost_center_item = new \Trollweb\EaccountingApi\Model\CostCenterItemApi(); // \Trollweb\EaccountingApi\Model\CostCenterItemApi | 

try {
    $result = $api_instance->costCenterItemsV2Put($cost_center_item_id, $cost_center_item);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CostCenterItemsVApi->costCenterItemsV2Put: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cost_center_item_id** | **string**|  |
 **cost_center_item** | [**\Trollweb\EaccountingApi\Model\CostCenterItemApi**](../Model/\Trollweb\EaccountingApi\Model\CostCenterItemApi.md)|  |

### Return type

[**\Trollweb\EaccountingApi\Model\CostCenterItemApi**](../Model/CostCenterItemApi.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/xml, text/xml, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

