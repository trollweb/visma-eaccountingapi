# Trollweb\EaccountingApi\CustomerLedgerItemsVApi

All URIs are relative to *https://eaccountingapi-sandbox.test.vismaonline.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**customerLedgerItemsV2Get**](CustomerLedgerItemsVApi.md#customerLedgerItemsV2Get) | **GET** /v2/customerledgeritems | Get a list of customer ledger items.
[**customerLedgerItemsV2Get_0**](CustomerLedgerItemsVApi.md#customerLedgerItemsV2Get_0) | **GET** /v2/customerledgeritems/{customerLedgerItemId} | Get a customer ledger item by id.
[**customerLedgerItemsV2Post**](CustomerLedgerItemsVApi.md#customerLedgerItemsV2Post) | **POST** /v2/customerledgeritems | Create a customer ledger item.
[**customerLedgerItemsV2Post_0**](CustomerLedgerItemsVApi.md#customerLedgerItemsV2Post_0) | **POST** /v2/customerledgeritems/customerledgeritemswithvoucher | Create a customer ledger item and a voucher included.


# **customerLedgerItemsV2Get**
> \Trollweb\EaccountingApi\Model\PaginatedResponseCustomerLedgerItemApi customerLedgerItemsV2Get()

Get a list of customer ledger items.

<p>Requires any of the following scopes: <br><b>ea:sales, ea:sales_readonly, ea.local:mobile_user</b></p><p>Available in any of the following variants: <br><b>Pro, Standard, Invoicing, Solo, Albert</b></p>

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Trollweb\EaccountingApi\Api\CustomerLedgerItemsVApi();

try {
    $result = $api_instance->customerLedgerItemsV2Get();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomerLedgerItemsVApi->customerLedgerItemsV2Get: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\Trollweb\EaccountingApi\Model\PaginatedResponseCustomerLedgerItemApi**](../Model/PaginatedResponseCustomerLedgerItemApi.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **customerLedgerItemsV2Get_0**
> \Trollweb\EaccountingApi\Model\CustomerLedgerItemApi customerLedgerItemsV2Get_0($customer_ledger_item_id)

Get a customer ledger item by id.

<p>Requires any of the following scopes: <br><b>ea:sales, ea:sales_readonly, ea.local:mobile_user</b></p><p>Available in any of the following variants: <br><b>Pro, Standard, Invoicing, Solo, Albert</b></p>

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Trollweb\EaccountingApi\Api\CustomerLedgerItemsVApi();
$customer_ledger_item_id = "customer_ledger_item_id_example"; // string | 

try {
    $result = $api_instance->customerLedgerItemsV2Get_0($customer_ledger_item_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomerLedgerItemsVApi->customerLedgerItemsV2Get_0: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **customer_ledger_item_id** | **string**|  |

### Return type

[**\Trollweb\EaccountingApi\Model\CustomerLedgerItemApi**](../Model/CustomerLedgerItemApi.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **customerLedgerItemsV2Post**
> \Trollweb\EaccountingApi\Model\CustomerLedgerItemApi customerLedgerItemsV2Post($customer_ledger_item)

Create a customer ledger item.

<p>Requires any of the following scopes: <br><b>ea:sales, ea.local:mobile_user</b></p><p>Available in any of the following variants: <br><b>Pro, Standard, Invoicing, Solo, Albert</b></p>

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Trollweb\EaccountingApi\Api\CustomerLedgerItemsVApi();
$customer_ledger_item = new \Trollweb\EaccountingApi\Model\CustomerLedgerItemApi(); // \Trollweb\EaccountingApi\Model\CustomerLedgerItemApi | 

try {
    $result = $api_instance->customerLedgerItemsV2Post($customer_ledger_item);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomerLedgerItemsVApi->customerLedgerItemsV2Post: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **customer_ledger_item** | [**\Trollweb\EaccountingApi\Model\CustomerLedgerItemApi**](../Model/\Trollweb\EaccountingApi\Model\CustomerLedgerItemApi.md)|  |

### Return type

[**\Trollweb\EaccountingApi\Model\CustomerLedgerItemApi**](../Model/CustomerLedgerItemApi.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/xml, text/xml, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **customerLedgerItemsV2Post_0**
> \Trollweb\EaccountingApi\Model\CustomerLedgerItemWithVoucherApi customerLedgerItemsV2Post_0($customer_ledger_item, $use_automatic_vat_calculation, $use_default_vat_codes, $use_default_voucher_series)

Create a customer ledger item and a voucher included.

<p>Requires any of the following scopes: <br><b>ea:sales, ea.local:mobile_user</b></p><p>Available in any of the following variants: <br><b>Pro, Standard, Invoicing, Solo, Albert</b></p>

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Trollweb\EaccountingApi\Api\CustomerLedgerItemsVApi();
$customer_ledger_item = new \Trollweb\EaccountingApi\Model\CustomerLedgerItemWithVoucherApi(); // \Trollweb\EaccountingApi\Model\CustomerLedgerItemWithVoucherApi | 
$use_automatic_vat_calculation = true; // bool | Default value: false. Set to true and specify the sales or purchase gross amount and vat rows will be added automatically.
$use_default_vat_codes = true; // bool | Default value: True. Set to false and override default vatcodes on all rows in the request.
$use_default_voucher_series = true; // bool | Default value: True. Set to false and override default voucher series (alphabetic character before number).

try {
    $result = $api_instance->customerLedgerItemsV2Post_0($customer_ledger_item, $use_automatic_vat_calculation, $use_default_vat_codes, $use_default_voucher_series);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomerLedgerItemsVApi->customerLedgerItemsV2Post_0: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **customer_ledger_item** | [**\Trollweb\EaccountingApi\Model\CustomerLedgerItemWithVoucherApi**](../Model/\Trollweb\EaccountingApi\Model\CustomerLedgerItemWithVoucherApi.md)|  |
 **use_automatic_vat_calculation** | **bool**| Default value: false. Set to true and specify the sales or purchase gross amount and vat rows will be added automatically. | [optional]
 **use_default_vat_codes** | **bool**| Default value: True. Set to false and override default vatcodes on all rows in the request. | [optional]
 **use_default_voucher_series** | **bool**| Default value: True. Set to false and override default voucher series (alphabetic character before number). | [optional]

### Return type

[**\Trollweb\EaccountingApi\Model\CustomerLedgerItemWithVoucherApi**](../Model/CustomerLedgerItemWithVoucherApi.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/xml, text/xml, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

