# Trollweb\EaccountingApi\ApprovalVApi

All URIs are relative to *https://eaccountingapi-sandbox.test.vismaonline.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**approvalV2Put**](ApprovalVApi.md#approvalV2Put) | **PUT** /v2/approval/vatreport/{id} | Update the approval status of a vat report


# **approvalV2Put**
> \Trollweb\EaccountingApi\Model\VatReportApi approvalV2Put($approval_model, $id)

Update the approval status of a vat report

<p>Requires any of the following scopes: <br><b>ea:accounting, ea:accounting_readonly, ea.local:mobile_user</b></p><p>Available in any of the following variants: <br><b>Pro, Standard, Bookkeeping, Solo, Albert</b></p>

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Trollweb\EaccountingApi\Api\ApprovalVApi();
$approval_model = new \Trollweb\EaccountingApi\Model\ApprovalApi(); // \Trollweb\EaccountingApi\Model\ApprovalApi | 
$id = "id_example"; // string | The id of the vat report

try {
    $result = $api_instance->approvalV2Put($approval_model, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ApprovalVApi->approvalV2Put: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **approval_model** | [**\Trollweb\EaccountingApi\Model\ApprovalApi**](../Model/\Trollweb\EaccountingApi\Model\ApprovalApi.md)|  |
 **id** | **string**| The id of the vat report |

### Return type

[**\Trollweb\EaccountingApi\Model\VatReportApi**](../Model/VatReportApi.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/xml, text/xml, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

