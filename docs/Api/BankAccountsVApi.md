# Trollweb\EaccountingApi\BankAccountsVApi

All URIs are relative to *https://eaccountingapi-sandbox.test.vismaonline.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**bankAccountsV2Delete**](BankAccountsVApi.md#bankAccountsV2Delete) | **DELETE** /v2/bankaccounts/{bankAccountId} | Delete a bank account.
[**bankAccountsV2Get**](BankAccountsVApi.md#bankAccountsV2Get) | **GET** /v2/bankaccounts | Get bank accounts.
[**bankAccountsV2Get_0**](BankAccountsVApi.md#bankAccountsV2Get_0) | **GET** /v2/bankaccounts/{bankAccountId} | Get a specific bank account.
[**bankAccountsV2Post**](BankAccountsVApi.md#bankAccountsV2Post) | **POST** /v2/bankaccounts | Add a bank account.
[**bankAccountsV2Put**](BankAccountsVApi.md#bankAccountsV2Put) | **PUT** /v2/bankaccounts/{bankAccountId} | Replace the data in a bank account.


# **bankAccountsV2Delete**
> object bankAccountsV2Delete($bank_account_id)

Delete a bank account.

<p>Requires any of the following scopes: <br><b>ea:purchase, ea.local:mobile_user</b></p><p>Available in any of the following variants: <br><b>Pro, Standard, Bookkeeping, Invoicing, Solo, Albert</b></p>

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Trollweb\EaccountingApi\Api\BankAccountsVApi();
$bank_account_id = "bank_account_id_example"; // string | 

try {
    $result = $api_instance->bankAccountsV2Delete($bank_account_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BankAccountsVApi->bankAccountsV2Delete: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **bank_account_id** | **string**|  |

### Return type

**object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **bankAccountsV2Get**
> \Trollweb\EaccountingApi\Model\PaginatedResponseBankAccountApi bankAccountsV2Get()

Get bank accounts.

<p>Requires any of the following scopes: <br><b>ea:purchase, ea:purchase_readonly, ea.local:mobile_user</b></p><p>Available in any of the following variants: <br><b>Pro, Standard, Bookkeeping, Invoicing, Solo, Albert</b></p>

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Trollweb\EaccountingApi\Api\BankAccountsVApi();

try {
    $result = $api_instance->bankAccountsV2Get();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BankAccountsVApi->bankAccountsV2Get: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\Trollweb\EaccountingApi\Model\PaginatedResponseBankAccountApi**](../Model/PaginatedResponseBankAccountApi.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **bankAccountsV2Get_0**
> \Trollweb\EaccountingApi\Model\BankAccountApi bankAccountsV2Get_0($bank_account_id)

Get a specific bank account.

<p>Requires any of the following scopes: <br><b>ea:purchase, ea:purchase_readonly, ea.local:mobile_user</b></p><p>Available in any of the following variants: <br><b>Pro, Standard, Bookkeeping, Invoicing, Solo, Albert</b></p>

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Trollweb\EaccountingApi\Api\BankAccountsVApi();
$bank_account_id = "bank_account_id_example"; // string | Id of the requsted bank account.

try {
    $result = $api_instance->bankAccountsV2Get_0($bank_account_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BankAccountsVApi->bankAccountsV2Get_0: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **bank_account_id** | **string**| Id of the requsted bank account. |

### Return type

[**\Trollweb\EaccountingApi\Model\BankAccountApi**](../Model/BankAccountApi.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **bankAccountsV2Post**
> \Trollweb\EaccountingApi\Model\BankAccountApi bankAccountsV2Post($bank_account)

Add a bank account.

<p>Requires any of the following scopes: <br><b>ea:purchase, ea.local:mobile_user</b></p><p>Available in any of the following variants: <br><b>Pro, Standard, Bookkeeping, Invoicing, Solo, Albert</b></p>

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Trollweb\EaccountingApi\Api\BankAccountsVApi();
$bank_account = new \Trollweb\EaccountingApi\Model\BankAccountApi(); // \Trollweb\EaccountingApi\Model\BankAccountApi | 

try {
    $result = $api_instance->bankAccountsV2Post($bank_account);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BankAccountsVApi->bankAccountsV2Post: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **bank_account** | [**\Trollweb\EaccountingApi\Model\BankAccountApi**](../Model/\Trollweb\EaccountingApi\Model\BankAccountApi.md)|  |

### Return type

[**\Trollweb\EaccountingApi\Model\BankAccountApi**](../Model/BankAccountApi.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/xml, text/xml, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **bankAccountsV2Put**
> \Trollweb\EaccountingApi\Model\BankAccountApi bankAccountsV2Put($bank_account_id, $bank_account_changes)

Replace the data in a bank account.

<p>Requires any of the following scopes: <br><b>ea:purchase, ea.local:mobile_user</b></p><p>Available in any of the following variants: <br><b>Pro, Standard, Bookkeeping, Invoicing, Solo, Albert</b></p>

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Trollweb\EaccountingApi\Api\BankAccountsVApi();
$bank_account_id = "bank_account_id_example"; // string | 
$bank_account_changes = new \Trollweb\EaccountingApi\Model\BankAccountApi(); // \Trollweb\EaccountingApi\Model\BankAccountApi | 

try {
    $result = $api_instance->bankAccountsV2Put($bank_account_id, $bank_account_changes);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BankAccountsVApi->bankAccountsV2Put: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **bank_account_id** | **string**|  |
 **bank_account_changes** | [**\Trollweb\EaccountingApi\Model\BankAccountApi**](../Model/\Trollweb\EaccountingApi\Model\BankAccountApi.md)|  |

### Return type

[**\Trollweb\EaccountingApi\Model\BankAccountApi**](../Model/BankAccountApi.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/xml, text/xml, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

