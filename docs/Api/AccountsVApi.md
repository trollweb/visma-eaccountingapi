# Trollweb\EaccountingApi\AccountsVApi

All URIs are relative to *https://eaccountingapi-sandbox.test.vismaonline.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**accountsV2Get**](AccountsVApi.md#accountsV2Get) | **GET** /v2/accounts | Get a list of accounts from all fiscalyears
[**accountsV2Get_0**](AccountsVApi.md#accountsV2Get_0) | **GET** /v2/accounts/standardaccounts | Get a list of the predefined standard accounts for dutch companies. This endpoint is only available in the Netherlands.
[**accountsV2Get_1**](AccountsVApi.md#accountsV2Get_1) | **GET** /v2/accounts/{fiscalyearId} | Get a list of accounts for a spcific fiscalyear
[**accountsV2Get_2**](AccountsVApi.md#accountsV2Get_2) | **GET** /v2/accounts/{fiscalyearId}/{accountNumber} | Get a single account by account number
[**accountsV2Post**](AccountsVApi.md#accountsV2Post) | **POST** /v2/accounts | Add account
[**accountsV2Put**](AccountsVApi.md#accountsV2Put) | **PUT** /v2/accounts/{fiscalyearId}/{accountNumber} | Replaces a account in a given fiscalyear


# **accountsV2Get**
> \Trollweb\EaccountingApi\Model\PaginatedResponseAccountApi accountsV2Get()

Get a list of accounts from all fiscalyears

<p>Requires any of the following scopes: <br><b>ea:accounting, ea:accounting_readonly, ea.local:mobile_user</b></p><p>Available in any of the following variants: <br><b>Pro, Standard, Invoicing, Bookkeeping, Solo, Albert</b></p>

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Trollweb\EaccountingApi\Api\AccountsVApi();

try {
    $result = $api_instance->accountsV2Get();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AccountsVApi->accountsV2Get: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\Trollweb\EaccountingApi\Model\PaginatedResponseAccountApi**](../Model/PaginatedResponseAccountApi.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **accountsV2Get_0**
> \Trollweb\EaccountingApi\Model\PaginatedResponseStandardAccountApi accountsV2Get_0()

Get a list of the predefined standard accounts for dutch companies. This endpoint is only available in the Netherlands.

<p>Requires any of the following scopes: <br><b>ea:accounting, ea:accounting_readonly, ea.local:mobile_user</b></p><p>Available in any of the following variants: <br><b>Pro, Standard, Invoicing, Bookkeeping, Solo, Albert</b></p>

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Trollweb\EaccountingApi\Api\AccountsVApi();

try {
    $result = $api_instance->accountsV2Get_0();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AccountsVApi->accountsV2Get_0: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\Trollweb\EaccountingApi\Model\PaginatedResponseStandardAccountApi**](../Model/PaginatedResponseStandardAccountApi.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **accountsV2Get_1**
> \Trollweb\EaccountingApi\Model\PaginatedResponseAccountApi accountsV2Get_1($fiscalyear_id)

Get a list of accounts for a spcific fiscalyear

<p>Requires any of the following scopes: <br><b>ea:accounting, ea:accounting_readonly, ea.local:mobile_user</b></p><p>Available in any of the following variants: <br><b>Pro, Standard, Invoicing, Bookkeeping, Solo, Albert</b></p>

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Trollweb\EaccountingApi\Api\AccountsVApi();
$fiscalyear_id = "fiscalyear_id_example"; // string | 

try {
    $result = $api_instance->accountsV2Get_1($fiscalyear_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AccountsVApi->accountsV2Get_1: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fiscalyear_id** | **string**|  |

### Return type

[**\Trollweb\EaccountingApi\Model\PaginatedResponseAccountApi**](../Model/PaginatedResponseAccountApi.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **accountsV2Get_2**
> \Trollweb\EaccountingApi\Model\AccountApi accountsV2Get_2($fiscalyear_id, $account_number)

Get a single account by account number

<p>Requires any of the following scopes: <br><b>ea:accounting, ea:accounting_readonly, ea.local:mobile_user</b></p><p>Available in any of the following variants: <br><b>Pro, Standard, Invoicing, Bookkeeping, Solo, Albert</b></p>

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Trollweb\EaccountingApi\Api\AccountsVApi();
$fiscalyear_id = "fiscalyear_id_example"; // string | 
$account_number = 56; // int | 

try {
    $result = $api_instance->accountsV2Get_2($fiscalyear_id, $account_number);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AccountsVApi->accountsV2Get_2: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fiscalyear_id** | **string**|  |
 **account_number** | **int**|  |

### Return type

[**\Trollweb\EaccountingApi\Model\AccountApi**](../Model/AccountApi.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **accountsV2Post**
> \Trollweb\EaccountingApi\Model\AccountApi accountsV2Post($api_account, $use_default_account_type)

Add account

<p>Requires any of the following scopes: <br><b>ea:accounting, ea.local:mobile_user</b></p><p>Available in any of the following variants: <br><b>Pro, Standard, Invoicing, Bookkeeping, Solo, Albert</b></p>

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Trollweb\EaccountingApi\Api\AccountsVApi();
$api_account = new \Trollweb\EaccountingApi\Model\AccountApi(); // \Trollweb\EaccountingApi\Model\AccountApi | 
$use_default_account_type = true; // bool | If true, eAccounting will provide the standard account type.               If false, you have to provide your own, can be found at /v2/accounttypes.               Account types can only be set on dutch companies.

try {
    $result = $api_instance->accountsV2Post($api_account, $use_default_account_type);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AccountsVApi->accountsV2Post: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **api_account** | [**\Trollweb\EaccountingApi\Model\AccountApi**](../Model/\Trollweb\EaccountingApi\Model\AccountApi.md)|  |
 **use_default_account_type** | **bool**| If true, eAccounting will provide the standard account type.               If false, you have to provide your own, can be found at /v2/accounttypes.               Account types can only be set on dutch companies. | [optional]

### Return type

[**\Trollweb\EaccountingApi\Model\AccountApi**](../Model/AccountApi.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/xml, text/xml, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **accountsV2Put**
> \Trollweb\EaccountingApi\Model\AccountApi accountsV2Put($fiscalyear_id, $account_number, $replaced_account)

Replaces a account in a given fiscalyear

<p>Requires any of the following scopes: <br><b>ea:accounting, ea.local:mobile_user</b></p><p>Available in any of the following variants: <br><b>Pro, Standard, Invoicing, Bookkeeping, Solo, Albert</b></p>

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Trollweb\EaccountingApi\Api\AccountsVApi();
$fiscalyear_id = "fiscalyear_id_example"; // string | 
$account_number = 789; // int | 
$replaced_account = new \Trollweb\EaccountingApi\Model\AccountApi(); // \Trollweb\EaccountingApi\Model\AccountApi | 

try {
    $result = $api_instance->accountsV2Put($fiscalyear_id, $account_number, $replaced_account);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AccountsVApi->accountsV2Put: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fiscalyear_id** | **string**|  |
 **account_number** | **int**|  |
 **replaced_account** | [**\Trollweb\EaccountingApi\Model\AccountApi**](../Model/\Trollweb\EaccountingApi\Model\AccountApi.md)|  |

### Return type

[**\Trollweb\EaccountingApi\Model\AccountApi**](../Model/AccountApi.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/xml, text/xml, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

