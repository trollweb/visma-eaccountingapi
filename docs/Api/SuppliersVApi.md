# Trollweb\EaccountingApi\SuppliersVApi

All URIs are relative to *https://eaccountingapi-sandbox.test.vismaonline.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**suppliersV2Delete**](SuppliersVApi.md#suppliersV2Delete) | **DELETE** /v2/suppliers/{supplierId} | Deletes a supplier
[**suppliersV2Get**](SuppliersVApi.md#suppliersV2Get) | **GET** /v2/suppliers | Get a list of suppliers.
[**suppliersV2Get_0**](SuppliersVApi.md#suppliersV2Get_0) | **GET** /v2/suppliers/{supplierId} | Get a specific supplier.
[**suppliersV2Post**](SuppliersVApi.md#suppliersV2Post) | **POST** /v2/suppliers | Post a supplier
[**suppliersV2Put**](SuppliersVApi.md#suppliersV2Put) | **PUT** /v2/suppliers/{supplierId} | Replace a supplier


# **suppliersV2Delete**
> object suppliersV2Delete($supplier_id)

Deletes a supplier

<p>Requires any of the following scopes: <br><b>ea:purchase, ea.local:mobile_user</b></p><p>Available in any of the following variants: <br><b>Pro, Standard, Solo, Albert</b></p>

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Trollweb\EaccountingApi\Api\SuppliersVApi();
$supplier_id = "supplier_id_example"; // string | 

try {
    $result = $api_instance->suppliersV2Delete($supplier_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SuppliersVApi->suppliersV2Delete: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **supplier_id** | **string**|  |

### Return type

**object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **suppliersV2Get**
> \Trollweb\EaccountingApi\Model\PaginatedResponseSupplierApi suppliersV2Get()

Get a list of suppliers.

<p>Requires any of the following scopes: <br><b>ea:purchase, ea:purchase_readonly, ea.local:mobile_user</b></p><p>Available in any of the following variants: <br><b>Pro, Standard, Solo, Albert</b></p>

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Trollweb\EaccountingApi\Api\SuppliersVApi();

try {
    $result = $api_instance->suppliersV2Get();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SuppliersVApi->suppliersV2Get: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\Trollweb\EaccountingApi\Model\PaginatedResponseSupplierApi**](../Model/PaginatedResponseSupplierApi.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **suppliersV2Get_0**
> \Trollweb\EaccountingApi\Model\SupplierApi suppliersV2Get_0($supplier_id)

Get a specific supplier.

<p>Requires any of the following scopes: <br><b>ea:purchase, ea:purchase_readonly, ea.local:mobile_user</b></p><p>Available in any of the following variants: <br><b>Pro, Standard, Solo, Albert</b></p>

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Trollweb\EaccountingApi\Api\SuppliersVApi();
$supplier_id = "supplier_id_example"; // string | 

try {
    $result = $api_instance->suppliersV2Get_0($supplier_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SuppliersVApi->suppliersV2Get_0: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **supplier_id** | **string**|  |

### Return type

[**\Trollweb\EaccountingApi\Model\SupplierApi**](../Model/SupplierApi.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **suppliersV2Post**
> \Trollweb\EaccountingApi\Model\SupplierApi suppliersV2Post($supplier)

Post a supplier

<p>Requires any of the following scopes: <br><b>ea:purchase, ea.local:mobile_user</b></p><p>Available in any of the following variants: <br><b>Pro, Standard, Solo, Albert</b></p>

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Trollweb\EaccountingApi\Api\SuppliersVApi();
$supplier = new \Trollweb\EaccountingApi\Model\SupplierApi(); // \Trollweb\EaccountingApi\Model\SupplierApi | 

try {
    $result = $api_instance->suppliersV2Post($supplier);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SuppliersVApi->suppliersV2Post: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **supplier** | [**\Trollweb\EaccountingApi\Model\SupplierApi**](../Model/\Trollweb\EaccountingApi\Model\SupplierApi.md)|  |

### Return type

[**\Trollweb\EaccountingApi\Model\SupplierApi**](../Model/SupplierApi.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/xml, text/xml, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **suppliersV2Put**
> \Trollweb\EaccountingApi\Model\SupplierApi suppliersV2Put($supplier_id, $api_supplier)

Replace a supplier

<p>Requires any of the following scopes: <br><b>ea:purchase, ea.local:mobile_user</b></p><p>Available in any of the following variants: <br><b>Pro, Standard, Solo, Albert</b></p>

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Trollweb\EaccountingApi\Api\SuppliersVApi();
$supplier_id = "supplier_id_example"; // string | 
$api_supplier = new \Trollweb\EaccountingApi\Model\SupplierApi(); // \Trollweb\EaccountingApi\Model\SupplierApi | 

try {
    $result = $api_instance->suppliersV2Put($supplier_id, $api_supplier);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SuppliersVApi->suppliersV2Put: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **supplier_id** | **string**|  |
 **api_supplier** | [**\Trollweb\EaccountingApi\Model\SupplierApi**](../Model/\Trollweb\EaccountingApi\Model\SupplierApi.md)|  |

### Return type

[**\Trollweb\EaccountingApi\Model\SupplierApi**](../Model/SupplierApi.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/xml, text/xml, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

