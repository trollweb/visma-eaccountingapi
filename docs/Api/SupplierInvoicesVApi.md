# Trollweb\EaccountingApi\SupplierInvoicesVApi

All URIs are relative to *https://eaccountingapi-sandbox.test.vismaonline.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**supplierInvoicesV2Get**](SupplierInvoicesVApi.md#supplierInvoicesV2Get) | **GET** /v2/supplierinvoices | Get a list of supplier invoices
[**supplierInvoicesV2Get_0**](SupplierInvoicesVApi.md#supplierInvoicesV2Get_0) | **GET** /v2/supplierinvoices/{supplierInvoiceId} | Get a supplier


# **supplierInvoicesV2Get**
> \Trollweb\EaccountingApi\Model\PaginatedResponseSupplierInvoiceApi supplierInvoicesV2Get()

Get a list of supplier invoices

<p>Requires any of the following scopes: <br><b>ea:purchase, ea:purchase_readonly, ea.local:mobile_user</b></p><p>Available in any of the following variants: <br><b>Pro, Standard, Bookkeeping, Solo, Albert</b></p>

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Trollweb\EaccountingApi\Api\SupplierInvoicesVApi();

try {
    $result = $api_instance->supplierInvoicesV2Get();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SupplierInvoicesVApi->supplierInvoicesV2Get: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\Trollweb\EaccountingApi\Model\PaginatedResponseSupplierInvoiceApi**](../Model/PaginatedResponseSupplierInvoiceApi.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **supplierInvoicesV2Get_0**
> \Trollweb\EaccountingApi\Model\SupplierInvoiceApi supplierInvoicesV2Get_0($supplier_invoice_id)

Get a supplier

<p>Requires any of the following scopes: <br><b>ea:purchase, ea:purchase_readonly, ea.local:mobile_user</b></p><p>Available in any of the following variants: <br><b>Pro, Standard, Bookkeeping, Solo, Albert</b></p>

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Trollweb\EaccountingApi\Api\SupplierInvoicesVApi();
$supplier_invoice_id = "supplier_invoice_id_example"; // string | 

try {
    $result = $api_instance->supplierInvoicesV2Get_0($supplier_invoice_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SupplierInvoicesVApi->supplierInvoicesV2Get_0: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **supplier_invoice_id** | **string**|  |

### Return type

[**\Trollweb\EaccountingApi\Model\SupplierInvoiceApi**](../Model/SupplierInvoiceApi.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

