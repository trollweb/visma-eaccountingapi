# Trollweb\EaccountingApi\FiscalYearsVApi

All URIs are relative to *https://eaccountingapi-sandbox.test.vismaonline.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**fiscalYearsV2Get**](FiscalYearsVApi.md#fiscalYearsV2Get) | **GET** /v2/fiscalyears | Get a list of fiscal years.
[**fiscalYearsV2Get_0**](FiscalYearsVApi.md#fiscalYearsV2Get_0) | **GET** /v2/fiscalyears/openingbalances | Gets the opening balances of the first fiscal year. If you want balances of following years, use the GET /accountbalances instead.
[**fiscalYearsV2Get_1**](FiscalYearsVApi.md#fiscalYearsV2Get_1) | **GET** /v2/fiscalyears/{id} | Get a singel fiscal year.
[**fiscalYearsV2Post**](FiscalYearsVApi.md#fiscalYearsV2Post) | **POST** /v2/fiscalyears | Create a fiscal year.


# **fiscalYearsV2Get**
> \Trollweb\EaccountingApi\Model\PaginatedResponseFiscalYearApi fiscalYearsV2Get()

Get a list of fiscal years.

<p>Requires any of the following scopes: <br><b>ea:sales, ea:sales_readonly, ea:accounting, ea:accounting_readonly, ea.local:mobile_user</b></p><p>Available in any of the following variants: <br><b>Pro, Standard, Invoicing, Bookkeeping, Solo, Albert</b></p>

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Trollweb\EaccountingApi\Api\FiscalYearsVApi();

try {
    $result = $api_instance->fiscalYearsV2Get();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling FiscalYearsVApi->fiscalYearsV2Get: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\Trollweb\EaccountingApi\Model\PaginatedResponseFiscalYearApi**](../Model/PaginatedResponseFiscalYearApi.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **fiscalYearsV2Get_0**
> \Trollweb\EaccountingApi\Model\PaginatedResponseOpeningBalancesApi fiscalYearsV2Get_0()

Gets the opening balances of the first fiscal year. If you want balances of following years, use the GET /accountbalances instead.

<p>Requires any of the following scopes: <br><b>ea:sales, ea:sales_readonly, ea:accounting, ea:accounting_readonly, ea.local:mobile_user</b></p><p>Available in any of the following variants: <br><b>Pro, Standard, Invoicing, Bookkeeping, Solo, Albert</b></p>

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Trollweb\EaccountingApi\Api\FiscalYearsVApi();

try {
    $result = $api_instance->fiscalYearsV2Get_0();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling FiscalYearsVApi->fiscalYearsV2Get_0: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\Trollweb\EaccountingApi\Model\PaginatedResponseOpeningBalancesApi**](../Model/PaginatedResponseOpeningBalancesApi.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **fiscalYearsV2Get_1**
> \Trollweb\EaccountingApi\Model\FiscalYearApi fiscalYearsV2Get_1($id)

Get a singel fiscal year.

<p>Requires any of the following scopes: <br><b>ea:sales, ea:sales_readonly, ea:accounting, ea:accounting_readonly, ea.local:mobile_user</b></p><p>Available in any of the following variants: <br><b>Pro, Standard, Invoicing, Bookkeeping, Solo, Albert</b></p>

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Trollweb\EaccountingApi\Api\FiscalYearsVApi();
$id = "id_example"; // string | Id of requested fiscal year.

try {
    $result = $api_instance->fiscalYearsV2Get_1($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling FiscalYearsVApi->fiscalYearsV2Get_1: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Id of requested fiscal year. |

### Return type

[**\Trollweb\EaccountingApi\Model\FiscalYearApi**](../Model/FiscalYearApi.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **fiscalYearsV2Post**
> \Trollweb\EaccountingApi\Model\FiscalYearApi fiscalYearsV2Post($fiscal_year)

Create a fiscal year.

<p>Requires any of the following scopes: <br><b>ea:sales, ea:accounting, ea.local:mobile_user</b></p><p>Available in any of the following variants: <br><b>Pro, Standard, Invoicing, Bookkeeping, Solo, Albert</b></p>

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Trollweb\EaccountingApi\Api\FiscalYearsVApi();
$fiscal_year = new \Trollweb\EaccountingApi\Model\FiscalYearApi(); // \Trollweb\EaccountingApi\Model\FiscalYearApi | 

try {
    $result = $api_instance->fiscalYearsV2Post($fiscal_year);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling FiscalYearsVApi->fiscalYearsV2Post: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fiscal_year** | [**\Trollweb\EaccountingApi\Model\FiscalYearApi**](../Model/\Trollweb\EaccountingApi\Model\FiscalYearApi.md)|  |

### Return type

[**\Trollweb\EaccountingApi\Model\FiscalYearApi**](../Model/FiscalYearApi.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/xml, text/xml, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

