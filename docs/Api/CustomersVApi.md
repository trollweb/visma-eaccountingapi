# Trollweb\EaccountingApi\CustomersVApi

All URIs are relative to *https://eaccountingapi-sandbox.test.vismaonline.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**customersV2Delete**](CustomersVApi.md#customersV2Delete) | **DELETE** /v2/customers/{customerId} | Delete a customer.
[**customersV2Get**](CustomersVApi.md#customersV2Get) | **GET** /v2/customers | Get customers.
[**customersV2Get_0**](CustomersVApi.md#customersV2Get_0) | **GET** /v2/customers/{customerId} | Get a specific customer.
[**customersV2Post**](CustomersVApi.md#customersV2Post) | **POST** /v2/customers | Add a customer.
[**customersV2Put**](CustomersVApi.md#customersV2Put) | **PUT** /v2/customers/{customerId} | Replace the data in a customer.


# **customersV2Delete**
> object customersV2Delete($customer_id)

Delete a customer.

<p>Requires any of the following scopes: <br><b>ea:sales, ea.local:mobile_user</b></p><p>Available in any of the following variants: <br><b>Pro, Standard, Invoicing, Solo, Albert</b></p>

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Trollweb\EaccountingApi\Api\CustomersVApi();
$customer_id = "customer_id_example"; // string | 

try {
    $result = $api_instance->customersV2Delete($customer_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomersVApi->customersV2Delete: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **customer_id** | **string**|  |

### Return type

**object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **customersV2Get**
> \Trollweb\EaccountingApi\Model\PaginatedResponseCustomerApi customersV2Get()

Get customers.

<p>Requires any of the following scopes: <br><b>ea:sales, ea:sales_readonly, ea.local:mobile_user</b></p><p>Available in any of the following variants: <br><b>Pro, Standard, Invoicing, Solo, Albert</b></p>

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Trollweb\EaccountingApi\Api\CustomersVApi();

try {
    $result = $api_instance->customersV2Get();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomersVApi->customersV2Get: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\Trollweb\EaccountingApi\Model\PaginatedResponseCustomerApi**](../Model/PaginatedResponseCustomerApi.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **customersV2Get_0**
> object customersV2Get_0($customer_id)

Get a specific customer.

<p>Requires any of the following scopes: <br><b>ea:sales, ea:sales_readonly, ea.local:mobile_user</b></p><p>Available in any of the following variants: <br><b>Pro, Standard, Invoicing, Solo, Albert</b></p>

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Trollweb\EaccountingApi\Api\CustomersVApi();
$customer_id = "customer_id_example"; // string | Id of the requsted customer.

try {
    $result = $api_instance->customersV2Get_0($customer_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomersVApi->customersV2Get_0: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **customer_id** | **string**| Id of the requsted customer. |

### Return type

**object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **customersV2Post**
> \Trollweb\EaccountingApi\Model\CustomerApi customersV2Post($customer)

Add a customer.

<p>Requires any of the following scopes: <br><b>ea:sales, ea.local:mobile_user</b></p><p>Available in any of the following variants: <br><b>Pro, Standard, Invoicing, Solo, Albert</b></p>

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Trollweb\EaccountingApi\Api\CustomersVApi();
$customer = new \Trollweb\EaccountingApi\Model\CustomerApi(); // \Trollweb\EaccountingApi\Model\CustomerApi | 

try {
    $result = $api_instance->customersV2Post($customer);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomersVApi->customersV2Post: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **customer** | [**\Trollweb\EaccountingApi\Model\CustomerApi**](../Model/\Trollweb\EaccountingApi\Model\CustomerApi.md)|  |

### Return type

[**\Trollweb\EaccountingApi\Model\CustomerApi**](../Model/CustomerApi.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/xml, text/xml, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **customersV2Put**
> \Trollweb\EaccountingApi\Model\CustomerApi customersV2Put($customer_id, $updated_customer)

Replace the data in a customer.

<p>Requires any of the following scopes: <br><b>ea:sales, ea.local:mobile_user</b></p><p>Available in any of the following variants: <br><b>Pro, Standard, Invoicing, Solo, Albert</b></p>

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Trollweb\EaccountingApi\Api\CustomersVApi();
$customer_id = "customer_id_example"; // string | 
$updated_customer = new \Trollweb\EaccountingApi\Model\CustomerApi(); // \Trollweb\EaccountingApi\Model\CustomerApi | 

try {
    $result = $api_instance->customersV2Put($customer_id, $updated_customer);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomersVApi->customersV2Put: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **customer_id** | **string**|  |
 **updated_customer** | [**\Trollweb\EaccountingApi\Model\CustomerApi**](../Model/\Trollweb\EaccountingApi\Model\CustomerApi.md)|  |

### Return type

[**\Trollweb\EaccountingApi\Model\CustomerApi**](../Model/CustomerApi.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/xml, text/xml, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

