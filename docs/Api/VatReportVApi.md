# Trollweb\EaccountingApi\VatReportVApi

All URIs are relative to *https://eaccountingapi-sandbox.test.vismaonline.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**vatReportV2Get**](VatReportVApi.md#vatReportV2Get) | **GET** /v2/vatreports | Gets a list of all vat reports
[**vatReportV2Get_0**](VatReportVApi.md#vatReportV2Get_0) | **GET** /v2/vatreports/{id} | Get a vat report item by id.


# **vatReportV2Get**
> \Trollweb\EaccountingApi\Model\VatReportApi[] vatReportV2Get()

Gets a list of all vat reports

<p>Requires any of the following scopes: <br><b>ea:accounting, ea:accounting_readonly, ea.local:mobile_user</b></p><p>Available in any of the following variants: <br><b>Pro, Standard, Bookkeeping, Solo, Albert</b></p>

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Trollweb\EaccountingApi\Api\VatReportVApi();

try {
    $result = $api_instance->vatReportV2Get();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling VatReportVApi->vatReportV2Get: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\Trollweb\EaccountingApi\Model\VatReportApi[]**](../Model/VatReportApi.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **vatReportV2Get_0**
> \Trollweb\EaccountingApi\Model\VatReportApi vatReportV2Get_0($id)

Get a vat report item by id.

<p>Requires any of the following scopes: <br><b>ea:accounting, ea:accounting_readonly, ea.local:mobile_user</b></p><p>Available in any of the following variants: <br><b>Pro, Standard, Bookkeeping, Solo, Albert</b></p>

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Trollweb\EaccountingApi\Api\VatReportVApi();
$id = "id_example"; // string | 

try {
    $result = $api_instance->vatReportV2Get_0($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling VatReportVApi->vatReportV2Get_0: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**|  |

### Return type

[**\Trollweb\EaccountingApi\Model\VatReportApi**](../Model/VatReportApi.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

