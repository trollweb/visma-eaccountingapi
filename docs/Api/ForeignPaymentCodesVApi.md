# Trollweb\EaccountingApi\ForeignPaymentCodesVApi

All URIs are relative to *https://eaccountingapi-sandbox.test.vismaonline.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**foreignPaymentCodesV2Get**](ForeignPaymentCodesVApi.md#foreignPaymentCodesV2Get) | **GET** /v2/foreignpaymentcodes | Gets a list of foreign payment codes.


# **foreignPaymentCodesV2Get**
> \Trollweb\EaccountingApi\Model\PaginatedResponseForeignPaymentCodesAPI foreignPaymentCodesV2Get()

Gets a list of foreign payment codes.

<p>Requires any of the following scopes: <br><b>ea:purchase, ea:purchase_readonly, ea.local:mobile_user</b></p><p>Available in any of the following variants: <br><b>Pro, Standard, Albert</b></p>

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Trollweb\EaccountingApi\Api\ForeignPaymentCodesVApi();

try {
    $result = $api_instance->foreignPaymentCodesV2Get();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ForeignPaymentCodesVApi->foreignPaymentCodesV2Get: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\Trollweb\EaccountingApi\Model\PaginatedResponseForeignPaymentCodesAPI**](../Model/PaginatedResponseForeignPaymentCodesAPI.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

