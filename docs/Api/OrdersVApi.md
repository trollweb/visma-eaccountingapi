# Trollweb\EaccountingApi\OrdersVApi

All URIs are relative to *https://eaccountingapi-sandbox.test.vismaonline.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**ordersV2Delete**](OrdersVApi.md#ordersV2Delete) | **DELETE** /v2/orders/{id} | Delete an order.
[**ordersV2Get**](OrdersVApi.md#ordersV2Get) | **GET** /v2/orders | Get orders.
[**ordersV2Get_0**](OrdersVApi.md#ordersV2Get_0) | **GET** /v2/orders/{id} | Get order
[**ordersV2Post**](OrdersVApi.md#ordersV2Post) | **POST** /v2/orders | Create order.
[**ordersV2Put**](OrdersVApi.md#ordersV2Put) | **PUT** /v2/orders/{id} | Replace content in an order.


# **ordersV2Delete**
> object ordersV2Delete($id)

Delete an order.

<p>Requires any of the following scopes: <br><b>ea:sales, ea.local:mobile_user</b></p><p>Available in any of the following variants: <br><b>Pro, Standard, Invoicing, Albert</b></p>

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Trollweb\EaccountingApi\Api\OrdersVApi();
$id = "id_example"; // string | 

try {
    $result = $api_instance->ordersV2Delete($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OrdersVApi->ordersV2Delete: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**|  |

### Return type

**object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **ordersV2Get**
> \Trollweb\EaccountingApi\Model\PaginatedResponseOrderApi ordersV2Get()

Get orders.

<p>Requires any of the following scopes: <br><b>ea:sales, ea:sales_readonly, ea.local:mobile_user</b></p><p>Available in any of the following variants: <br><b>Pro, Standard, Invoicing, Albert</b></p>

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Trollweb\EaccountingApi\Api\OrdersVApi();

try {
    $result = $api_instance->ordersV2Get();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OrdersVApi->ordersV2Get: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\Trollweb\EaccountingApi\Model\PaginatedResponseOrderApi**](../Model/PaginatedResponseOrderApi.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **ordersV2Get_0**
> \Trollweb\EaccountingApi\Model\OrderApi ordersV2Get_0($id)

Get order

<p>Requires any of the following scopes: <br><b>ea:sales, ea:sales_readonly, ea.local:mobile_user</b></p><p>Available in any of the following variants: <br><b>Pro, Standard, Invoicing, Albert</b></p>

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Trollweb\EaccountingApi\Api\OrdersVApi();
$id = "id_example"; // string | 

try {
    $result = $api_instance->ordersV2Get_0($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OrdersVApi->ordersV2Get_0: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**|  |

### Return type

[**\Trollweb\EaccountingApi\Model\OrderApi**](../Model/OrderApi.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **ordersV2Post**
> \Trollweb\EaccountingApi\Model\OrderApi ordersV2Post($order)

Create order.

<p>Requires any of the following scopes: <br><b>ea:sales, ea.local:mobile_user</b></p><p>Available in any of the following variants: <br><b>Pro, Standard, Invoicing, Albert</b></p>

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Trollweb\EaccountingApi\Api\OrdersVApi();
$order = new \Trollweb\EaccountingApi\Model\OrderApi(); // \Trollweb\EaccountingApi\Model\OrderApi | 

try {
    $result = $api_instance->ordersV2Post($order);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OrdersVApi->ordersV2Post: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **order** | [**\Trollweb\EaccountingApi\Model\OrderApi**](../Model/\Trollweb\EaccountingApi\Model\OrderApi.md)|  |

### Return type

[**\Trollweb\EaccountingApi\Model\OrderApi**](../Model/OrderApi.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/xml, text/xml, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **ordersV2Put**
> object ordersV2Put($id, $order)

Replace content in an order.

<p>Requires any of the following scopes: <br><b>ea:sales, ea.local:mobile_user</b></p><p>Available in any of the following variants: <br><b>Pro, Standard, Invoicing, Albert</b></p>

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Trollweb\EaccountingApi\Api\OrdersVApi();
$id = "id_example"; // string | 
$order = new \Trollweb\EaccountingApi\Model\OrderApi(); // \Trollweb\EaccountingApi\Model\OrderApi | 

try {
    $result = $api_instance->ordersV2Put($id, $order);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OrdersVApi->ordersV2Put: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**|  |
 **order** | [**\Trollweb\EaccountingApi\Model\OrderApi**](../Model/\Trollweb\EaccountingApi\Model\OrderApi.md)|  |

### Return type

**object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/xml, text/xml, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

