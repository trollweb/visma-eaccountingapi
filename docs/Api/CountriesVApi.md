# Trollweb\EaccountingApi\CountriesVApi

All URIs are relative to *https://eaccountingapi-sandbox.test.vismaonline.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**countriesV2Get**](CountriesVApi.md#countriesV2Get) | **GET** /v2/countries | Get a list of Countries.
[**countriesV2Get_0**](CountriesVApi.md#countriesV2Get_0) | **GET** /v2/countries/{countrycode} | Get a singel country.


# **countriesV2Get**
> \Trollweb\EaccountingApi\Model\PaginatedResponseCountryApi countriesV2Get()

Get a list of Countries.

<p>Requires any of the following scopes: <br><b>ea:sales, ea:sales_readonly, ea.local:mobile_user</b></p><p>Available in any of the following variants: <br><b>Pro, Standard, Invoicing, Solo, Albert</b></p>

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Trollweb\EaccountingApi\Api\CountriesVApi();

try {
    $result = $api_instance->countriesV2Get();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CountriesVApi->countriesV2Get: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\Trollweb\EaccountingApi\Model\PaginatedResponseCountryApi**](../Model/PaginatedResponseCountryApi.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **countriesV2Get_0**
> \Trollweb\EaccountingApi\Model\CountryApi countriesV2Get_0($countrycode)

Get a singel country.

<p>Requires any of the following scopes: <br><b>ea:sales, ea:sales_readonly, ea.local:mobile_user</b></p><p>Available in any of the following variants: <br><b>Pro, Standard, Invoicing, Solo, Albert</b></p>

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Trollweb\EaccountingApi\Api\CountriesVApi();
$countrycode = "countrycode_example"; // string | Two letter code of requested country.

try {
    $result = $api_instance->countriesV2Get_0($countrycode);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CountriesVApi->countriesV2Get_0: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **countrycode** | **string**| Two letter code of requested country. |

### Return type

[**\Trollweb\EaccountingApi\Model\CountryApi**](../Model/CountryApi.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

