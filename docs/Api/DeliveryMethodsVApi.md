# Trollweb\EaccountingApi\DeliveryMethodsVApi

All URIs are relative to *https://eaccountingapi-sandbox.test.vismaonline.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**deliveryMethodsV2Get**](DeliveryMethodsVApi.md#deliveryMethodsV2Get) | **GET** /v2/deliverymethods | Get delivery methods.
[**deliveryMethodsV2Get_0**](DeliveryMethodsVApi.md#deliveryMethodsV2Get_0) | **GET** /v2/deliverymethods/{deliveryMethodId} | Get a delivery method.


# **deliveryMethodsV2Get**
> \Trollweb\EaccountingApi\Model\PaginatedResponseDeliveryMethodApi deliveryMethodsV2Get()

Get delivery methods.

<p>Requires any of the following scopes: <br><b>ea:sales, ea:sales_readonly, ea.local:mobile_user</b></p><p>Available in any of the following variants: <br><b>Pro, Standard, Invoicing, Solo, Albert</b></p>

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Trollweb\EaccountingApi\Api\DeliveryMethodsVApi();

try {
    $result = $api_instance->deliveryMethodsV2Get();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DeliveryMethodsVApi->deliveryMethodsV2Get: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\Trollweb\EaccountingApi\Model\PaginatedResponseDeliveryMethodApi**](../Model/PaginatedResponseDeliveryMethodApi.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deliveryMethodsV2Get_0**
> \Trollweb\EaccountingApi\Model\DeliveryMethodApi deliveryMethodsV2Get_0($delivery_method_id)

Get a delivery method.

<p>Requires any of the following scopes: <br><b>ea:sales, ea:sales_readonly, ea.local:mobile_user</b></p><p>Available in any of the following variants: <br><b>Pro, Standard, Invoicing, Solo, Albert</b></p>

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Trollweb\EaccountingApi\Api\DeliveryMethodsVApi();
$delivery_method_id = "delivery_method_id_example"; // string | 

try {
    $result = $api_instance->deliveryMethodsV2Get_0($delivery_method_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DeliveryMethodsVApi->deliveryMethodsV2Get_0: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **delivery_method_id** | **string**|  |

### Return type

[**\Trollweb\EaccountingApi\Model\DeliveryMethodApi**](../Model/DeliveryMethodApi.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

