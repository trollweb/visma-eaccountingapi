# Trollweb\EaccountingApi\CurrenciesVApi

All URIs are relative to *https://eaccountingapi-sandbox.test.vismaonline.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**currenciesV2Get**](CurrenciesVApi.md#currenciesV2Get) | **GET** /v2/currencies | Get a list of Currencies


# **currenciesV2Get**
> \Trollweb\EaccountingApi\Model\PaginatedResponseCurrencyApi currenciesV2Get()

Get a list of Currencies

<p>Requires any of the following scopes: <br><b>ea:sales, ea:sales_readonly, ea:purchase, ea:purchase_readonly, ea.local:mobile_user</b></p><p>Available in any of the following variants: <br><b>Pro, Standard, Invoicing, Albert</b></p>

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Trollweb\EaccountingApi\Api\CurrenciesVApi();

try {
    $result = $api_instance->currenciesV2Get();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CurrenciesVApi->currenciesV2Get: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\Trollweb\EaccountingApi\Model\PaginatedResponseCurrencyApi**](../Model/PaginatedResponseCurrencyApi.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

