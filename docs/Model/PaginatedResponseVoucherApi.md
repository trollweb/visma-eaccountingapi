# PaginatedResponseVoucherApi

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**meta** | [**\Trollweb\EaccountingApi\Model\PaginationMetadata**](PaginationMetadata.md) |  | [optional] 
**data** | [**\Trollweb\EaccountingApi\Model\VoucherApi[]**](VoucherApi.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


