# FilterQueryOption

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**context** | [**\Trollweb\EaccountingApi\Model\ODataQueryContext**](ODataQueryContext.md) |  | [optional] 
**validator** | [**\Trollweb\EaccountingApi\Model\FilterQueryValidator**](FilterQueryValidator.md) |  | [optional] 
**filter_clause** | [**\Trollweb\EaccountingApi\Model\FilterClause**](FilterClause.md) |  | [optional] 
**raw_value** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


