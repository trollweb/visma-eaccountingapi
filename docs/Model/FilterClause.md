# FilterClause

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**expression** | [**\Trollweb\EaccountingApi\Model\SingleValueNode**](SingleValueNode.md) |  | [optional] 
**range_variable** | [**\Trollweb\EaccountingApi\Model\RangeVariable**](RangeVariable.md) |  | [optional] 
**item_type** | [**\Trollweb\EaccountingApi\Model\IEdmTypeReference**](IEdmTypeReference.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


