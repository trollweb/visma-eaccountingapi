# TopQueryOption

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**context** | [**\Trollweb\EaccountingApi\Model\ODataQueryContext**](ODataQueryContext.md) |  | [optional] 
**raw_value** | **string** |  | [optional] 
**value** | **int** |  | [optional] 
**validator** | [**\Trollweb\EaccountingApi\Model\TopQueryValidator**](TopQueryValidator.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


