# ArticleApi

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** | Read-only: Unique Id provided by eAccounting | [optional] 
**is_active** | **bool** |  | 
**number** | **string** | Max length: 40 characters | 
**name** | **string** | Max length: 50 characters | 
**name_english** | **string** | Max length: 50 characters | [optional] 
**net_price** | **double** | Format: Max 2 decimals | [optional] 
**gross_price** | **double** | Format: Max 2 decimals | [optional] 
**coding_id** | **string** | Source: Get from /v1/articleaccountcodings | 
**unit_id** | **string** | Source: Get from /v1/units | 
**unit_name** | **string** | Read-only: Returns the unit name entered from UnitId | [optional] 
**unit_abbreviation** | **string** | Read-only: Returns the unit abbreviation entered from UnitId | [optional] 
**stock_balance** | **double** | Default: 0. Purpose: Sets the stock balance for this article | [optional] 
**stock_balance_reserved** | **double** | Purpose: Returns the reserved stock balance for this article | [optional] 
**stock_balance_available** | **double** | Purpose: Returns the available stock balance for this article | [optional] 
**changed_utc** | [**\DateTime**](\DateTime.md) | Purpose: Returns the last date and time from when a change was made on the article | [optional] 
**house_work_type** | **int** |  | [optional] 
**purchase_price** | **double** |  | [optional] 
**article_labels** | [**\Trollweb\EaccountingApi\Model\ArticleLabelApi[]**](ArticleLabelApi.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


