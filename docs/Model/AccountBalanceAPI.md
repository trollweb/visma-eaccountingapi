# AccountBalanceAPI

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**account_number** | **int** | Read-only. The account number | [optional] 
**account_name** | **string** | Read-only. The name of the account | [optional] 
**balance** | **double** | Read-only. The account balance | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


