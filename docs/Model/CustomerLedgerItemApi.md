# CustomerLedgerItemApi

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**currency_code** | **string** | Max length: 3 characters | 
**currency_rate** | **double** |  | [optional] 
**currency_rate_unit** | **int** |  | [optional] 
**customer_id** | **string** | Source: Get from /v1/customerlistitems. | 
**due_date** | [**\DateTime**](\DateTime.md) | Format: YYYY-MM-DD | 
**id** | **string** | Read-only: Unique Id provided by eAccounting | [optional] 
**invoice_date** | [**\DateTime**](\DateTime.md) | Format: YYYY-MM-DD | 
**invoice_number** | **int** |  | 
**is_credit_invoice** | **bool** |  | 
**payment_reference_number** | **string** |  | [optional] 
**remaining_amount_invoice_currency** | **double** |  | 
**roundings_amount_invoice_currency** | **double** |  | 
**total_amount_invoice_currency** | **double** |  | 
**vat_amount_invoice_currency** | **double** |  | 
**voucher_id** | **string** | Source: Get from v1/vouchers/{fiscalyearid}. | 
**modified_utc** | [**\DateTime**](\DateTime.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


