# SelectExpandQueryOption

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**context** | [**\Trollweb\EaccountingApi\Model\ODataQueryContext**](ODataQueryContext.md) |  | [optional] 
**raw_select** | **string** |  | [optional] 
**raw_expand** | **string** |  | [optional] 
**validator** | [**\Trollweb\EaccountingApi\Model\SelectExpandQueryValidator**](SelectExpandQueryValidator.md) |  | [optional] 
**select_expand_clause** | [**\Trollweb\EaccountingApi\Model\SelectExpandClause**](SelectExpandClause.md) |  | [optional] 
**levels_max_literal_expansion_depth** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


