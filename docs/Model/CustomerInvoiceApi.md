# CustomerInvoiceApi

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** |  | [optional] 
**eu_third_party** | **bool** |  | 
**is_credit_invoice** | **bool** |  | [optional] 
**currency_code** | **string** |  | 
**created_by_user_id** | **string** |  | [optional] 
**total_amount** | **double** |  | 
**total_vat_amount** | **double** |  | 
**total_roundings** | **double** |  | 
**customer_id** | **string** |  | 
**rows** | [**\Trollweb\EaccountingApi\Model\CustomerInvoiceRowApi[]**](CustomerInvoiceRowApi.md) |  | 
**invoice_date** | [**\DateTime**](\DateTime.md) |  | 
**due_date** | [**\DateTime**](\DateTime.md) |  | 
**delivery_date** | [**\DateTime**](\DateTime.md) |  | [optional] 
**rot_reduced_invoicing_type** | **int** | 0 &#x3D; Normal, 1 &#x3D; Rot, 2 &#x3D; Rut | 
**rot_reduced_invoicing_amount** | **double** |  | [optional] 
**rot_reduced_invoicing_percent** | **double** |  | [optional] 
**rot_reduced_invoicing_property_name** | **string** |  | [optional] 
**rot_reduced_invoicing_org_number** | **string** |  | [optional] 
**persons** | [**\Trollweb\EaccountingApi\Model\SalesDocumentRotRutReductionPersonApi[]**](SalesDocumentRotRutReductionPersonApi.md) |  | [optional] 
**rot_reduced_invoicing_automatic_distribution** | **bool** |  | [optional] 
**electronic_reference** | **string** |  | [optional] 
**electronic_address** | **string** |  | [optional] 
**edi_service_deliverer_id** | **string** |  | [optional] 
**our_reference** | **string** |  | [optional] 
**your_reference** | **string** |  | [optional] 
**invoice_customer_name** | **string** |  | 
**invoice_address1** | **string** |  | [optional] 
**invoice_address2** | **string** |  | [optional] 
**invoice_postal_code** | **string** |  | 
**invoice_city** | **string** |  | 
**invoice_country_code** | **string** |  | 
**delivery_customer_name** | **string** |  | [optional] 
**delivery_address1** | **string** |  | [optional] 
**delivery_address2** | **string** |  | [optional] 
**delivery_postal_code** | **string** |  | [optional] 
**delivery_city** | **string** |  | [optional] 
**delivery_country_code** | **string** |  | [optional] 
**delivery_method_name** | **string** |  | [optional] 
**delivery_term_name** | **string** |  | [optional] 
**delivery_method_code** | **string** |  | [optional] 
**delivery_term_code** | **string** |  | [optional] 
**customer_is_private_person** | **bool** |  | 
**terms_of_payment_id** | **string** |  | 
**customer_email** | **string** |  | [optional] 
**invoice_number** | **int** |  | [optional] 
**customer_number** | **string** |  | [optional] 
**payment_reference_number** | **string** |  | [optional] 
**rot_property_type** | **int** |  | [optional] 
**work_house_other_costs** | **double** |  | [optional] 
**remaining_amount** | **double** |  | [optional] 
**remaining_amount_invoice_currency** | **double** |  | [optional] 
**referring_invoice_id** | **string** |  | [optional] 
**voucher_number** | **string** |  | [optional] 
**voucher_id** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


