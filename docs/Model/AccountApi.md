# AccountApi

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **string** | Max length: 100 characters. The name of the account | 
**number** | **string** | The account number | 
**vat_code_id** | **string** | The Id of the VAT code that is associated with the account | [optional] 
**vat_code_description** | **string** | Read-only. Describes what kind of VAT that is associated with the account | [optional] 
**fiscal_year_id** | **string** | The Id of the Fiscal year that the account belongs to | 
**reference_code** | **string** | Read-only. Returns the reference code on the account. This feature is for dutch companies only | [optional] 
**type** | **int** | Read-only. Returns account type number. Netherlands only | [optional] 
**type_description** | **string** | Read-only. Returns account type descripion | [optional] 
**modified_utc** | [**\DateTime**](\DateTime.md) | Read-only. | [optional] 
**is_active** | **bool** |  | 
**is_project_allowed** | **bool** |  | [optional] 
**is_cost_center_allowed** | **bool** |  | [optional] 
**is_blocked_for_manual_booking** | **bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


