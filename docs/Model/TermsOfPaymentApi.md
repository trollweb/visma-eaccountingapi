# TermsOfPaymentApi

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** | Read-only: Unique Id provided by eAccounting | [optional] 
**name** | **string** |  | [optional] 
**name_english** | **string** |  | [optional] 
**number_of_days** | **int** |  | [optional] 
**terms_of_payment_type_id** | **int** |  | [optional] 
**terms_of_payment_type_text** | **string** |  | [optional] 
**available_for_sales** | **bool** |  | [optional] 
**available_for_purchase** | **bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


