# SupplierInvoiceRowApi

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** |  | [optional] 
**account_number** | **int** |  | [optional] 
**account_name** | **string** |  | [optional] 
**vad_code_id** | **string** |  | [optional] 
**cost_center_item_id1** | **string** |  | [optional] 
**cost_center_item_id2** | **string** |  | [optional] 
**cost_center_item_id3** | **string** |  | [optional] 
**quantity** | **double** |  | [optional] 
**weight** | **double** |  | [optional] 
**delivery_date** | [**\DateTime**](\DateTime.md) |  | [optional] 
**harvest_year** | **int** |  | [optional] 
**debet_amount** | **double** |  | [optional] 
**credit_amount** | **double** |  | [optional] 
**line_number** | **int** |  | [optional] 
**project_id** | **string** |  | [optional] 
**transaction_text** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


