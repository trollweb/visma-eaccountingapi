# SupplierInvoiceApi

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** |  | [optional] 
**supplier_id** | **string** |  | 
**bank_account_id** | **string** |  | [optional] 
**invoice_date** | [**\DateTime**](\DateTime.md) |  | [optional] 
**payment_date** | [**\DateTime**](\DateTime.md) |  | [optional] 
**due_date** | [**\DateTime**](\DateTime.md) |  | [optional] 
**invoice_number** | **string** |  | [optional] 
**total_amount** | **double** |  | [optional] 
**vat** | **double** |  | [optional] 
**vat_high** | **double** |  | [optional] 
**vat_medium** | **double** |  | [optional] 
**vat_low** | **double** |  | [optional] 
**is_credit_invoice** | **bool** |  | [optional] 
**currency_code** | **string** |  | [optional] 
**currency_rate** | **double** |  | [optional] 
**ocr_number** | **string** |  | [optional] 
**message** | **string** |  | [optional] 
**created_utc** | [**\DateTime**](\DateTime.md) |  | [optional] 
**modified_utc** | [**\DateTime**](\DateTime.md) |  | [optional] 
**plus_giro_number** | **string** |  | [optional] 
**bank_giro_number** | **string** |  | [optional] 
**rows** | [**\Trollweb\EaccountingApi\Model\SupplierInvoiceRowApi[]**](SupplierInvoiceRowApi.md) |  | [optional] 
**supplier_name** | **string** |  | [optional] 
**supplier_number** | **string** |  | [optional] 
**is_quick_invoice** | **bool** |  | [optional] 
**is_domestic** | **bool** |  | [optional] 
**remaining_amount** | **double** |  | [optional] 
**remaining_amount_invoice_currency** | **double** |  | [optional] 
**voucher_number** | **string** |  | [optional] 
**voucher_id** | **string** |  | [optional] 
**created_from_draft_id** | **string** |  | [optional] 
**self_employed_without_fixed_address** | **bool** |  | [optional] 
**allocation_periods** | [**\Trollweb\EaccountingApi\Model\AllocationPeriodApi[]**](AllocationPeriodApi.md) |  | [optional] 
**attachments** | **string[]** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


