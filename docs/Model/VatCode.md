# VatCode

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** | Read-only: Unique Id provided by eAccounting | [optional] 
**code** | **string** | Returns the VAT code | [optional] 
**description** | **string** |  | [optional] 
**vat_rate** | **double** |  | [optional] 
**related_accounts** | [**\Trollweb\EaccountingApi\Model\RelatedAccounts**](RelatedAccounts.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


