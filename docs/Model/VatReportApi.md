# VatReportApi

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** |  | [optional] 
**name** | **string** |  | [optional] 
**start_date** | [**\DateTime**](\DateTime.md) |  | [optional] 
**end_date** | [**\DateTime**](\DateTime.md) |  | [optional] 
**document_approval_status** | **int** | 0 &#x3D; None, 1 &#x3D; Approved, 2 &#x3D; Rejected, 3 &#x3D; ReadyForApproval | [optional] 
**document_id** | **string** | Purpose: Use for GET /v2/documents/{id} | [optional] 
**created_utc** | [**\DateTime**](\DateTime.md) |  | [optional] 
**is_regretted** | **bool** | Indicates whether the vat report was undone | [optional] 
**regretted_by_user_id** | **string** | If the vat report was undone this indicates the user id that did the action | [optional] 
**regretted_date** | [**\DateTime**](\DateTime.md) | If the vat report was undone this indicates the date of the action | [optional] 
**modified_utc** | [**\DateTime**](\DateTime.md) |  | [optional] 
**sent_for_approval_by_user_id** | **string** |  | [optional] 
**voucher_id** | **string** | Purpose: Use for GET /v2/vouchers/{fiscalyearId}/{voucherId} | [optional] 
**total_amount** | **double** | Predicted vat amount to pay or be refunded | [optional] 
**approval_events_history** | [**\Trollweb\EaccountingApi\Model\DocumentApprovalEventApi[]**](DocumentApprovalEventApi.md) | The history of approval events of the vat report. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


