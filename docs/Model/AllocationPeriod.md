# AllocationPeriod

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** |  | [optional] 
**supplier_invoice_id** | **string** |  | 
**supplier_invoice_row_line** | **int** |  | 
**allocation_period_source_type** | **int** |  | [optional] 
**status** | **int** |  | [optional] 
**cost_center_item_id1** | **string** |  | [optional] 
**cost_center_item_id2** | **string** |  | [optional] 
**cost_center_item_id3** | **string** |  | [optional] 
**project_id** | **string** |  | [optional] 
**bookkeeping_date** | [**\DateTime**](\DateTime.md) |  | [optional] 
**rows** | [**\Trollweb\EaccountingApi\Model\AllocationPeriodRow[]**](AllocationPeriodRow.md) |  | 
**debit_account_number** | **int** |  | [optional] 
**credit_account_number** | **int** |  | [optional] 
**amount** | **double** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


