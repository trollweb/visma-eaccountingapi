# CountQueryOption

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**context** | [**\Trollweb\EaccountingApi\Model\ODataQueryContext**](ODataQueryContext.md) |  | [optional] 
**raw_value** | **string** |  | [optional] 
**value** | **bool** |  | [optional] 
**validator** | [**\Trollweb\EaccountingApi\Model\CountQueryValidator**](CountQueryValidator.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


