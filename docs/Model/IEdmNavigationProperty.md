# IEdmNavigationProperty

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**partner** | [**\Trollweb\EaccountingApi\Model\IEdmNavigationProperty**](IEdmNavigationProperty.md) |  | [optional] 
**on_delete** | **int** |  | [optional] 
**contains_target** | **bool** |  | [optional] 
**referential_constraint** | [**\Trollweb\EaccountingApi\Model\IEdmReferentialConstraint**](IEdmReferentialConstraint.md) |  | [optional] 
**property_kind** | **int** |  | [optional] 
**type** | [**\Trollweb\EaccountingApi\Model\IEdmTypeReference**](IEdmTypeReference.md) |  | [optional] 
**declaring_type** | [**\Trollweb\EaccountingApi\Model\IEdmStructuredType**](IEdmStructuredType.md) |  | [optional] 
**name** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


