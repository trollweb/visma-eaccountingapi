# OrderByQueryOption

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**context** | [**\Trollweb\EaccountingApi\Model\ODataQueryContext**](ODataQueryContext.md) |  | [optional] 
**order_by_nodes** | [**\Trollweb\EaccountingApi\Model\OrderByNode[]**](OrderByNode.md) |  | [optional] 
**raw_value** | **string** |  | [optional] 
**validator** | [**\Trollweb\EaccountingApi\Model\OrderByQueryValidator**](OrderByQueryValidator.md) |  | [optional] 
**order_by_clause** | [**\Trollweb\EaccountingApi\Model\OrderByClause**](OrderByClause.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


