# IEdmVocabularyAnnotation

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**qualifier** | **string** |  | [optional] 
**term** | [**\Trollweb\EaccountingApi\Model\IEdmTerm**](IEdmTerm.md) |  | [optional] 
**target** | [**\Trollweb\EaccountingApi\Model\IEdmVocabularyAnnotatable**](IEdmVocabularyAnnotatable.md) |  | [optional] 
**value** | [**\Trollweb\EaccountingApi\Model\IEdmExpression**](IEdmExpression.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


