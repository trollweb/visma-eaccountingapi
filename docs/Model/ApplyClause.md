# ApplyClause

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**transformations** | [**\Trollweb\EaccountingApi\Model\TransformationNode[]**](TransformationNode.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


