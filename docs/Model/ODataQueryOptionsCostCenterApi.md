# ODataQueryOptionsCostCenterApi

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**if_match** | **object** |  | [optional] 
**if_none_match** | **object** |  | [optional] 
**context** | [**\Trollweb\EaccountingApi\Model\ODataQueryContext**](ODataQueryContext.md) |  | [optional] 
**request** | **object** |  | [optional] 
**raw_values** | [**\Trollweb\EaccountingApi\Model\ODataRawQueryOptions**](ODataRawQueryOptions.md) |  | [optional] 
**select_expand** | [**\Trollweb\EaccountingApi\Model\SelectExpandQueryOption**](SelectExpandQueryOption.md) |  | [optional] 
**apply** | [**\Trollweb\EaccountingApi\Model\ApplyQueryOption**](ApplyQueryOption.md) |  | [optional] 
**filter** | [**\Trollweb\EaccountingApi\Model\FilterQueryOption**](FilterQueryOption.md) |  | [optional] 
**order_by** | [**\Trollweb\EaccountingApi\Model\OrderByQueryOption**](OrderByQueryOption.md) |  | [optional] 
**skip** | [**\Trollweb\EaccountingApi\Model\SkipQueryOption**](SkipQueryOption.md) |  | [optional] 
**top** | [**\Trollweb\EaccountingApi\Model\TopQueryOption**](TopQueryOption.md) |  | [optional] 
**count** | [**\Trollweb\EaccountingApi\Model\CountQueryOption**](CountQueryOption.md) |  | [optional] 
**validator** | [**\Trollweb\EaccountingApi\Model\ODataQueryValidator**](ODataQueryValidator.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


