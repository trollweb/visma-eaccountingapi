# ODataPath

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**edm_type** | [**\Trollweb\EaccountingApi\Model\IEdmType**](IEdmType.md) |  | [optional] 
**navigation_source** | [**\Trollweb\EaccountingApi\Model\IEdmNavigationSource**](IEdmNavigationSource.md) |  | [optional] 
**segments** | [**\Trollweb\EaccountingApi\Model\ODataPathSegment[]**](ODataPathSegment.md) |  | [optional] 
**path_template** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


