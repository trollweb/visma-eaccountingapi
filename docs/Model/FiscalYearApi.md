# FiscalYearApi

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** | Read-only: Unique Id provided by eAccounting | [optional] 
**start_date** | [**\DateTime**](\DateTime.md) |  | [optional] 
**end_date** | [**\DateTime**](\DateTime.md) |  | [optional] 
**is_locked_for_accounting** | **bool** | Read-only | [optional] 
**bookkeeping_method** | **int** | Read-only: When posting fiscalyear, previous years bookkeeping method is chosen. 0 &#x3D; Invoicing, 1 &#x3D; Cash, 2 &#x3D; NoBookkeeping | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


