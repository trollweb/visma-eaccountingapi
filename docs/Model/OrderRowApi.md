# OrderRowApi

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**line_number** | **int** |  | 
**delivered_quantity** | **double** | Format: 2 decimals | [optional] 
**article_id** | **string** |  | [optional] 
**article_number** | **string** | Max length: 40 characters | [optional] 
**is_text_row** | **bool** |  | 
**text** | **string** | Max length: 2000 characters | [optional] 
**unit_price** | **double** | Format: 2 decimals | [optional] 
**discount_percentage** | **double** | Format: 2 decimals | [optional] 
**quantity** | **double** | Format: 4 decimals | [optional] 
**work_cost_type** | **int** |  | [optional] 
**is_work_cost** | **bool** |  | 
**eligible_for_reverse_charge_on_vat** | **bool** |  | 
**cost_center_item_id1** | **string** | Source: Get from /v2/costcenters | [optional] 
**cost_center_item_id2** | **string** | Source: Get from /v2/costcenters | [optional] 
**cost_center_item_id3** | **string** | Source: Get from /v2/costcenters | [optional] 
**id** | **string** |  | [optional] 
**project_id** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


