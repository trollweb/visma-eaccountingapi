# CostCenterApi

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **string** |  | [optional] 
**number** | **int** |  | [optional] 
**is_active** | **bool** |  | [optional] 
**items** | [**\Trollweb\EaccountingApi\Model\CostCenterItemApi[]**](CostCenterItemApi.md) |  | [optional] 
**id** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


