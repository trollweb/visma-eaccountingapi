# IEdmNavigationPropertyBinding

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**navigation_property** | [**\Trollweb\EaccountingApi\Model\IEdmNavigationProperty**](IEdmNavigationProperty.md) |  | [optional] 
**target** | [**\Trollweb\EaccountingApi\Model\IEdmNavigationSource**](IEdmNavigationSource.md) |  | [optional] 
**path** | [**\Trollweb\EaccountingApi\Model\IEdmPathExpression**](IEdmPathExpression.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


