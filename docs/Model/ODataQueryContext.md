# ODataQueryContext

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**default_query_settings** | [**\Trollweb\EaccountingApi\Model\DefaultQuerySettings**](DefaultQuerySettings.md) |  | [optional] 
**model** | [**\Trollweb\EaccountingApi\Model\IEdmModel**](IEdmModel.md) |  | [optional] 
**element_type** | [**\Trollweb\EaccountingApi\Model\IEdmType**](IEdmType.md) |  | [optional] 
**navigation_source** | [**\Trollweb\EaccountingApi\Model\IEdmNavigationSource**](IEdmNavigationSource.md) |  | [optional] 
**element_clr_type** | **string** |  | [optional] 
**path** | [**\Trollweb\EaccountingApi\Model\ODataPath**](ODataPath.md) |  | [optional] 
**request_container** | [**\Trollweb\EaccountingApi\Model\IServiceProvider**](IServiceProvider.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


