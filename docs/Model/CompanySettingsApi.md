# CompanySettingsApi

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **string** |  | 
**email** | **string** |  | [optional] 
**phone** | **string** |  | [optional] 
**mobile_phone** | **string** |  | [optional] 
**address1** | **string** |  | [optional] 
**address2** | **string** |  | [optional] 
**country_code** | **string** |  | 
**postal_code** | **string** |  | [optional] 
**city** | **string** |  | [optional] 
**website** | **string** |  | [optional] 
**currency_code** | **string** |  | [optional] 
**terms_of_payment_id** | **string** |  | [optional] 
**corporate_identity_number** | **string** | Read-only | [optional] 
**vat_code** | **string** | VAT identification number | [optional] 
**bank_giro** | **string** | Only used in Sweden. | [optional] 
**plus_giro** | **string** | Only used in Sweden. | [optional] 
**bank_account** | **string** |  | [optional] 
**iban** | **string** |  | [optional] 
**accounting_locked_to** | [**\DateTime**](\DateTime.md) |  | [optional] 
**gln** | **string** | Global Location Number | [optional] 
**product_variant** | **int** | Read-only: Variant of eAccouting. 1 &#x3D; Standard/Smart, 2 &#x3D; Invoicing, 3 &#x3D; Bookkeeping, 4 &#x3D; Start/Solo, 5 &#x3D; Pro, 6 &#x3D; InvoicingCollaboration | [optional] 
**type_of_business** | **int** | Read-only: 1 &#x3D; Corporation, 2 &#x3D; SoleProprietorship, 3 &#x3D; EconomicAssociation, 4 &#x3D; NonProfitOrganization, 5 &#x3D; GeneralPartnership, 6 &#x3D; LimitedPartnership, 7 &#x3D; Cooperatives, 9 &#x3D; PublicLimited | [optional] 
**vat_period** | **int** | Read-only: Period when VAT report should be sent. 1 &#x3D; OnceAMonth12th, 2 &#x3D; OnceAMonth26th, 3 &#x3D; OnceAQuarter, 4 &#x3D; OnceAYear, 5 &#x3D; Never, 6 &#x3D; Bimonthly, 7 &#x3D; OnceAMonth, 8 &#x3D; TwiceAYear, 9 &#x3D; OnceAQuarterFloating | [optional] 
**activated_modules** | **string[]** |  | [optional] 
**company_text** | [**\Trollweb\EaccountingApi\Model\CompanyTextsApi**](CompanyTextsApi.md) | Company settings - Other information | [optional] 
**next_customer_number** | **int** | Read-only | [optional] 
**next_supplier_number** | **int** | Read-only | [optional] 
**next_customer_invoice_numer** | **int** | Read-only | [optional] 
**next_quote_number** | **int** | Read-only | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


