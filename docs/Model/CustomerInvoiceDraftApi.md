# CustomerInvoiceDraftApi

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** | Read-only: Unique Id provided by eAccounting | [optional] 
**customer_id** | **string** | Source: Get from /v2/customers | 
**created_utc** | [**\DateTime**](\DateTime.md) | Read-only: Is automatically set | [optional] 
**is_credit_invoice** | **bool** |  | [optional] 
**rot_reduced_invoicing_type** | **int** | 0 &#x3D; Normal, 1 &#x3D; Rot, 2 &#x3D; Rut | 
**rot_reduced_invoicing_property_name** | **string** | Max length: 40 characters | [optional] 
**rot_reduced_invoicing_org_number** | **string** | Max length: 11 characters | [optional] 
**rot_reduced_invoicing_amount** | **double** | Format: 2 decimals | [optional] 
**rot_reduced_invoicing_automatic_distribution** | **bool** | Default: False | [optional] 
**rot_property_type** | **int** |  | [optional] 
**house_work_other_costs** | **double** |  | [optional] 
**rows** | [**\Trollweb\EaccountingApi\Model\CustomerInvoiceDraftRowApi[]**](CustomerInvoiceDraftRowApi.md) |  | [optional] 
**persons** | [**\Trollweb\EaccountingApi\Model\SalesDocumentRotRutReductionPersonApi[]**](SalesDocumentRotRutReductionPersonApi.md) |  | [optional] 
**your_reference** | **string** | Max length: 100 characters | [optional] 
**our_reference** | **string** | Max length: 100 characters | [optional] 
**invoice_customer_name** | **string** | Max length: 50 characters | 
**invoice_address1** | **string** | Max length: 50 characters | [optional] 
**invoice_address2** | **string** | Max length: 50 characters | [optional] 
**invoice_postal_code** | **string** | Max length: 10 characters | 
**invoice_city** | **string** | Max length: 50 characters | 
**invoice_country_code** | **string** | Max length: 2 characters | 
**delivery_customer_name** | **string** | Max length: 50 characters | [optional] 
**delivery_address1** | **string** | Max length: 50 characters | [optional] 
**delivery_address2** | **string** | Max length: 50 characters | [optional] 
**delivery_postal_code** | **string** | Max length: 10 characters | [optional] 
**delivery_city** | **string** | Max length: 50 characters | [optional] 
**delivery_country_code** | **string** | Max length: 2 characters | [optional] 
**delivery_method_name** | **string** | Max length: 50 characters | [optional] 
**delivery_term_name** | **string** | Max length: 50 characters | [optional] 
**delivery_method_code** | **string** | Max length: 20 characters | [optional] 
**delivery_term_code** | **string** | Max length: 20 characters | [optional] 
**eu_third_party** | **bool** |  | 
**customer_is_private_person** | **bool** |  | 
**reverse_charge_on_construction_services** | **bool** |  | 
**sales_document_attachments** | **string[]** | Read-only | [optional] 
**invoice_date** | [**\DateTime**](\DateTime.md) |  | [optional] 
**delivery_date** | [**\DateTime**](\DateTime.md) |  | [optional] 
**customer_number** | **string** | Read-only  Max length: 16 characters | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


