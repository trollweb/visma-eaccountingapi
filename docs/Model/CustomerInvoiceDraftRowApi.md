# CustomerInvoiceDraftRowApi

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**line_number** | **int** |  | 
**article_id** | **string** | Source: Get from /v2/articles. Required if IsTextRow is false | [optional] 
**article_number** | **string** | Purpose: Returns the article number from the entered ArticleId | [optional] 
**is_text_row** | **bool** |  | 
**text** | **string** | Max length: 2000. Sets the article name | 
**unit_price** | **double** | Format: 2 decimals allowed | [optional] 
**discount_percentage** | **double** | Format: 4 decimals allowed | [optional] 
**quantity** | **double** | Format: 2 decimals | [optional] 
**work_cost_type** | **int** |  | [optional] 
**is_work_cost** | **bool** |  | [optional] 
**work_hours** | **double** |  | [optional] 
**material_costs** | **double** |  | [optional] 
**reversed_construction_services_vat_free** | **bool** |  | 
**cost_center_item_id1** | **string** | Source: Get from /v2/costcenteritems | [optional] 
**cost_center_item_id2** | **string** | Source: Get from /v2/costcenteritems | [optional] 
**cost_center_item_id3** | **string** | Source: Get from /v2/costcenteritems | [optional] 
**unit_abbreviation** | **string** |  | [optional] 
**vat_rate_id** | **string** | Source: Get from /v2/articleaccountcodings   Read-only | [optional] 
**unit_name** | **string** |  | [optional] 
**project_id** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


