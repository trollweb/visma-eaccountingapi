# OrderApi

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** | Read-only: Unique Id provided by eAccounting | [optional] 
**amount** | **double** | Format: 2 decimals | 
**customer_id** | **string** |  | 
**currency_code** | **string** | Max length: 3 characters | 
**created_utc** | [**\DateTime**](\DateTime.md) | Read-Only | [optional] 
**vat_amount** | **double** | Format: 2 decimals | 
**roundings_amount** | **double** | Format: 2 decimals | 
**delivered_amount** | **double** | Format: 2 decimals | [optional] 
**delivered_vat_amount** | **double** | Format: 2 decimals | [optional] 
**delivered_roundings_amount** | **double** | Format: 2 decimals | [optional] 
**delivery_customer_name** | **string** | Max length: 50 characters | [optional] 
**delivery_address1** | **string** | Max length: 50 characters | [optional] 
**delivery_address2** | **string** | Max length: 50 characters | [optional] 
**delivery_postal_code** | **string** | Max length: 10 characters | [optional] 
**delivery_city** | **string** | Max length: 50 characters | [optional] 
**delivery_country_code** | **string** | Max length: 2 characters | [optional] 
**your_reference** | **string** | Max length: 50 characters | [optional] 
**our_reference** | **string** | Max length: 50 characters | [optional] 
**invoice_address1** | **string** | Max length: 50 characters | [optional] 
**invoice_address2** | **string** | Max length: 50 characters | [optional] 
**invoice_city** | **string** |  | 
**invoice_country_code** | **string** | Max length: 2 characters | 
**invoice_customer_name** | **string** | Max length: 50 characters | 
**invoice_postal_code** | **string** | Max length: 10 characters | 
**delivery_method_name** | **string** | Max length: 50 characters | [optional] 
**delivery_method_code** | **string** | Max length: 50 characters | [optional] 
**delivery_term_name** | **string** | Max length: 50 characters | [optional] 
**delivery_term_code** | **string** | Max length: 50 characters | [optional] 
**eu_third_party** | **bool** |  | 
**customer_is_private_person** | **bool** |  | 
**order_date** | [**\DateTime**](\DateTime.md) | Format: YYYY-MM-DD | 
**status** | **int** | 1 &#x3D; Draft, 2 &#x3D; Ongoing, 3 &#x3D; Shipped, 4 &#x3D; Invoiced | 
**number** | **int** |  | [optional] 
**modified_utc** | [**\DateTime**](\DateTime.md) | Read-Only | [optional] 
**delivery_date** | [**\DateTime**](\DateTime.md) | Format: YYYY-MM-DD. Default: null | [optional] 
**house_work_amount** | **double** |  | [optional] 
**house_work_automatic_distribution** | **bool** |  | [optional] 
**house_work_corporate_identity_number** | **string** | Max length: 20 characters | [optional] 
**house_work_property_name** | **string** | Max length: 100 characters | [optional] 
**rows** | [**\Trollweb\EaccountingApi\Model\OrderRowApi[]**](OrderRowApi.md) |  | [optional] 
**shipped_date_time** | [**\DateTime**](\DateTime.md) | Format: YYYY-MM-DD. Default: null | [optional] 
**rot_reduced_invoicing_type** | **int** | 0 &#x3D; None, 1 &#x3D; Rot, 2 &#x3D; Rut | 
**rot_property_type** | **int** |  | [optional] 
**persons** | [**\Trollweb\EaccountingApi\Model\SalesDocumentRotRutReductionPersonApi[]**](SalesDocumentRotRutReductionPersonApi.md) |  | [optional] 
**reverse_charge_on_construction_services** | **bool** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


