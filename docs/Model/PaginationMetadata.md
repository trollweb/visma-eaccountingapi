# PaginationMetadata

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**current_page** | **int** |  | [optional] 
**page_size** | **int** |  | [optional] 
**total_number_of_pages** | **int** |  | [optional] 
**total_number_of_results** | **int** |  | [optional] 
**server_time_utc** | [**\DateTime**](\DateTime.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


