# CompanyTextsApi

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**customer_invoice_text_domestic** | **string** |  | [optional] 
**customer_invoice_text_foreign** | **string** |  | [optional] 
**order_text_domestic** | **string** |  | [optional] 
**order_text_foreign** | **string** |  | [optional] 
**over_due_text_domestic** | **string** |  | [optional] 
**over_due_text_foreign** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


