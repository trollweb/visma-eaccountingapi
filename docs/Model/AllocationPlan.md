# AllocationPlan

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**supplier_invoice_id** | **string** |  | [optional] 
**supplier_invoice_row** | **int** |  | [optional] 
**voucher_id** | **string** |  | [optional] 
**voucher_row** | **int** |  | [optional] 
**bookkeeping_start_date** | [**\DateTime**](\DateTime.md) |  | 
**amount_to_allocate** | **double** |  | 
**quantity_to_allocate** | **double** |  | [optional] 
**weight_to_allocate** | **double** |  | [optional] 
**allocation_account_number** | **int** |  | 
**number_of_allocation_periods** | **int** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


