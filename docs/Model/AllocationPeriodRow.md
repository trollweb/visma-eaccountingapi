# AllocationPeriodRow

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** |  | [optional] 
**allocation_period_id** | **string** |  | [optional] 
**account_number** | **int** |  | 
**amount** | **double** |  | 
**debit_credit** | **int** |  | 
**quantity** | **double** |  | [optional] 
**weight** | **double** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


