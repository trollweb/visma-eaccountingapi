# ODataPathSegment

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**edm_type** | [**\Trollweb\EaccountingApi\Model\IEdmType**](IEdmType.md) |  | [optional] 
**identifier** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


