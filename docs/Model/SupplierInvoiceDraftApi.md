# SupplierInvoiceDraftApi

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** | Read-only: Unique Id provided by eAccounting | [optional] 
**supplier_id** | **string** | Source: Get from /supplierlistitems | 
**bank_account_id** | **string** | Source: Get from /bankaccounts, if not provided the supplier bank account will be used. | [optional] 
**invoice_date** | [**\DateTime**](\DateTime.md) | Format: YYYY-MM-DD. Default: Today&#39;s date | [optional] 
**payment_date** | [**\DateTime**](\DateTime.md) | Format: YYYY-MM-DD | [optional] 
**due_date** | [**\DateTime**](\DateTime.md) | Format: YYYY-MM-DD. Default: Date based on the suppliers Terms of payment | [optional] 
**invoice_number** | **string** | Max length: 50 characters | 
**total_amount** | **double** | Format: Max 2 decimals | [optional] 
**vat** | **double** | Format: Max 2 decimals | [optional] 
**vat_high** | **double** | Format: Max 2 decimals | [optional] 
**vat_medium** | **double** | Format: Max 2 decimals | [optional] 
**vat_low** | **double** | Format: Max 2 decimals | [optional] 
**is_credit_invoice** | **bool** |  | 
**currency_code** | **string** | Max length: 3 characters | [optional] 
**currency_rate** | **double** | Purpose: If currency code is domestic and currency rate isn&#39;t included it will be fetched from eAccounting | [optional] 
**ocr_number** | **string** | Max length: 25 characters | [optional] 
**message** | **string** | Max length: 25 characters | [optional] 
**rows** | [**\Trollweb\EaccountingApi\Model\SupplierInvoiceDraftRowApi[]**](SupplierInvoiceDraftRowApi.md) |  | 
**supplier_name** | **string** | Max length: 50 characters | [optional] 
**supplier_number** | **string** | Max length: 50 characters | [optional] 
**self_employed_without_fixed_address** | **bool** |  | [optional] 
**is_quick_invoice** | **bool** |  | [optional] 
**is_domestic** | **bool** |  | [optional] 
**approval_status** | **int** | 0 &#x3D; None, 1 &#x3D; Approved, 2 &#x3D; Rejected, 3 &#x3D; ReadyForApproval | [optional] 
**allocation_periods** | [**\Trollweb\EaccountingApi\Model\AllocationPeriodApi[]**](AllocationPeriodApi.md) | Read-only: Post to /supplierinvoicedrafts/{supplierInvoiceDraftId}/allocationperiods/ | [optional] 
**attachments** | [**\Trollweb\EaccountingApi\Model\AttachmentLinkApi**](AttachmentLinkApi.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


