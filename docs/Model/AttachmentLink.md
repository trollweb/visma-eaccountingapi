# AttachmentLink

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**document_id** | **string** |  | [optional] 
**document_type** | **int** |  | 
**attachment_ids** | **string[]** | Source: Get from /v1/attachments. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


