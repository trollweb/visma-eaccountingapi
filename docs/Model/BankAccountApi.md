# BankAccountApi

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**bank** | **string** |  | 
**bank_account_type** | **int** | 1 &#x3D; ChequeAccount, 2 &#x3D; CashAccount, 3 &#x3D; SavingsAccount, 4 &#x3D; CurrencyAccount, 5 &#x3D; DigitalWalletAccount,  6 &#x3D; CashCreditAccount, 7 &#x3D; TaxAccount | 
**bank_account_type_description** | **string** | Read-only: Description of Bank Account type | [optional] 
**bban** | **string** | Also known as Bank Account number | 
**iban** | **string** |  | [optional] 
**name** | **string** |  | 
**id** | **string** | Read-only: Unique Id provided by eAccounting | [optional] 
**is_active** | **bool** |  | [optional] 
**ledger_account_number** | **int** |  | 
**has_active_bank_agreement** | **bool** |  | [optional] 
**is_default_cheque_account** | **bool** | Purpose: Only used when having several cheque accounts | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


