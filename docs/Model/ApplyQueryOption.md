# ApplyQueryOption

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**context** | [**\Trollweb\EaccountingApi\Model\ODataQueryContext**](ODataQueryContext.md) |  | [optional] 
**result_clr_type** | **string** |  | [optional] 
**apply_clause** | [**\Trollweb\EaccountingApi\Model\ApplyClause**](ApplyClause.md) |  | [optional] 
**raw_value** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


