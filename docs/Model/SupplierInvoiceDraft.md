# SupplierInvoiceDraft

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** | Read-only: Unique Id provided by eAccounting | [optional] 
**supplier_id** | **string** | Source: Get from /v1/supplierlistitems | 
**bank_account_id** | **string** | Source: Get from /v1/bankaccounts | [optional] 
**invoice_date** | [**\DateTime**](\DateTime.md) | Format: YYYY-MM-DD. Default: Today&#39;s date | [optional] 
**payment_date** | [**\DateTime**](\DateTime.md) | Format: YYYY-MM-DD | [optional] 
**due_date** | [**\DateTime**](\DateTime.md) | Format: YYYY-MM-DD. Default: Date based on the suppliers Terms of payment | [optional] 
**invoice_number** | **string** | Max length: 50 characters | [optional] 
**total_amount** | **double** | Format: Max 2 decimals | [optional] 
**vat** | **double** | Format: Max 2 decimals | [optional] 
**vat_high** | **double** | Format: Max 2 decimals | [optional] 
**vat_medium** | **double** | Format: Max 2 decimals | [optional] 
**vat_low** | **double** | Format: Max 2 decimals | [optional] 
**is_credit_invoice** | **bool** |  | 
**currency_code** | **string** | Max length: 3 characters | [optional] 
**currency_rate** | **double** | Purpose: If currency code is domestic and currency rate isn&#39;t included it will be fetched from eAccounting | [optional] 
**ocr_number** | **string** | Max length: 25 characters | [optional] 
**message** | **string** | Max length: 25 characters | [optional] 
**rows** | [**\Trollweb\EaccountingApi\Model\SupplierInvoiceDraftRow[]**](SupplierInvoiceDraftRow.md) |  | 
**supplier_name** | **string** | Max length: 50 characters | [optional] 
**supplier_number** | **string** | Max length: 50 characters | [optional] 
**self_employed_without_fixed_address** | **bool** |  | [optional] 
**allocation_periods** | [**\Trollweb\EaccountingApi\Model\AllocationPeriod[]**](AllocationPeriod.md) | Read-only: Post to /v1/supplierinvoicedrafts/{supplierInvoiceDraftId}/allocationperiods/ | [optional] 
**attachments** | [**\Trollweb\EaccountingApi\Model\AttachmentLink**](AttachmentLink.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


