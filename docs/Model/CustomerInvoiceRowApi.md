# CustomerInvoiceRowApi

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**article_number** | **string** |  | [optional] 
**amount_no_vat** | **double** |  | 
**percent_vat** | **double** |  | 
**line_number** | **int** |  | 
**is_text_row** | **bool** |  | 
**text** | **string** |  | 
**unit_price** | **double** |  | [optional] 
**unit_name** | **string** |  | [optional] 
**discount_percentage** | **double** |  | [optional] 
**quantity** | **double** |  | [optional] 
**is_work_cost** | **bool** |  | 
**is_vat_free** | **bool** |  | 
**cost_center_item_id1** | **string** |  | [optional] 
**cost_center_item_id2** | **string** |  | [optional] 
**cost_center_item_id3** | **string** |  | [optional] 
**unit_id** | **string** |  | [optional] 
**project_id** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


