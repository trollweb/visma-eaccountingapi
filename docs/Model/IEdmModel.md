# IEdmModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**schema_elements** | [**\Trollweb\EaccountingApi\Model\IEdmSchemaElement[]**](IEdmSchemaElement.md) |  | [optional] 
**vocabulary_annotations** | [**\Trollweb\EaccountingApi\Model\IEdmVocabularyAnnotation[]**](IEdmVocabularyAnnotation.md) |  | [optional] 
**referenced_models** | [**\Trollweb\EaccountingApi\Model\IEdmModel[]**](IEdmModel.md) |  | [optional] 
**declared_namespaces** | **string[]** |  | [optional] 
**direct_value_annotations_manager** | [**\Trollweb\EaccountingApi\Model\IEdmDirectValueAnnotationsManager**](IEdmDirectValueAnnotationsManager.md) |  | [optional] 
**entity_container** | [**\Trollweb\EaccountingApi\Model\IEdmEntityContainer**](IEdmEntityContainer.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


