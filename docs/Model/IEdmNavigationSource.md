# IEdmNavigationSource

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**navigation_property_bindings** | [**\Trollweb\EaccountingApi\Model\IEdmNavigationPropertyBinding[]**](IEdmNavigationPropertyBinding.md) |  | [optional] 
**path** | [**\Trollweb\EaccountingApi\Model\IEdmPathExpression**](IEdmPathExpression.md) |  | [optional] 
**type** | [**\Trollweb\EaccountingApi\Model\IEdmType**](IEdmType.md) |  | [optional] 
**name** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


